# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide tapp_lib 1.0

#
# The application argument utility. Provides functionality for both proc and
# application parameter handling.
#
# @cfg AUTO_LOG_INIT     Logs are automatically initialised after parsing the
#                        default TApp parameters. Defaults to enabled. Disable
#                        via config if the application at hand handles
#                        additional log configuration.
#
namespace eval arg_utils {
	namespace export let
	opt::proc ::param
}

#
# This procedure is responsible for formatting and aligning the passed in
# arguments for display purposes.
#
# @param args               The arguments to format for display purposes
# @cfg CONSOLE_WIDTH        Determines the width of the console, note that this is
#                           automatically set by the tapp package on load if the
#                           core linux utility "tty" is available. Defaults to 80.
# @cfg HELP_ARG_WIDTH       The width of the argument column, which determines
#                           where the argument comment column starts, defaults to
#                           27.
# @cfg HELP_ARG_INDENT      Determines where the argument column starts, i.e. how
#                           much the arguments are indented. Defaults to 2.
# @cfg HELP_ARG_PADDING     Determines the padding needed when there are only one
#                           argument. The formatting utility displays up to two
#                           aliases for the same parameter, for example the help
#                           can be triggered by passing in either ‑h or ‑‑help.
#                           This would be listed as ‑h, ‑‑help in the output. The
#                           padding here is for when the ‑h parameter does not
#                           exist, in which case we need 4 spaces to align the
#                           ‑‑help parameter with other parameters. Defaults to 4.
#                           This is a config that one should generally not need to
#                           change.
# @cfg EXIT_ON_ARG_FAILURE  By default TApp will exit when argument validation
#                           fails, i.e. an illegal parameter is passed in or a
#                           parameter expecting a file argument receives none.
#                           When exiting an error message will be written to
#                           standard err. This can be disabled by setting this
#                           configuration item to 0. The main risk of disabling
#                           this functionality is that it may not be clear to
#                           the end user that an argument passed in was not
#                           actually applied.
# @return                   The arguments formatted as text (string containing
#                           newlines)
#
opt::option param format --params { args } --body {

	touch args_to_print padding processed_args

	set line_width  [cfg get CONSOLE_WIDTH 80]
	set arg_width   [cfg get HELP_ARG_WIDTH 27]
	set arg_indent  [cfg get HELP_ARG_INDENT 2]
	set arg_padding [cfg get HELP_ARG_PADDING 4]

	set arguments [concat {*}$args]

	# Check whether we only have single numbered arguments, if not
	# then we want to pad the first argument with spaces so that single
	# numbered arguments line up with double numbered arguments
	foreach {arglist desc} $arguments {
		if {[llength $arglist] > 1} {
			set padding [string repeat { } $arg_padding]
			break
		}
	}

	# Loop through all arguments first to find out if we need to auto
	# increase the arg width.
	foreach {arglist desc} $arguments {

		# Only show up to two of the arguments
		set arg1 [lindex $arglist 0]
		set arg2 [lindex $arglist 1]

		set joined_arg [expr {$arg2 != {} ? "$arg1, $arg2" : "$padding$arg1"}]

		if {[string length $joined_arg] > $arg_width} {
			set arg_width [expr {[string length $joined_arg] + 1}]
		}
	}

	set desc_width  [expr {$line_width - $arg_width - $arg_indent}]
	set desc_indent [string repeat { } [expr {$arg_width + $arg_indent}]]
	set format_str  [string repeat { } $arg_indent]%-${arg_width}s%s

	foreach {arglist desc} $arguments {

		# Only show up to two of the arguments
		set arg1 [lindex $arglist 0]
		set arg2 [lindex $arglist 1]

		if {$arg2 != {}} {
			set joined_arg "$arg1, $arg2"
		} elseif {![string match {--*} $arg1] || [string length $arg1] <= $arg_padding - 2} {
			set joined_arg $arg1
		} else {
			set joined_arg "$padding$arg1"
		}

		# We only want to process each argument once, for example if
		# defined in both application help and in the more help section.
		if {[lsearch -exact $processed_args $joined_arg] != -1} {
			continue
		}
		lappend processed_args $joined_arg

		clear line

		set arg_printed 0

		foreach desc_line [split [string map {\n \x00 \\n \x00} $desc] \x00] {
			if {[string length $desc_line] <= $desc_width} {
				if {$arg_printed} {
					lappend args_to_print ${desc_indent}$desc_line
				} else {
					lappend args_to_print [format $format_str $joined_arg $desc_line]
					set arg_printed 1
				}
				continue
			}

			foreach word [split $desc_line { }] {
				if {[string length $line] + [string length $word] + 1 <= $desc_width} {
					if {[string length $line] != 0} {
						append line { }
					}
					append line $word
				} else {
					if {$arg_printed} {
						lappend args_to_print ${desc_indent}$line
					} else {
						lappend args_to_print [format $format_str $joined_arg $line]
						set arg_printed 1
					}
					set line $word
				}
			}

			if {[string length $line] > 0} {
				lappend args_to_print ${desc_indent}$line
			}
		}
	}

	# If we have additional args to display, add a trailing newline
	if {[llength $args_to_print] > 0} {
		lappend args_to_print {}
	}

	return [join $args_to_print \n]
}

#
# Join arguments that are separated by a space.
#
# Most parameter flags are just single values such as --debug, --stdout or
# --help.
#
# Some parameters take a value and these would need to be joined in the
# parameter list before the parsing takes place. An example of this would be
# the --cset CONFIG_ITEM=my_value parameter that allows for individual config
# items to be overridden from the command line.
#
# @param arguments       The full list of command line parameters
# @param args_to_join    List of parameters that take a value and needs to be
#                        joined (with the value) prior to the parameter parsing
#                        process taking place. Note that the items in this list
#                        can be regular expressions.
# @return                The list of command line parameters with the given
#                        parameters joined if they exist. The order of the
#                        argument list is preserved.
#
opt::option param join --params { arguments { args_to_join {} } } --body {
	touch joined_args
	set args_length [llength $arguments]
	for {set i 0} {$i < $args_length} {incr i} {
		set arg [lindex $arguments $i]
		foreach regexp $args_to_join {
			if {[regexp $regexp $arg] == 1} {
				incr i
				set arg [list $arg [lindex $arguments $i]]
				break
			}
		}
		lappend joined_args $arg
	}

	return $joined_args
}

#
# Procedure to split parameters based on a given prefix.
#
# For example:
#    --type d -i --file ex1.txt ex2.txt --num 1 2 3
#
# would be split up into a list on the form {param} {value...} {param} {value...} ...
#
# I.e.:
#    {--type} {d} {-i} {} {--file} {ex1.txt ex2.txt} {--num} {1 2 3}
#
# @param arguments        List of arguments to split
# @option -strip_prefix   Option to remove the prefix from the params in the returned list
# @option --arg_prefix    Specifies the prefix used, defaults to hyphen (-)
# @return                 A list on the form {param} {value...} {param} {value...} ...
#
opt::option param split --params { args } --body {

	let $args {
		strip_prefix 0
		arg_prefix   -
		default      {}
	} arguments

	touch output
	foreach arg [lreverse $arguments] {
		if {![regexp ^$arg_prefix+ $arg]} {
			lappend value $arg
			continue
		}
		if {[info exists value]} {
			lappend output [lreverse $value]
			unset value
		} else {
			lappend output $default
		}
		if {$strip_prefix} {
			set arg [string trimleft $arg $arg_prefix]
		}
		lappend output $arg
	}
	return [lreverse $output]
}

#
# Split file arguments based on the file type.
#
# The names of the variables to store the result in are passed in to the proc,
# the procedure does not return any values.
#
# Example usage:
#    param separate $argv params files directories
#    process_parameters $params
#    process_files $files
#    process_directories $directories
#
# @param arguments       List of arguments passed to the script
# @param app_var         The name of the variable to store application
#                        arguments as (this will be all non-file arguments)
# @param files_var       The variable name to store the file arguments as
# @param dirs_var        The variable name to store the directory arguments as
#
opt::option param separate --params { arguments { app_var {} } { files_var {} } { dirs_var {} } } --body {

	if {$app_var != {}} {
		upvar 1 $app_var application_args
	}
	if {$files_var != {}} {
		upvar 1 $files_var files_to_process
	}
	if {$dirs_var != {}} {
		upvar 1 $dirs_var dirs_to_process
	}

	touch application_args files_to_process dirs_to_process

	foreach arg $arguments {

		if {![file exists $arg]} {
			lappend application_args $arg
			continue
		}

		# If "." set file to current working directory
		if { $arg == {.} } {
			set file [pwd]
		}

		# Process based on file type
		set file_type [file type $arg]
		switch -- $file_type {
		fifo -
		link -
		characterSpecial -
		blockSpecial -
		socket -
		file {
			lappend files_to_process $arg
		}
		directory {
			lappend dirs_to_process $arg
		}
		default {
			log INFO {Strangely enough the file $arg exists but is not a recognisable file type ($file_type). Omitting. ($arg)}
		}
		}
	}
}

#
# Prints out the help info to standard out.
#
# @cfg HELP              Contains the help info text. If the %args% placeholder
#                        is present then this will be replaced with formatted
#                        arguments taken from the ARGS config item.
# @cfg ARGS              The application arguments that can be passed in, this
#                        will override the description for any existing TApp
#                        parameters provided the short and long parameter names
#                        match. Should be a list on the form:
#                        {-s --long} {description} {-l --list} {description}
#
proc arg_utils::help { args } {

	variable ARGS

	set charmap [list %args% [param format [concat [cfg get ARGS] [_get_help_args]]]]
	log STDOUT [string map $charmap [cfg get HELP]]
}

#
# Retrieves the application parameters that should be listed on the help output.
#
# The parameters returned are:
#     - suitable for the current ---more level
#     - ordered by the parameter disporder
#     - then ordered by the long parameter lexicographically
#
# @return                A list on the form "<param> <comment> <param>
#                        <comment> ..."
#
proc arg_utils::_get_help_args {} {

	variable ARGS

	set more_level [cfg get MORE_LEVEL 0]

	array set DISPORDER {}

	foreach param [array names ARGS] {
		set opts $ARGS($param)
		set param_more_level [dict get $opts more_level]

		# Exclude parameters that are not to be displayed
		if {$param_more_level < 0 || $param_more_level > $more_level} {
			continue
		}

		lappend DISPORDER([dict get $opts disporder]) $param
	}

	touch help_args

	foreach disporder [lsort -integer [array names DISPORDER]] {
		foreach param [lsort -command _param_sort $DISPORDER($disporder)] {
			lassign [split $param ,] short long

			foreach dictval {param_long param_short comment} {
				set $dictval [dict get $ARGS($param) $dictval]
			}

			if {$short == {}} {
				set param_disp [list --$param_long]
			} elseif {$long == {}} {
				set param_disp [list -$param_short]
			} else {
				set param_disp [list -$param_short --$param_long]
			}

			lappend help_args $param_disp $comment
		}
	}

	return $help_args
}

#
# lsort command: Special parameter sort which socuses on the long parameter
# version rather than just doing a lexicographical sort.
#
proc arg_utils::_param_sort { left right } {
	set lparam [string map {- {} _ {}} [string tolower [lindex [split $left  ,] end]]]
	set rparam [string map {- {} _ {}} [string tolower [lindex [split $right ,] end]]]
	return [string compare $lparam $rparam]
}

#
# Procedure to retrieve the value of a given parameter from a list of arguments.
#
# The number of argument prefixes (hyphen) determines the role of the argument
# passed in (either flags or parameters).
#
# A single prefix indicates an optional flag (e.g. -flag) and will default to 1
# if passed in. If a flag is followed by a value then this will not be taken
# into account as flags don't take values.
#
# Two prefixes indicates a parameter and parameters are followed by an explicit
# value (e.g. --file ex.txt). If the following argument looks like another
# parameter (e.g. starting with the prefix) then it will default to 1 and a
# warning will be logged.
#
# If more than one of the same parameter or flag is passed in then the last
# argument takes precedence and a warning will be logged.
#
# Parameters and flags are case insensitive.
#
# Synopsis:
#    param get <parameter> ?<default>? ?<list>?
#
# For example:
#    set args [list -i 3 --file ex1.txt ex2.txt --sep {|} --num 1 --num 2}]
#    set file [param get --file]
#    set sep  [param get --sep {:}]
#    set i    [param get -i]
#    set num  [param get --num]
#    set dflt [param get --default {99}]
#
# will result in:
#    set file {ex1.txt}; # because parameters takes maximum 1 value (use list)
#    set sep  {|};       # because parameter was found in args list
#    set i    {1};       # because flags (single hyphen) enable functionality
#    set num  {2};       # because the last parameter takes precedence
#    set dflt {99};      # because the parameter was not found in args list
#
# @param param           The parameter to search for (can be a regular expression)
# @param default         The default value if not found
# @param arguments       The list of arguments to search through, defaults to
#                        doing upvar to get hold of the proc's "args" variable
#                        if not defined.
# @param arg_prefix      The parameter prefix, defaults to hyphen (-)
# @return                Empty string if the argument value was not found,
#                        the value if a single value was found or "1" if the
#                        argument was found to exist, but with no values.
#
opt::option param get --params { param {default {}} {arguments "\0"} {arg_prefix -} } --body {

	if {$arguments == "\0"} {
		unset arguments
		upvar 1 args arguments
		if {![info exists arguments]} {
			error {Either pass the arguments list in or make sure the variable is named "args"}
		}
	}

	set length [llength $arguments]
	set param [string trimleft $param $arg_prefix]

	set idx [lsearch -nocase -all -regexp $arguments ^${arg_prefix}+$param\$]

	if {[llength $idx] == 0} {
		return $default
	}

	if {[llength $idx] > 1} {
		log WARNING {param get: Parameter $param passed in more than once.}
		set idx [lindex $idx end]
	}

	set arg [lindex $arguments $idx]
	set num [expr {[string length $arg] - [string length [string trimleft $arg $arg_prefix]]}]

	set value 1
	if {$num > 1} {
		incr idx
		set arg [lindex $arguments $idx]
		if {$idx >= [llength $arguments] || [regexp ^$arg_prefix+\[A-Za-z\] $arg]} {
			log WARNING {param get: No value passed in for parameter $param, defaulting to 1.}
		} else {
			set value $arg
		}
	}
	return $value
}

#
# Shorthand proc to get a series of parameter values from a list of arguments.
#
# Each parameter will be looked up in the list of arguments.
#   - if the parameter is not found then the default value will be used
#   - if the parameter is found then any following values will be used
#   - if the parameter is found, but isn't followed by any explicit values then
#     it will default to 1 (as in true / enabled)
#
# The value used is stored in a variable with the same name as the parameter
# (less any argument prefix) in the calling proc.
#
# The parameters passed in have to have the argument prefix. The amount of
# prefixes denote whether it is a flag taking no values (e.g. -nocase) or a
# parameter with a value (e.g. --file ex.txt).
#
# If parameters matching regular expressions is needed then have a look at
# param get.
#
# Synopsis:
#    let $args [list param default param default ... param default] ?arg_prefix?
#
# Example:
#    set args {--color red -enabled --list {d e f} --ignored yes}
#    let $args {
#            color   {blue}
#            list    {a b c}
#            enabled {0}
#            repeat  {no}
#    }
#
# Will result in the following variables being set:
#    set color   {red}
#    set list    {d e f}
#    set enabled {1}
#    set repeat  {no}
#
# @param arguments       The list of arguments to search through
# @param arg_list        A list on the form <parameter> <default>, can contain
#                        multiple parameters
# @param remaining_args  Name of the variable to store the remaining args in
# @param arg_prefix      The parameter prefix, defaults to hyphen (-)
#
proc arg_utils::let { arguments arg_list {remaining_args {}} {arg_prefix -} } {

	touch plist vlist rlist
	foreach {param default} $arg_list {
		lappend plist [string trimleft $param $arg_prefix]
		lappend vlist $default
	}

	set llength [llength $arguments]
	for {set i 0} {$i < $llength} {incr i} {
		set arg [lindex $arguments $i]
		if {[string index $arg 0] != $arg_prefix} {
			lappend rlist $arg
			continue
		}
		set val 1
		set prm [string trimleft $arg $arg_prefix]
		set idx [lsearch -nocase -exact $plist $prm]
		if {$idx == -1} {
			lappend rlist $arg
			continue
		}

		if {[string index $arg 1] == $arg_prefix} {
			set next [lindex $arguments $i+1]
			if {$i+1 >= $llength || [regexp ^$arg_prefix+\[A-Za-z\] $next]} {
				log WARNING {let: No value passed in for parameter $arg, defaulting to $val.}
			} else {
				set val $next
				incr i
			}
		}

		lset vlist $idx $val
	}

	foreach param $plist value $vlist {
		uplevel 1 "set $param {$value}"
	}

	if {$remaining_args != {}} {
		uplevel 1 "set $remaining_args {[join $rlist { }]}"
	}
}

#
# Parse the application arguments and take action based on registered
# parameters.
#
# Main responsibilities:
#     - expand file arguments
#     - identify application parameters
#     - prioritise parameters that need to run prior to others
#     - perform the action that should be taken when the parameter is passed in
#
# Synopsis:
#    param parse <arguments> ?<remaining_arguments_variable>?
#
# Example usage:
#    param parse $argv remaining_args
#
# or alternatively:
#    set remaining_args [param parse $argv]
#
# @param arguments
# @param remaining_args_var  The variable in which to store the remaining
#                            arguments (i.e. the ones that does not seem
#                            to be registered application parameters)
# @param arg_prefix          The parameter prefix, should always be a hyphen
#                            (-) character
# @return                    The remaining application parameters that were
#                            not identified (same as stored in the
#                            remaining_args_var if provided)
#
opt::option param parse --params { arguments { remaining_args_var {} } {arg_prefix -} } --body {

	if {$remaining_args_var != {}} {
		upvar 1 $remaining_args_var remaining_args
	}
	set expanded_args [arg_utils::_expand_fileargs $arguments]
	set args_to_parse [arg_utils::_identify_params $expanded_args remaining_args $arg_prefix]
	set args_to_parse [arg_utils::_prioritise_args $args_to_parse]
	set parse_result  [arg_utils::_evaluate_params $args_to_parse]
	if {[cfg enabled AUTO_LOG_INIT 1]} {
		tapp_log::init
	}
	return $remaining_args
}

#
# Expands file arguments, i.e. when an argument contains an asterix
# like file*.txt and one or more files matches then the argument will
# be replaced with said files as individual arguments
#
# @param arguments       The list of arguments to go through to check every
#                        that contains an asterix (*)
# @return                The same list of arguments with file matching patterns
#                        having been replaced with file arguments
#
proc arg_utils::_expand_fileargs { arguments } {

	set length [llength $arguments]
	for {set i 0} {$i < $length} {incr i} {
		if {[string first * [lindex $arguments $i]] == -1} {
			continue
		}

		set arg [lindex $arguments $i]
		set arg [string map {:\\ :/} $arg]; # Windows compatibility
		set files [glob -nocomplain $arg]
		if {[llength $files]} {
			set arguments [lreplace $arguments $i $i {*}$files]
		}
	}

	return $arguments
}

#
# Prioritises arguments based on the --priority setting for the parameter.
#
# @param arguments       The arguments to sort by priority
# @see                   param register for further info on the
#                        --priority flag and default value
# @return                The arguments ordered by the parameter priority
#
proc arg_utils::_prioritise_args { arguments } {

	variable ARGS

	array set PRIORITY {}

	foreach argument $arguments {
		set param [lindex $argument 0]
		lappend PRIORITY([dict get $ARGS($param) priority]) $argument
	}

	set output_list {}
	foreach priority [lsort -integer [array names PRIORITY]] {
		lappend output_list {*}$PRIORITY($priority)
	}

	return $output_list
}

#
# Identifies registered parameters in the list of arguments and returns them.
#
# @param arguments           The list of arguments to compare against
#                            registered application parameters
# @param remaining_args_var  The variable in which to store the remaining
#                            arguments (i.e. the ones that did not match)
# @param arg_prefix          The parameter prefix, should always be a hyphen
#                            (-) character
# @return                    A list of lists containing the parameter key and
#                            the parameter value (if any)
#
proc arg_utils::_identify_params { arguments { remaining_args_var {} } {arg_prefix -} } {

	variable ARGS

	if {$remaining_args_var != {}} {
		upvar 1 $remaining_args_var remaining_args
	}

	touch remaining_args
	touch args_to_parse
	# Identifying parameters and their arguments
	set l [llength $arguments]
	set parse_options 1
	for {set i 0} {$i < $l} {incr i} {
		set arg [lindex $arguments $i]

		if {!$parse_options || ![string match ${arg_prefix}* $arg]} {
			lappend remaining_args $arg
			continue
		}

		# Marks end of options
		if {$arg == {--}} {
			set parse_options 0
			continue
		}

		# Handle --param=value options
		set argl [split $arg {=}]
		set arg0 [lindex $argl 0]
		set param [arg_utils::_clean_param $arg0]

		if {$param == {}} {
			lappend remaining_args $arg
			continue
		}

		set value [join [lrange $argl 1 end] {=}]
		set value [string trim $value {\"'}]

		# Try the short version first
		set key [array names ARGS $param,*]

		# If not found try the long version
		if {$key == {}} {
			set key [array names ARGS *,$param]
		}

		# If the long version also doesn't exist, let's check if this
		# is a combined argument like -clr
		if {$key == {} && [string index $arg0 1] != {-}} {

			set param_parts [split $param {}]
			set param_keys {}
			foreach part $param_parts {
				set param_key [array names ARGS $part,*]
				if {$param_key == {}} {
					log DEBUG {Rejecting $arg0 because -$part was not found to be a valid parameter}
					if {[cfg enabled EXIT_ON_ARG_FAILURE 1]} {
						puts stderr "[this_script]: invalid argument '$part' (from argument $arg)"
						puts stderr "Try '[this_script] --help' for more information."
						exit 1
					}
					break
				}
				lappend param_keys $param_key
			}

			# If all the characters in the argument were found to be valid parameters,
			# they should be fine to expand. Check first if we have any parameters that
			# mandates an argument.
			if {[llength $param_keys] == [llength $param_parts]} {
				# If one of the parameters mandates an argument then set that as
				# the primary key for the following arguments. If more than one
				# parameter mandates an argument then reject the argument in full.
				foreach param_key $param_keys {
					if {[dict get $ARGS($param_key) min_args] > 0} {
						lappend key $param_key
					}
				}

				if {[llength $key] > 1} {
					log DEBUG {Rejecting $arg0 because there are multiple parameters that requires an argument (-$key and -$param_key)}
					if {[cfg enabled EXIT_ON_ARG_FAILURE 1]} {
						puts stderr "Error: there are multiple parameters that requires aaasfd (-[lindex [split $key ,] 0] and -[lindex [split $param_key ,] 0]) (from argument $arg)"
						puts stderr "Consider passing these in individually."
						puts stderr "Try '[this_script] --help' for more information."
						exit 1
					}
					lappend remaining_args $arg
					continue
				}


				if {$key == {}} {
					set key [lindex $param_keys end]
				}
				foreach param_key $param_keys {
					if {$param_key == $key} {
						continue
					}
					lappend args_to_parse [list $param_key {} -[lindex [split $param_key ,] 0]]
				}
				log DEBUG {Split argument $arg0 into -[join $param_parts { -}]}
			}
		}

		if {$key == {}} {
			lappend remaining_args $arg
			continue
		}

		set max_args [dict get $ARGS($key) max_args]
		set min_args [dict get $ARGS($key) min_args]
		set check    [dict get $ARGS($key) validation]
		if {$value != {} && ![param validate $value $check]} {
			lappend remaining_args $arg
			log WARN {Parameter $arg supplied an illegal argument (failed $check check)}
			if {[cfg enabled EXIT_ON_ARG_FAILURE 1]} {
				puts stderr "Illegal argument passed for $arg ($value is not [text_utils::a_or_an $check] $check)"
				puts stderr "Try '[this_script] --help' for more information."
				exit 1
			}
			continue
		}

		# If the parameter can take many arguments, or the number of arguments remaining
		# is less than the max_args, then limit max_args to the number of remaining args.
		if {$max_args == {*} || $max_args + $i >= $l} {
			set max_args [expr {$l - $i - 1}]
		}

		for {set a 0} {$a < $max_args} {incr a} {
			set nextarg [lindex $arguments $i+1]
			if {[string match ${arg_prefix}* $nextarg]} {
				break
			}
			set nextarg [string trim $nextarg {\"'}]
			if {![param validate $nextarg $check]} {
				log DEV {Not considering $nextarg as argument of $arg as it failed validation check '$check'}
				if {[llength $value] < $min_args && [cfg enabled EXIT_ON_ARG_FAILURE 1]} {
					puts stderr "Illegal argument passed for $arg ($nextarg is not [text_utils::a_or_an $check] $check)"
					puts stderr "Try '[this_script] --help' for more information."
					exit 1
				}
				break
			}
			incr i
			lappend value $nextarg
		}

		if {[llength $value] < $min_args} {
			lappend remaining_args $arg
			log WARN {Parameter $arg does not supply enough arguments ([llength $value] out of $min_args)}
			if {[cfg enabled EXIT_ON_ARG_FAILURE 1]} {
				puts stderr "Error: not enough arguments supplied for parameter -$arg ([llength $value] out of $min_args)"
				puts stderr "Try '[this_script] --help' for more information."
				exit 1
			}
			continue
		}

		# If the maximum args is 1 then don't wrap the argument value in a list
		if {$max_args == 1} {
			if {[llength $value] > 1} {
				lappend remaining_args {*}[lrange $value 1 end]
			}
			set value [lindex $value 0]
		}

		lappend args_to_parse [list $key $value $arg]
	}

	return $args_to_parse
}

#
# Evaluates / performs the actions that should be taken when a given parameter
# is passed in to the application.
#
# @params                The parameters to evaluate, expected to be on the form
#                        of a list of lists containing parameter key, value and
#                        the original argument
#
proc arg_utils::_evaluate_params { params } {

	variable ARGS

	log DEV {Parameters to evaluate: $params}

	foreach param $params {

		lassign $param key values argument
		set value $values
		set opts $ARGS($key)

		set log_msg  [dict get $opts log]
		set body     [dict get $opts body]
		set ns       [dict get $opts namespace]
		set cfg_name [dict get $opts config]
		set cfg_val  [dict get $opts config_value]

		if {$cfg_name != {}} {
			if {$values != {}} {
				set cfg_val $values
			}
			if {$log_msg == {}} {
				switch -nocase -- $cfg_val {
				{true} -
				{yes} -
				{y} -
				{1} {
					set log_msg {Config $cfg_name enabled}
				}
				{false} -
				{no} -
				{n} -
				{0} {
					set log_msg {Config $cfg_name disabled}
				}
				default {
					set log_msg {Config $cfg_name set to '$cfg_val'}
				}
				}
				set log_msg
			}

			cfg set $cfg_name $cfg_val
		}

		if {$body != {}} {
			namespace eval $ns [subst {
				set value    \{$values\}
				set values   \{$values\}
				set argument \{$argument\}
				$body
			}]
		}

		log DEBUG $log_msg
	}
}

#
# Validate an argument value.
#
# Possible validation checks:
#    - any "string is" class   e.g. "integer", empty strings fail validation
#    - file                    input must be a regular file
#    - directory               input must be a directory (or not exist), the
#                              application using the parameter is responsible
#                              for creating the directory if it does not
#                              already exist
#    - directory_exists        input must be an existing directory
#    - anyfile                 input file must exists (i.e. file or directory)
#    - nofile                  input must not be a file
#
# List of "string is" classes supported:
#    alnum, alpha, ascii, control, boolean, digit,
#    double, entier, false, graph, integer, list,
#    lower, print, punct, space, true, upper,
#    wideinteger, wordchar, xdigit
#
# @param value               The argument value to validate
# @param validation_check    The validation check to apply
# @return                    1 if the value passed the validation check
#                            and 0 otherwise
#
opt::option param validate --params { value validation_check } --body {

	switch -- $validation_check {
	{} -
	{none} {
		return 1
	}
	{alpha} -
	{alnum} -
	{ascii} -
	{graph} -
	{upper} -
	{lower} -
	{print} -
	{wordchar} -
	{control} -
	{punct} -
	{space} -
	{bool} -
	{boolean} -
	{true} -
	{false} -
	{digit} -
	{int} -
	{integer} -
	{wideint} -
	{wideinteger} -
	{double} -
	{xdigit} {
		if {$value == {}} {
			return 0
		}
		return [string is $validation_check $value]
	}
	{file} {
		return [file isfile $value]
	}
	{directory} {
		# If the directory does not exist then it should be fine, the
		# application using the argument is responsible for creating
		# the directory if it doesn't exist.
		if {![file exists $value]} {
			return 1
		}
		return [file isdirectory $value]
	}
	{directory_exists} {
		return [file isdirectory $value]
	}
	{anyfile} {
		return [file exists $value]
	}
	{nofile} {
		return [expr {![file exists $value]}]
	}
	default {
		log WARN {Unknown validation check $validation_check for input '$value'}
	}
	}

	return 1
}

#
# Register an application parameter.
#
# Synopsis:
#    param register <short param> <long param> ?options?
#
# Example:
#    param register -d --dry-run --config DRY_RUN
#
# The above enables the config DRY_RUN when -d or --dry-run is passed in to the
# application.
#
# @param short_param         The short hand parameter, should be a single
#                            character
# @param long_param          The long version of the parameter
# @param args                Additional options
# @option --config           Specifies a config item to modify when the
#                            parameter is passed in to the application
# @option --config_value     The value to set the config item to when
#                            the parameter is passed in, defaults to 1
#                            (enabled)
# @option --override         If registering a parameter that already exists
#                            (like -d for example) then an error will be
#                            thrown. The --override flag allows for the
#                            application to override the existing parameter.
# @option --comment          The comment to display alongside the parameter
#                            when listing the help options
# @option --body             Specifies Tcl code that should be evaluated when
#                            the parameter is passed in (allows for more
#                            control). Existing variables: $values (or $value)
#                            containing the parameter argument (if any),
#                            $argument containing the original argument. The
#                            Tcl body will be evaluated in the namespace where
#                            the parameter was registered from. Do not use the
#                            "return" command within the body as this will
#                            cause parameter evaluation to stop abruptly
#                            resulting in undefined behaviour.
# @option --log              The message to log when the parameter has been
#                            evaluated. If not set and the parameter controls
#                            a config item then a default message is logged.
# @option --max_args         Sets the number of expected  arguments following
#                            the parameter, defaults to 0 (no arguments). This
#                            is primarily used when deducing whether
#                            unparameterised arguments (like files for example)
#                            belong as an argument to a parameter or not.
# @option --min_args         Sets the minimum number of arguments to following
#                            the parameter. If the parameter is passed in, but
#                            fails to supply enough arguments then an error is
#                            thrown.
# @option --validation       Apply validation to the parameter arguments. This
#                            can be used to check that a given input value is
#                            an integer, a file or directory for example.
#                            See arg_utils::param validate for available options.
# @option --priority         Certain parameters need to be run prior to others
#                            and this allows for some control over the order in
#                            which they are processed. Defaults to 50.
# @option --disporder        Allows for some control over the order in which
#                            the parameters are displayed when listing options
#                            on the help output. Defaults to 20.
# @option --more_level       Controls what parameters are displayed when the
#                            help section is listed. Defaults to 0, meaning
#                            the parameter info is displayed when the --help
#                            argument is passed to the application. If --help
#                            and --more is passed then parameters with a
#                            more level of 2 and below are displayed. If
#                            ---more is passed in the parameters with a
#                            more_level of 3 and below are displayed, etc.
# @option --unlisted         Never list the parameter in the help info, this is
#                            an alias for --more_level -1
# @option --morearg          Only list the parameter when --more is passed in,
#                            this is an alias for --more_level 1
#
#
opt::option param register --params { short_param long_param args } --body {

	variable ::arg_utils::ARGS

	# proc args
	let $args {
		override      0
		unlisted      {}
		morearg       {}
	}

	# split param if it happens to be one of these two forms:
	#    --clappend <cfg>=val
	#    --log-file=<file>
	set short [lindex $short_param 0]
	set short [lindex [split $short {=}] 0]
	set long  [lindex $long_param 0]
	set long  [lindex [split $long {=}] 0]
	set short [arg_utils::_clean_param $short]
	set long  [arg_utils::_clean_param $long]

	set opts [dict create]

	# param configuration
	foreach {default value} {
		comment       { }
		max_args      0
		min_args      0
		priority      50
		disporder     20
		body          {}
		validation    none
		config        {}
		config_value  1
		log           {}
		more_level    0
	} {
		dict set opts $default [param get $default $value]
	}

	dict set opts namespace [uplevel 1 {namespace current}]

	dict set opts param_long  [string trimleft $long_param  {-}]
	dict set opts param_short [string trimleft $short_param {-}]

	# This is in particular for the --help output to decide
	# whether to display a parameter or not
	if {$unlisted == 1} {
		dict set opts more_level -1
	}
	if {$morearg == 1} {
		dict set opts more_level 1
	}

	if {$short != {}} {
		foreach short_match [array names ARGS -glob $short,*] {
			lassign [split $short_match ,] x_short x_long

			if {!$override} {
				set msg "Trying to register parameter $short_param, $long_param, but it already exists as -$x_short, --$x_long. If the intention is to override this parameter then also supply --override."
				log ERROR {$msg}
				error $msg
			}

			if {$x_long == $long} {
				log DEBUG {Overriding app parameter -$short, --$long}
			} else {
				log DEBUG {Overriding short app parameter -$x_short, --$x_long with -$short, --$long}
				set ARGS(,$x_long) $ARGS($short_match)
			}
			unset ARGS($short_match)
		}
	}

	foreach long_match [array names ARGS -glob *,$long] {
		if {!$override} {
			set msg "Trying to register parameter $long_param, but it already exists. If the intention is to override this parameter then also supply --override."
			log ERROR {$msg}
			error $msg
		}
		log DEBUG {Overriding long app parameter --$long}

		unset ARGS($long_match)
	}

	if {$short != {} && $long != {}} {
		set disp "-$short, --$long"
	} elseif {$short == {}} {
		set disp --$long
	} else {
		set disp -$short
	}
	log DEV {Registering app param $disp}

	set ARGS($short,$long) $opts
}

#
# This allows for an application to update a registered parameter. A common
# use case is to alter the description of what a given parameter does.
#
# For example the default description for the --dry-run option may not be
# suitable for all applications.
#
# @param short_param         The short hand parameter, should be a single
#                            character
# @param long_param          The long version of the parameter
# @param args                Additional options to specify what needs to
#                            be changed
# @see                       param register for a list of
#                            available options
#
opt::option param update --params { short_param long_param args } --body {

	variable ::arg_utils::ARGS

	# proc args
	let $args {
		unlisted      {}
		morearg       {}
	}

	# split param if it happens to be one of these two forms:
	#    --clappend <cfg>=val
	#    --log-file=<file>
	set short [lindex $short_param 0]
	set short [lindex [split $short {=}] 0]
	set long  [lindex $long_param 0]
	set long  [lindex [split $long {=}] 0]
	set short [arg_utils::_clean_param $short]
	set long  [arg_utils::_clean_param $long]

	if {![info exists ARGS($short,$long)]} {
		error "Parameter $short_param, $long_param is not registered"
	}

	set opts $ARGS($short,$long)

	# param configuration
	foreach {default value} {
		comment       \0
		max_args      \0
		min_args      \0
		priority      \0
		disporder     \0
		body          \0
		validation    \0
		config        \0
		config_value  \0
		log           \0
		more_level    \0
	} {
		set p_value [param get $default $value]
		if {$p_value != \0} {
			dict set opts $default $p_value
		}
	}

	# This is in particular for the --help output to decide
	# whether to display a parameter or not
	if {$unlisted == 1} {
		dict set opts more_level -1
	}
	if {$morearg == 1} {
		dict set opts more_level 1
	}

	dict set opts param_long  [string trimleft $long_param  {-}]
	dict set opts param_short [string trimleft $short_param {-}]

	log DEBUG {Updating app param -$short, --$long}
	set ARGS($short,$long) $opts
}

#
# This will remove certain characters from a parameter name, such as for
# example the hyphen (-) and underscore (_) characters.
#
# This allows for parameters like --dryrun, --dry_run and --dry-run to be
# passed in and still match.
#
proc arg_utils::_clean_param { param } {

	# String hyphen and underscore from parameter
	set param [string map {- {} _ {}} $param]
}

#
# Initialises the arg_utils by setting up the application parameters.
#
# This is called automatically when the tapp library is being loaded.
#
proc arg_utils::init args {

	variable ARGS

	param register -d --dry-run\
		--config DRY_RUN\
		--disporder 10\
		--priority 10\
		--morearg\
		--comment {run through the script, but don't change anything}

	param register -v --verbose\
		--config VERBOSE\
		--disporder 30\
		--morearg\
		--comment {enable verbose output}

	param register {} --more\
		--priority 10\
		--disporder 35\
		--body {
			set more_level [expr {[string length $argument] - 4}]
			cfg set MORE_LEVEL $more_level
			log DEBUG {Enabled more output (more level $more_level)}
		}\
		--morearg\
		--comment {show more output, further dashes (e.g. ---more) show additional output (for applications where applicable)}

	param register {} --stdout\
		--config LOG_FILE\
		--config_value {stdout}\
		--disporder 55\
		--morearg\
		--log {Setting log file to standard out}\
		--comment {set log file to standard out}

	param register {} --stderr\
		--config LOG_FILE\
		--config_value {stderr}\
		--disporder 55\
		--morearg\
		--log {Setting log file to standard err}\
		--comment {set log file to standard err}

	param register {} {--<log level>}\
		--disporder 90\
		--morearg\
		--comment {enable logging at a specific log level:\ntrace, dev, debug, info, warn, error, fatal, mute}

	foreach level {trace dev debug info warn error fatal mute} {
		param register {} --$level\
			--priority 10\
			--unlisted\
			--config LOG_LEVEL\
			--config_value [string toupper $level]\
			--log "Enabled $level logging"
	}

	param register -h --help\
		--body { arg_utils::help }\
		--disporder 20\
		--comment {display this help, try including --more for additional options}

	param register -c --config\
		--body {
			cfg file $value
			log DEBUG {Using $value as configuration file}
		}\
		--max_args 1\
		--min_args 1\
		--validation {file}\
		--disporder 60\
		--priority 10\
		--morearg\
		--comment {specify the configuration file to load}

	param register {} {--cset <cfg>=1}\
		--max_args 1\
		--body {
			lassign [split $values {=}] cfg_name cfg_value
			catch {
				set cfg_value [subst $cfg_value]
			}
			log DEBUG {Setting config item $cfg_name to $cfg_value}
			cfg set $cfg_name $cfg_value
		}\
		--disporder 60\
		--morearg\
		--comment {override a specific configuration item}

	param register {} {--cappend <cfg>=val}\
		--max_args 1\
		--body {
			lassign [split $values {=}] cfg_name cfg_value
			catch {
				set cfg_value [subst $cfg_value]
			}
			log DEBUG {Appending \"$cfg_value\" to config item $cfg_name}
			cfg append $cfg_name $cfg_value
		}\
		--disporder 60\
		--morearg\
		--comment {append a value to a specific configuration item}

	param register {} {--clappend <cfg>=val}\
		--max_args 1\
		--body {
			lassign [split $values {=}] cfg_name cfg_value
			catch {
				set cfg_value [subst $cfg_value]
			}
			log DEBUG {Appending \"$cfg_value\" to config item $cfg_name}
			cfg lappend $cfg_name {*}$cfg_value
		}\
		--disporder 60\
		--morearg\
		--comment {list append a value to a specific configuration item}

	param register {} {--log-file=<file>}\
		--config LOG_FILE\
		--max_args 1\
		--disporder 90\
		--morearg\
		--comment {override file to log to}

	param register {} {--log-dir=<dir>}\
		--config LOG_DIR\
		--max_args 1\
		--disporder 90\
		--morearg\
		--comment {override directory to log to}

	param register {} {--log-prefix=<text>}\
		--config LOG_PREFIX\
		--max_args 1\
		--disporder 90\
		--morearg\
		--comment {specify a log prefix, for cross-script calls}

	param register {} --log-proc\
		--config LOG_PROC_PREFIX\
		--disporder 90\
		--morearg\
		--comment {also output the Tcl procedure doing the logging}

	param register {} --log_enable_stdout\
		--config LOG_DISABLE_STD_STREAMS\
		--config_value 0\
		--disporder 90\
		--morearg\
		--comment {enable logging to standard out (refers to stdout log statements)}

	param register {} --log_disable_stdout\
		--config LOG_DISABLE_STD_STREAMS\
		--config_value 1\
		--disporder 90\
		--morearg\
		--comment {disable logging to standard out (refers to stdout log statements)}

	param register {} {--show-config ?<cfg>?}\
		--body {
			if {$values == {}} {
				set values {*}
			}
			foreach filter $values {
				log STDOUT [cfg print $filter]
			}
		}\
		--disporder 60\
		--morearg\
		--max_args *\
		--comment {dump configuration items currently set; accepts multiple glob patterns\
			   as arguments which can be used to pull out individual config items,\
			   defaults to listing all configuration items}

	param register {} {--trace_var=<var>}\
		--body {
			trace add variable $value array tapp_lang::_watch
			trace add variable $value write tapp_lang::_watch
			trace add variable $value read  tapp_lang::_watch
			trace add variable $value unset tapp_lang::_watch
			log DEBUG {Enabled tracing of variable: $value}
		}\
		--disporder 60\
		--morearg\
		--comment {enable tracing of when a given variable is set and read}

	param register {} {--trace_cmd=<cmd>}\
		--body {
			trace add execution $value enter tapp_lang::_watch_command_enter
			trace add execution $value leave tapp_lang::_watch_command_leave
			log DEBUG {Enabled tracing of command: $value}
		}\
		--disporder 60\
		--morearg\
		--comment {enable tracing of when a given procedure is entered and left}

	param register {} --assert\
		--body {
			set ::tapp::ASSERT 1
			log DEV {Enabled assert statements}
		}\
		--disporder 65\
		--morearg\
		--comment [subst {enable assert statements [expr {[info exists ::tapp::ASSERT] && $::tapp::ASSERT} ? \{(default)\} : \{\}]}]

	param register {} --no_assert\
		--body {
			set ::tapp::ASSERT 0
			log DEV {Disabled assert statements}
		}\
		--disporder 65\
		--morearg\
		--comment [subst {disable assert statements [expr {[info exists ::tapp::ASSERT] && !$::tapp::ASSERT} ? \{(default)\} : \{\}]}]

	param register {} {--dest=<dir>}\
		--config DESTINATION\
		--max_args 1\
		--validation {directory}\
		--log {Destination set to $value}\
		--disporder 68\
		--morearg\
		--comment {set destination for scripts where this is relevant}
}


