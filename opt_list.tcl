# Copyright (c) 2014 Stein Gunnar Bakkeby all rights reserved.

package provide tapp_lib 1.0

namespace eval opt_list {

}

#
# Option preset: list
#
# This will set / define the following proc options:
#     <proc> init                   Initialises the list (setting it to empty)
#     <proc> add <value>            Adds a value to the given list. If the opt
#                                   proc is initialised with both the list
#                                   preset and contains the flag "-unique" then
#                                   the value will only be added if it does not
#                                   already exist. Returns the list index where
#                                   the value was added / exists.
#     <proc> get <index>            Retrieves a value from the list with a
#                                   given "ID" or index.
#     <proc> remove <index>         Removes a value from the list with a given
#                                   "ID" or index.  If this is a unique list
#                                   then the removed value will be replaced by
#                                   an empty string in order to preserve the
#                                   index order. Otherwise deleting a value
#                                   will shift the index order for all values
#                                   following the deleted value.
#     <proc> all <indexes>          Retrieves values from multiple indexes. The
#                                   returned values are returned in the order
#                                   of the given indexes.
#     <proc> reset                  Clears / empties the list
#     <proc> id <value> ?<opt>?     Retrieves the "ID" / index of the given
#                                   value by applying lsearch. See lsearch for
#                                   additional search options.
#     <proc> load <values> ?<opt>?  Loads multiple entries into the list. If
#                                   the option "-unique" is included as an
#                                   an option then each value will only be
#                                   added if it does not already exist.
#     <proc> unload                 Returns the list data
#
# @param pname           The proc to add the options to
# @see                   opt::proc
#
proc opt_list::preset { pname args } {

	set LIST [opt::get_namespace_var $pname]
	set UNIQUE [expr {[param get -unique] != {}}]
	
	opt::option $pname init --params { args } --body [subst -nocommands {
		set $LIST {}
	}]
	
	opt::option $pname add --params { value args } --body [subst {
		[expr {$UNIQUE ?
		"set idx \[lsearch -exact \$$LIST \$value\]
		if {\$idx != -1} {
			return \$idx
		}" : ""}]
		set idx \[llength \$$LIST\]
		lappend $LIST \$value

		return \$idx
	}]
	
	opt::option $pname get --params { id args } --body [subst -nocommands {
		return [lindex \$$LIST \$id]
	}]
	
	opt::option $pname remove --params { id args } --body [subst {
		set $LIST \[lreplace \$$LIST \$id \$id [expr {$UNIQUE ? "{}" : ""}]
		return \$$LIST
	}]
	
	opt::option $pname all --params { ids args } --body [subst -nocommands {
		touch values
		foreach id \$ids {
			lappend values [$pname get \$id {*}\$args]
		}
		return \$values
	}]
	
	opt::option $pname id --params { value args } --body [subst -nocommands {
		return [lsearch {*}\$args \$$LIST \$value]
	}]

	opt::option $pname reset --params { args } --body [subst {
		set $LIST {}
	}]
	
	opt::option $pname load --params { values args } --body [subst -nocommands {
		touch output
		foreach value \$values {
			lappend output [$pname add \$value]
		}
		return \$output
	}]

	opt::option $pname unload --params { args } --body [subst {
		return \$$LIST
	}]
	
	opt::option $pname destroy --params { args } --body [subst -nocommands {
		unset $LIST
		opt::destroy $pname
	}]
	
	$pname init {*}$args
}
