#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require tcltest
namespace import tcltest::test

package require tapp 1.0

incr test
test string_capitalize.${test} {capitalize oK} { return [string_capitalize {oK}] } {Ok}

incr test
test string_capitalize.${test} {capitalize A} { return [string_capitalize {A}] } {A}

incr test
test string_capitalize.${test} {capitalize \\} { return [string_capitalize {\\}] } {\\}

incr test
test string_capitalize.${test} {capitalize the.big_bad-WOLF#wAs/here} { return [string_capitalize {the.big_bad-WOLF#wAs/here}] } {The.Big_Bad-Wolf#Was/Here}

incr test
test string_capitalize.${test} {capitalize rosemary's} { return [string_capitalize {rosemary's}] } {Rosemary's}

incr test
test string_capitalize.${test} {capitalize somewhere.[2003]} { return [string_capitalize {somewhere.[2003]}] } {Somewhere.[2003]}

incr test
test convert_clock_format_to_regexp.${test} {test various format groups} {
	
	set format_groups {
		%a
		%A
		%b
		%B
		%c
		%C
		%d
		%D
		%e
		%Ec
		%EC
		%EE
		%Ex
		%EX
		%Ey
		%EY
		%g
		%G
		%h
		%H
		%I
		%j
		%J
		%k
		%l
		%m
		%M
		%N
		%Od
		%Oe
		%OH
		%OI
		%Ok
		%Om
		%OM
		%OS
		%Ou
		%Ow
		%Oy
		%p
		%P
		%Q
		%r
		%R
		%s
		%S
		%t
		%T
		%u
		%U
		%V
		%w
		%W
		%x
		%X
		%y
		%Y
		%z
		%Z
		%%
		%+
		%Y.%m.%d
		Time_is_%H:%M_for_%d/%m
	}

	set now [clock scan {2012-01-01 00:00:00}]
	for {set d 0} {$d < 7} {incr d} {
		for {set m 0} {$m < 12} {incr m} {
			set time [expr {$now + 86400 * $d + 86400 * 30 * $m}]
			foreach group $format_groups {
				set formatted_time [clock format $time -format $group]
				set regexp [convert_clock_format_to_regexp $group]
				if {![regexp $regexp $formatted_time]} {
					puts "convert_clock_format_to_regexp failed for $formatted_time ($group) using regexp $regexp"
					return 0
				}
			}
		}
	}
	return 1
} {1}

incr test
test wrap_text.${test} {normal word wrap example} {
	set text {The quick brown fox jumps over the lazy dog}
	set expected [subst {The quick brown fox\njumps over the lazy\ndog}]
	return [expr {$expected == [text_utils::wrap_text $text 20]}]
} {1}

incr test
test wrap_text.${test} {edge case, width is less than largest word} {
	set text {The quick brown fox jumps over the lazy dog}
	set expected [subst {The\nquick\nbrown\nfox\njumps\nover\nthe\nlazy\ndog}]
	return [expr {$expected == [text_utils::wrap_text $text 4]}]
} {1}

incr test
test wrap_text.${test} {edge case, word separator does not exist in text} {
	set text {Thequickbrownfoxjumpsoverthelazydog}
	set expected $text
	return [expr {$expected == [text_utils::wrap_text $text 4]}]
} {1}

incr test
test wrap_text.${test} {edge case, word separator does not exist in text} {
	set text {The__quick__brown__fox__jumps__over__the__lazy__dog}
	set expected [subst {The__quick__brown\nfox__jumps__over\nthe__lazy__dog}]
	return [expr {$expected == [text_utils::wrap_text $text 20 {__}]}]
} {1}

incr test
test index_replace.${test} {Replace individual characters in string} {
	set str {------------------------------}
	set idx {0 13 17 22 29}
	return [text_utils::index_replace $str $idx {|}]

} {|------------|---|----|------|}


incr test
test index_replace.${test} {Replace individual characters in string} {
	set str {------------------------------}
	set idx {0 13 17 22 29}
	return [text_utils::index_replace $str $idx {||}]

} {||-----------||--||---||-----||}

incr test
test pad.${test} {Left-pad a given string} {
	set str "The quick brown fox\njumps over the lazy dog"
	return [text_utils::pad $str [string repeat { } 8]]
} "        The quick brown fox\n        jumps over the lazy dog"

incr test
test pad.${test} {Right-pad a given string} {
	set str "The quick brown fox\njumps over the lazy dog"
	return [text_utils::pad $str [string repeat { } 8] right]
} "The quick brown fox        \njumps over the lazy dog        "