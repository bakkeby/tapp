#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require tcltest
package require tapp 1.0

namespace import tcltest::test

incr test
test get_next_log_rotation_time.${test} {year} {
	set now [clock scan {2014-07-23 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2015-01-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time year $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {month} {
	set now [clock scan {2014-01-29 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-02-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time month $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {month, year change} {
	set now [clock scan {2014-12-29 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2015-01-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time month $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {day} {
	set now [clock scan {2014-07-23 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-24 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time day $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {day, month change} {
	set now [clock scan {2014-01-31 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-02-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time day $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {day, year change} {
	set now [clock scan {2014-12-31 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2015-01-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time day $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {hour} {
	set now [clock scan {2014-07-23 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-23 10:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time hour $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {hour, day change} {
	set now [clock scan {2014-07-23 23:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-24 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time hour $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {hour, month change} {
	set now [clock scan {2014-07-31 23:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-08-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time hour $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {hour, year change} {
	set now [clock scan {2014-12-31 23:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2015-01-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time hour $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {minute} {
	set now [clock scan {2014-07-23 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-23 09:14:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time minute $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {minute, hour change} {
	set now [clock scan {2014-07-23 09:59:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-23 10:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time minute $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {minute, day change} {
	set now [clock scan {2014-07-23 23:59:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-24 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time minute $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {minute, month change} {
	set now [clock scan {2014-07-31 23:59:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-08-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time minute $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {minute, year change} {
	set now [clock scan {2014-12-31 23:59:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2015-01-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time minute $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {second} {
	set now [clock scan {2014-07-23 09:13:27} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-23 09:13:28} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time second $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {second, minute change} {
	set now [clock scan {2014-07-23 09:13:59} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-23 09:14:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time second $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {second, hour change} {
	set now [clock scan {2014-07-23 09:59:59} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-23 10:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time second $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {second, day change} {
	set now [clock scan {2014-07-23 23:59:59} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-07-24 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time second $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {second, month change} {
	set now [clock scan {2014-07-31 23:59:59} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2014-08-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time second $now]
	return [expr {$exp == $res}]
} {1}

incr test
test get_next_log_rotation_time.${test} {second, year change} {
	set now [clock scan {2014-12-31 23:59:59} -format {%Y-%m-%d %H:%M:%S}]
	set exp [clock scan {2015-01-01 00:00:00} -format {%Y-%m-%d %H:%M:%S}]
	set res [tapp_log::get_next_log_rotation_time second $now]
	return [expr {$exp == $res}]
} {1}

