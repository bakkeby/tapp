#!/bin/sh
# Copyright (c) 2014 Stein Gunnar Bakkeby all rights reserved.
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require tcltest
package require tapp 1.0

namespace import tcltest::test

incr test
test reset.${test} {check reset} {
	tapp_lang::_reset_all
	return [concat [info vars] [info locals]]
} {}

incr test
test nvl.${test} {check nvl non-existing variable (form 1)} {
	catch {unset does_not_exist}
	return [tapp_lang::nvl {$does_not_exist, -1}]
} {-1}

incr test
test nvl.${test} {check nvl non-existing variable (form 2)} {
	catch {unset does_not_exist}
	return [tapp_lang::nvl {$does_not_exist , -2}]
} {-2}

incr test
test nvl.${test} {check nvl non-existing variable (form 3)} {
	catch {unset does_not_exist}
	return [tapp_lang::nvl {$does_not_exist -3}]
} {-3}

incr test
test nvl.${test} {check nvl non-existing variable (form 4)} {
	catch {unset does_not_exist}
	return [tapp_lang::nvl {$does_not_exist} -4]
} {-4}

incr test
test nvl.${test} {check nvl existing variable (form 1)} {
	set does_exist -1
	return [tapp_lang::nvl {$does_exist, -5}]
} {-1}

incr test
test nvl.${test} {check nvl existing variable (form 2)} {
	set does_exist -2
	return [tapp_lang::nvl {$does_exist , -6}]
} {-2}

incr test
test nvl.${test} {check nvl existing variable (form 3)} {
	set does_exist -3
	return [tapp_lang::nvl {$does_exist -7}]
} {-3}

incr test
test nvl.${test} {check nvl existing variable (form 4)} {
	set does_exist -4
	return [tapp_lang::nvl {$does_exist} -8]
} {-4}

incr test
test default.${test} {check that default can set variable} {
	default VAR(minus_two) -2
	return $VAR(minus_two)
} -2

incr test
test default.${test} {check that default can set variable to default} {
	default VAR(empty)
	return $VAR(empty)
} {}

incr test
test default.${test} {check that default does not set existing value} {
	set VAR(existing_value) existing
	default VAR(existing_value) {default value}
	return $VAR(existing_value)
} {existing}

incr test
test touch.${test} {check that touch actually initialises the provided variable} {
	touch test_var.${test} 
	return [info exists test_var.${test}]
} {1}

incr test
test touch.${test} {check that touch initialises multiple variables} {
	touch var1.${test} var2.${test} var3.${test}
	return [info exists var1.${test}][info exists var2.${test}][info exists var3.${test}]
} {111}

incr test
test touch.${test} {check that touch does not override an already set value} {
	set var1.${test} 99
	touch var1.${test} var2.${test} 
	return [info exists var1.${test}][info exists var2.${test}]|[set var1.${test}]|[set var2.${test}]
} {11|99|}

incr test
test touch.${test} {check that touch can set a variable that is just integers} {
	touch 234 567
	return [info exists 234][info exists 567]|[set 234]|[set 567]
} {11||}

incr test
test clear.${test} {check that clear actually initialises the provided variable} {
	clear test_var.${test} 
	return [info exists test_var.${test}]
} {1}

incr test
test clear.${test} {check that clear initialises multiple variables} {
	clear var1.${test} var2.${test} var3.${test}
	return [info exists var1.${test}][info exists var2.${test}][info exists var3.${test}]
} {111}

incr test
test clear.${test} {check that clear does override an already set value} {
	set var1.${test} 99
	clear var1.${test} var2.${test} 
	return [info exists var1.${test}][info exists var2.${test}]|[set var1.${test}]|[set var2.${test}]
} {11||}

incr test
test do.${test} {check that the do command while loop works} {
	set i 0
	set result {}
	do {
		incr i
		lappend result $i
	} while {$i < 3}
	return $result
} {1 2 3}

incr test
test do.${test} {check that the do command while loop works with break (within first block)} {
	set i 0
	set result {}
	do {
		incr i
		lappend result $i
		if {$i == 1} {
			break
		}
	} while {$i < 3}
	return $result
} {1}

incr test
test do.${test} {check that the do command while loop works with break (within while)} {
	set i 0
	set result {}
	do {
		incr i
		lappend result $i
		if {$i == 2} {
			break
		}
	} while {$i < 3}
	return $result
} {1 2}

incr test
test do.${test} {check that the do command while loop works with continue (within first block)} {
	set i 0
	set result {}
	do {
		incr i
		if {$i == 1} {
			continue
		}
		lappend result $i
	} while {$i < 3}
	return $result
} {2 3}

incr test
test do.${test} {check that the do command while loop works with continue (within while)} {
	set i 0
	set result {}
	do {
		incr i
		if {$i == 2} {
			continue
		}
		lappend result $i
	} while {$i < 3}
	return $result
} {1 3}

incr test
test lremove.${test} {check that the lremove command can remove empty elements from a list} {
	set list {a b {} c {} {} d {} {} {} e {} f {} {} g}
	return [lremove $list {}]
} {a b c d e f g}

incr test
test lremove.${test} {check that the lremove command can remove a given element from a list} {
	set list {a b {} c {} {} d {} {} {} e {} f {} {} g}
	return [lremove $list {d}]
} {a b {} c {} {} {} {} {} e {} f {} {} g}

incr test
test lremove.${test} {check that the lremove command can handle an empty list} {
	return [lremove {} {}]
} {}

incr test
test lremove.${test} {check that the lremove command does not change a list where the element does not exist} {
	return [lremove {a b d e f g h i j k l m n o p q r s t u v w x y z} {c}]
} {a b d e f g h i j k l m n o p q r s t u v w x y z}

incr test
test lremove.${test} {try to remove multiple items from a list using lremove} {
	return [lremove {a b c d e f g h i j k l m n o p q r s t u v w x y z} a e i o u y]
} {b c d f g h j k l m n p q r s t v w x z}

incr test
test lremove.${test} {try to remove elements using a glob pattern} {
	return [lremove {aei beo ceo dra mni echo tad} *e*]
} {dra mni tad}

incr test
test larrange.${test} {try to prioritise elements using a glob pattern} {
	return [larrange -glob -prioritise -list {aei beo ceo dra mni echo tad} -pattern *e*]
} {aei beo ceo echo dra mni tad}

incr test
test larrange.${test} {try to prioritise elements using a regexp pattern} {
	return [larrange -regexp -prioritise -list {aei beo ceo dra mni echo tad} -pattern {^ceo$}]
} {ceo aei beo dra mni echo tad}

incr test
test larrange.${test} {try to deprioritise elements using a glob pattern} {
	return [larrange -glob -deprioritise -list {aei beo ceo dra mni echo tad} -pattern *e*]
} {dra mni tad aei beo ceo echo}

incr test
test larrange.${test} {try to deprioritise elements using a regexp pattern} {
	return [larrange -regexp -deprioritise -list {aei beo ceo dra mni echo tad} -pattern {^ceo$}]
} {aei beo dra mni echo tad ceo}

incr test
test larrange.${test} {try to deprioritise elements using a regexp pattern containing a space} {
	return [larrange -regexp -deprioritise -list {aei beo {c o} dra mni echo tad} -pattern {^c o$}]
} {aei beo dra mni echo tad {c o}}

incr test
test larrange.${test} {try to deprioritise elements using multiple regexp patterns} {
	return [larrange -regexp -deprioritise -list {aei beo {c o} dra mni echo tad} -patterns {aei {^c o$}}]
} {beo dra mni echo tad aei {c o}}

incr test
test decr.${test} {check that the decr command can handle decrement} {
	catch {unset var}
	return [decr var]
} -1

incr test
test decr.${test} {check that the decr command can handle decrement argument} {
	set var 5
	return [decr var 3]
} 2

incr test
test decr.${test} {check that the decr command can handle decrement} {
	set var 5
	return [decr var -3]
} 8

incr test
test decr.${test} {check that the decr command leave existing variable} {
	catch {unset var}
	decr var -3
	return $var
} 3

incr test
test prepend.${test} {check that the prepend command prepends values} {
	catch {unset var}
	set var {World!}
	prepend var {Hello }
	return $var
} {Hello World!}

incr test
test lsplit.${test} {check that the lsplit command can split a list into two} {
	lsplit {a 1 b 2 c 3 d 4 e 5} letters numbers
	return [list $letters $numbers]
} {{a b c d e} {1 2 3 4 5}}

incr test
test lsplit.${test} {edge case, splitting a list into one which makes no sense} {
	lsplit {a 1 b 2 c 3 d 4 e 5} list
	return $list
} {a 1 b 2 c 3 d 4 e 5}

incr test
test lsplit.${test} {splitting a list into four} {
	lsplit {a 1 b 2 c 3 d 4 e 5 f 6} l1 l2 l3 l4
	return [list $l1 $l2 $l3 $l4]
} {{a c e} {1 3 5} {b d f} {2 4 6}}

incr test
test lsplit.${test} {splitting a list into three} {
	lsplit {a 1 b 2 c 3 d 4 e 5 f 6} l1 l2 l3
	return [list $l1 $l2 $l3]
} {{a 2 d 5} {1 c 4 f} {b 3 e 6}}

incr test
test lsplit.${test} {splitting a list into five, with insufficient values} {
	lsplit {a 1 b 2 c 3 d 4 e 5 f 6} l1 l2 l3 l4 l5
	return [list $l1 $l2 $l3 $l4 $l5]
} {{a 3 f} {1 d 6} {b 4 {}} {2 e {}} {c 5 {}}}
