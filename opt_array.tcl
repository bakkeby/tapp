# Copyright (c) 2014 Stein Gunnar Bakkeby all rights reserved.

package provide tapp_lib 1.0

namespace eval opt_array {
	
}

#
# Option preset: array
#
# This will enable the following proc options:
#     <proc> init <location>        Initialises the array and sets a given
#                                   file location used for saving and loading
#                                   the array data to/from file
#     <proc> set <key> <value>      Sets a key in the array with a given value
#     <proc> get <key>              Retrieves a value from the array with a
#                                   given key
#     <proc> remove <key>           Removes a key / value from the array. Can
#                                   also be a string match pattern.
#     <proc> reset                  Clears / empties the array
#     <proc> dirty                  Returns 1 if the array has been altered
#                                   since it was first initialised or loaded,
#                                   returns 0 otherwise
#     <proc> save                   Saves the content of the array as plain
#                                   text to file defined by the location the
#                                   proc was initialised with
#     <proc> load                   Loads array data from the file location
#                                   specified by the init option
#     <proc> delete                 Deletes the file at the given location.
#                                   This action is irreversible, use with care.
#     <proc> keys                   Returns the available keys in the array,
#                                   equivalent of "array names"
#     <proc> location               Returns the file location where the array
#                                   data is stored (if defined)
#     <proc> exists <key>           Checks if a key exists in the array (as
#                                   opposed to "array exists" which checks
#                                   whether the array variable exists or not)
#
# @param pname           The proc to add the options to
# @see                   opt::proc
#
proc opt_array::preset { pname args } {

	set PNAME [opt::get_namespace_var $pname]

	array set $PNAME {}
	
	opt::option $pname init --params { location args } --body [subst {
		array set $PNAME {}
		set CFG_PREFIX [lindex [split ${PNAME} {:}] end]
		cfg set \${CFG_PREFIX}_FILE_LOCATION \$location
	}]
	
	opt::option $pname set --params { key value args } --body [subst {
		set ${PNAME}(\$key) \$value
		set ${PNAME}_DIRTY 1
	}]
	
	opt::option $pname get --params { key args } --body [subst -nocommands {
		set value [lindex \$args 0]
		if {[info exists ${PNAME}(\$key)]} {
			set value [set ${PNAME}(\$key)]
		}
		return \$value
	}]
	
	opt::option $pname remove --params { key args } --body [subst {
		array unset $PNAME \$key
		set ${PNAME}_DIRTY 1
	}]
	
	opt::option $pname reset --params { args } --body [subst {
		array unset $PNAME
		set ${PNAME}_DIRTY 1
	}]
	
	opt::option $pname dirty --params { args } --body [subst -nocommands {
		if {[info exists ${PNAME}_DIRTY]} {
			return [set ${PNAME}_DIRTY]
		}
		return 0
	}]
	
	opt::option $pname save --params { args } --body [subst -nocommands {
		if {![opt::_parse $pname dirty]} {
			return
		}
		if {![info exists $PNAME]} {
			return
		}
		set filename [opt::_parse $pname location]
		if {\$filename != {}} {
			file_utils::save_array $PNAME \$filename
		}
	}]
	
	opt::option $pname load --params { args } --body [subst -nocommands {
		file_utils::load_array $PNAME [opt::_parse $pname location]
		set ${PNAME}_DIRTY 0
	}]
	
	opt::option $pname unload --params { args } --body [subst -nocommands {
		return [array get $PNAME]
	}]
	
	opt::option $pname keys --params { args } --body [subst -nocommands {
		set ret {}
		if {[info exists $PNAME]} {
			set ret [array names $PNAME]
		}
		return \$ret
	}]
	
	opt::option $pname location --params { args } --body [subst -nocommands {
		set CFG_PREFIX [lindex [split ${PNAME} {:}] end]
		return [cfg get \${CFG_PREFIX}_FILE_LOCATION {}]
	}]

	opt::option $pname exists --params { key args } --body [subst -nocommands {
		return [info exists ${PNAME}(\$key)]
	}]
	
	opt::option $pname delete --params { args } --body [subst -nocommands {
		set filename [opt::_parse $pname location]
		
		if {\$filename != {}} {
			file_utils::delete_file \$filename
		}
	}]
	
	opt::option $pname destroy --params { args } --body [subst -nocommands {
		unset -nocomplain -- $PNAME
		unset -nocomplain -- ${PNAME}_DIRTY
		opt::destroy $pname
	}]
}
