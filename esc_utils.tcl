# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide escape_utils 1.0

namespace eval escape {
	namespace export escape
	opt::proc escape

	variable bash_escape_map {
		{ } {\ }
		{&} {\&}
		{'} {\'}
		\"  {\"}
		{>} {\>}
		{<} {\<}
		{|} {\|}
		{(} {\(}
		{)} {\)}
		{!} {\!}
		{[} {\[}
		{]} {\]}
	}
	
	variable entity_escape_map {
		\"  {&quot;}
		{&} {&amp;} 
		{<} {&lt;}
		{>} {&gt;}
		{'} {&apos;}
	}
}

#
# Escapes a string for bash output.
#
# Synopsis:
#    escape bash <input>
#
# @param input           The input string to escape
# @return                The input string escaped for bash output
#
opt::option ::escape::escape bash --params { input } --body {
	return [string map $::escape::bash_escape_map $input]
}

#
# Unescapes bash input.
#
# Synopsis:
#    unescape bash <input>
#
# @param input           The bash string to unescape
# @return                The input string unescaped
#
opt::option ::escape::unescape bash --params { input } --body {
	return [string map [lreverse $::escape::bash_escape_map] $input]
}

#
# Escapes a string for entity output.
#
# Synopsis:
#    escape entity <input>
#
# @param input           The input string to escape
# @return                The input string escaped for entity output
#
opt::option ::escape::escape entity --params { input } --body {
	return [string map $::escape::entity_escape_map $input]
}

#
# Unescapes entity input.
#
# Synopsis:
#    unescape entity <input>
#
# @param input           The entity string to unescape
# @return                The input string unescaped
#
opt::option ::escape::unescape entity --params { input } --body {
	return [string map [lreverse $::escape::entity_escape_map] $input]
}