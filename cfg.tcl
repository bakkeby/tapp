# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide tapp_lib 1.0

#
# Configuration facility.
#
# The idea with this configuration package is to use the same naming convention
# as the array proc uses, i.e. we have cfg get, cfg set, cfg names etc.
#
# A rather specific feature that is worth noting is that it is possible to have
# a configuration file automatically generated if it doesn't already exist when
# the application starts.
# If the configuration file has the same name as the application namespace, and
# the namespace contains a proc named "write_default_config", then that proc
# will be called (assuming that it will write the default configuration to
# disk).
# 
# @cfg MASKED_CFG                List of specific configuration items that
#                                should not be listed in full when doing cfg
#                                print. Defaults to HELP, ARGS and MOREARGS as
#                                these tend to contain a lot of data and is not
#                                particularly useful for debugging purposes.
# @cfg CFG_MULTI_LINE_SEPARATOR  Configuration items can span multiple lines if
#                                they have a trailing backslash at the end of
#                                each line. In most cases it is just fine to
#                                join each line adding up to one long string.
#                                Typically this will be a list of values to set
#                                a given configuration item to. In some special
#                                cases, however, the lines in themselves might
#                                be of relevance and the application may need
#                                to parse each line separately. This is where
#                                this configuration item comes in. By default
#                                this will be set to a space character meaning
#                                that each line is joined by a space. Setting
#                                the config to a pipe character or a null value
#                                (\u0) for example would allow the application
#                                to split the config value based on this
#                                character in order to process each line as a
#                                list.
#
namespace eval cfg_utils {
	namespace export cfg
	variable ::cfg_utils::CFG
	variable ::cfg_utils::SOURCED_CFG_FILES {}
	
	opt::proc ::cfg_utils::cfg
}

#
# Loads a configuration file.
#
# Specifies a configuration file to load configuration from.
#
# Lines starting with a hash (#) are being treated as comments and will be
# ignored.
#
# Lines starting with !include specifies other configuration files to be
# loaded. This can be used to store the common settings in a base configuration
# file and have environment specific files that only specifies configuration
# relevant for the given environment. The !include statements are processed as
# soon as they are found and loaded configuration files may themselves contain
# further !include statements.
#
# Infinite loops are automatically detected and resolved. Configuration items
# set after a file has been loaded will override those set in the included
# file(s).
#
# Multi-line configuration items are supported and will need a trailng
# backslash.
#
# When a file is being loaded any configuration items previously set in the
# file is available through a Tcl variable with the same name. This does not
# work cross file loads, i.e. the configuration items set in an included file
# will not be available as a variable. Consider using configuration subsitution
# placeholders instead (e.g. %CONFIG_ITEM%).
#
# @param file_name       The name of the file to load
# @see                   cfg subst for more information on substitution
#                        variables
#
opt::option ::cfg_utils::cfg file --params { file_name } --body {

	variable ::cfg_utils::SOURCED_CFG_FILES
	
	set file_name [file normalize $file_name]
	log DEBUG {Loading config file $file_name}

	if {![file exists $file_name]} {
		log DEBUG {Config file $file_name does not exist}
		set cfg_gen_proc ::[file rootname [file tail $file_name]]::write_default_config
		if {[info commands $cfg_gen_proc] == {}} {
			return
		}
		log STDOUT {Warning: The configuration file $file_name does not exist}
		log STDOUT {Generating default configuration file $file_name\n}
		$cfg_gen_proc $file_name
		if {![file exists $file_name]} {
			log WARNING {Configuration file still does not exist, ignoring.}
			return
		}
	}

	# Mechanism to prevent the same config file from being loaded more
	# than once (preventing infinite loops).
	if {[lsearch $::cfg_utils::SOURCED_CFG_FILES $file_name] != -1} {
		return
	}
	lappend ::cfg_utils::SOURCED_CFG_FILES $file_name

	set file_data [file_utils::read_file $file_name]

	# Process config file
	set data [split $file_data "\n"]
	touch cfg_name cfg_value
	set multi_line 0
	set multi_line_separator [cfg get CFG_MULTI_LINE_SEPARATOR { }]
	foreach line $data {

		set line [string trim $line]
		if {[string match {#*} $line]} {
			continue
		}

		# Lines starting with !include indicate other
		# configuration files that should also be sourced.
		if {[string match {!include *} $line]} {
			set incl_name [string range $line 9 end]
			
			# If included file has a relative path, then determine the
			# absolute path based on the current config file.
			if {[file path $incl_name] == {relative}} {
				set incl_name [file join [file dir $file_name] $incl_name]
			}
			cfg file $incl_name
			continue
		} 

		if {$multi_line} {
			append cfg_value $line
		} elseif {![regexp {^\s*([A-Za-z0-9_-]+)\s*=\s*(.*)\s*$} $line --> cfg_name cfg_value]} {
			continue
		}

		# Removes the trailing \ of the line (for multi-line configs)
		set multi_line [regsub {\\$} $cfg_value $multi_line_separator cfg_value]

		if {!$multi_line} {
			if {[catch {set cfg_value [subst $cfg_value]} msg]} {
				::tapp_log::log DEBUG {Unable to substitute $cfg_name configuration value due to: $msg}
			}
			cfg set $cfg_name $cfg_value
			set $cfg_name $cfg_value
			clear cfg_name cfg_value
		}
	}
	
	# Exception, if the last config ends with a trailing backslash then it is treated
	# as a multi-line, but we don't have any more lines to process. Catch this here.
	if {$multi_line} {
		if {[catch {set cfg_value [subst $cfg_value]} msg]} {
			::tapp_log::log DEBUG {Unable to substitute $cfg_name configuration value due to: $msg}
		}
		cfg set $cfg_name $cfg_value
		set $cfg_name $cfg_value
		clear cfg_name cfg_value
	}
}

#
# Collate configuration items for print purposes.
#
# This is mainly used for debugging purposes. The proc will generate a string
# output that lists all configuration items and their values. This in turn can
# be logged or printed to standard out.
#
# Synopsis:
#    cfg print ?<pattern>?
#
# @param pattern         An optional glob pattern that can be used to only
#                        only include specific configuration items in the
#                        returned list. This can also be a list of glob
#                        patterns if necessary.
# @return                A string on the form "<CONFIG> = <VALUE>" where each
#                        configuration item is separated by a newline.
#
opt::option ::cfg_utils::cfg print --params { { pattern {*} } } --body {

	touch output
	
	if {$pattern == {}} {
		set pattern {*}
	}
	
	set cfg_names [cfg names [lindex $pattern 0]]
	foreach filter [lrange $pattern 1 end] {
		lappend cfg_names {*}[cfg names $filter]
	}
	
	foreach name [lsort -unique $cfg_names] {
		if {[lsearch [cfg get MASKED_CFG {HELP ARGS MOREARGS}] $name] > -1} {
			append output "$name = ***\n"
		} else {
			append output "$name = [cfg get $name]\n"
		}
	}

	return [string trimright $output \n]
}

#
# Substitute configuration placeholders with their relative configuration
# item values.
#
# Configuration placeholders are uppercase alphanumeric strings wrapped with
# the % sign. Underscores are allowed. For example say we have the following
# string: "Tea ready in %TEA_DELAY% minutes". The %TEA_DELAY% placeholder here
# will be replaced by "5" if the configuration item TEA_DELAY is set to 5.
#
# If the configuration item TEA_DELAY is not set, then the placeholder is left
# intact.
#
# If the input is in itself a configuration item then the value of that will
# be substituted.
#
# Note that substitution happens recursively, i.e. the configuration item that
# is substituting a placeholder may itself contain configuration placeholders.
# This also means that infinite loops are possible.
#
# Synopsis:
#    cfg subst <text>
#    cfg subst <config> ?<default>?
#
# @param text            The input string that contains configuration
#                        placeholders that need to be substituted
# @param config          The name of a configuration item to apply the
#                        substitution to
# @param default         Optional default value to return if the config item
#                        does not exist. If erroneously used in combination with
#                        substituting plain text (as opposed to a config item)
#                        then the default will simply override the text.
# @return                The value of the input string (or config item) with
#                        configuration placeholders substituted
#
opt::option ::cfg_utils::cfg subst --params { input { default {} } } --body {

	touch replace_list
	set start 0
	
	if {[cfg exists $input]} {
		set input [cfg get $input]
	} elseif {$default != {}} {
		set input $default
	}

	while {[regexp -indices -start $start {(%[A-Z0-9_][A-Z0-9_]+%)} $input indices]} {
		lassign $indices from to
		set subst_name  [string range $input $from $to]
		set cfg_name    [string trim $subst_name %]
		set subst_value [cfg get $cfg_name $subst_name]
		if {$subst_value != $subst_name} {
			if {[regexp {%[A-Z0-9_][A-Z0-9_]+%} $subst_value]} {
				set subst_value [cfg subst $subst_value]
			}
			lappend replace_list $subst_name $subst_value
		}
		set start $to
	}

	return [string map $replace_list $input]
}

#
# Indicates whether a particular functionality (config item) is enabled.
#
# Synopsis:
#    cfg enabled <config> ?<default>?
#
# @param config          Name of the configuration item to check
# @param default         The default value that should be used if the
#                        configuration item is not set.
# @return                1 if the configuration is enabled, interpreted by the
#                        first character of the value being one of "1", "Y", 
#                        "T", or "A" which should cover cases like "1", "Yes",
#                        "True", "Active"
#
opt::option ::cfg_utils::cfg enabled --params { name { default {} } } --body {
	variable ::cfg_utils::CFG

	if {[info exists CFG($name)]} {
		set cfg $CFG($name)
	} else {
		set cfg $default
	}

	return [expr {[lsearch -nocase {Y T 1 A} [string index $cfg 0]] > -1}]
}

#
# Indicates whether a particular functionality (config item) is disabled.
#
# Synopsis:
#    cfg disabled <config> ?<default>?
#
# @param config          Name of the configuration item to check
# @param default         The default value that should be used if the
#                        configuration item is not set. It should be clarified,
#                        however, that the default value here is in respect to
#                        the value of the configuration item and not whether it
#                        is disabled or not. E.g. passing in "1" as the default
#                        will mean that the configuration item is enabled by
#                        default meaning it is not disabled so 0 would be
#                        returned. Typically no default option is provided for
#                        this option as if a configuration item is not set then
#                        it will be interpreted as not being enabled.
# @return                1 if the configuration is disabled, 0 otherwise
#
opt::option ::cfg_utils::cfg disabled --params { name { default {} } } --body {
	return [expr {![cfg enabled $name $default]}]
}


#
# Retrieves the value of a given configuration item.
#
# Synopsis:
#    cfg get <config> ?<default>?
#
# @param config          Name of the configuration item to retrieve
# @param default         The default value to return if the config item is not
#                        set
# @return                The value of the configuration item if set, otherwise
#                        the passed in default value is returned
#
opt::option ::cfg_utils::cfg get --params { name { default {} } } --body {
	variable ::cfg_utils::CFG

	if {[info exists CFG($name)]} {
		return $CFG($name)
	}
	return $default
}

#
# Enable functionality by setting a given configuration item to 1.
#
# Synopsis:
#    cfg enable <config>
#
# @param config          Name of the configuration item to enable
#
opt::option ::cfg_utils::cfg enable --params { name } --body {
	variable ::cfg_utils::CFG
	set CFG($name) 1
}

#
# Disable functionality by setting a given configuration item to 0.
#
# Synopsis:
#    cfg disable <config>
#
# @param config          Name of the configuration item to disable
#
opt::option ::cfg_utils::cfg disable --params { name } --body {
	variable ::cfg_utils::CFG
	set CFG($name) 0
}

#
# Set the value of a given configuration item.
#
# Synopsis:
#    cfg set <config> <value>
#
# @param config          Name of the configuration item to set
# @param value           The value to set the configuration item to
#
opt::option ::cfg_utils::cfg set --params { name value } --body {
	variable ::cfg_utils::CFG
	set CFG($name) $value
}

#
# Appends a value to a given configuration item.
#
# Synopsis:
#    cfg append <config> <value>
#
# @param config          Name of the configuration item to modify
# @param value           The value to append to the configuration item
#
opt::option ::cfg_utils::cfg append --params { name value } --body {
	variable ::cfg_utils::CFG
	return [append CFG($name) $value]
}

#
# Like the list lappend proc this will list append a value to a given
# configuration item.
#
# Synopsis:
#    cfg lappend <config> <value>
#
# @param config          Name of the configuration item to add to
# @param value           The value to list append the configuration item to
#
opt::option ::cfg_utils::cfg lappend --params { name args } --body {
	variable ::cfg_utils::CFG
	return [lappend CFG($name) {*}$args]
}

#
# Returns all configuration values in key value form like one would expect from
# an "array get" call.
#
# Synopsis:
#    cfg all
#
# @return                All configuration items and values in list form
#
opt::option ::cfg_utils::cfg all --params {} --body {
	variable ::cfg_utils::CFG
	return [array get CFG]
}

#
# Returns all configuration values of a given pattern in key value form like
# one would expect from an "array get" call.
#
# Synopsis:
#    cfg unload ?<pattern>?
#
# @param pattern         A glob pattern representing the configuration items to
#                        include in the unload. Defaults to *, i.e. all
#                        configuration items.
# @return                All configuration items matching the glob pattern and
#                        their values in list form
#
opt::option ::cfg_utils::cfg unload --params { { pattern {*} } } --body {
	variable ::cfg_utils::CFG
	return [array get CFG $pattern]
}

#
# Loads a set of configuration items and their values. Any existing
# configuration items that is present in the data being loaded will be
# overwritten without warning.
#
# Synopsis:
#    cfg load ?<data>?
#
# @param data            A list of key value pairs of configuration items and
#                        their respective values to load
#
opt::option ::cfg_utils::cfg load --params { cfg_data } --body {
	variable ::cfg_utils::CFG
	array set CFG $cfg_data
}

#
# This will clear all configuration items currently set.
#
# This is primarily intended for test purposes where it may be necessary to
# start a test without any pre-existing configuration items set.
#
# Synopsis:
#    cfg reset
#
opt::option ::cfg_utils::cfg reset --params {} --body {
	variable ::cfg_utils::CFG
	variable ::cfg_utils::SOURCED_CFG_FILES
	
	set ::cfg_utils::SOURCED_CFG_FILES {}
	array unset CFG
}

#
# Unset a particular configuration item.
#
# Synopsis:
#    cfg unset ?<pattern>?
#
# @param pattern         The name of the configuration item or a glob pattern
#                        representing the configuration items to unset.
#                        Defaults to *, i.e. all configuration items.
#
opt::option ::cfg_utils::cfg unset --params { { pattern {*} } } --body {

	if {$pattern == {*}} {
		set ::cfg_utils::SOURCED_CFG_FILES {}
	}

	array unset ::cfg_utils::CFG $pattern
}

#
# Checks whether a particular configuration item exists (is set).
#
# Synopsis:
#    cfg exists <config>
#
# @param config          The name of the configuration item to check if it
#                        exists or not
# @return                1 if the configuration item is set, 0 otherwise
#
opt::option ::cfg_utils::cfg exists --params { name } --body {
	return [info exists ::cfg_utils::CFG($name)]
}

#
# Returns a list of configuration names matching a given pattern.
#
# Synopsis:
#    cfg names ?<pattern>?
#
# @param pattern         A glob pattern to match the configuration names to
#                        list. Defaults to *, i.e. all configuration items.
# @return                All configuration items that matches the given glob
#                        pattern
#
opt::option ::cfg_utils::cfg names --params { { pattern {*} } } --body {
	return [array names ::cfg_utils::CFG $pattern]
}

