# Copyright (c) 2015 Stein Gunnar Bakkeby all rights reserved.

package provide net_utils 1.0

#
# This is pretty much fetched directly from http://wiki.tcl.tk/14144.
#
# @see                   http://wiki.tcl.tk/14144
#
namespace eval net_utils {
	namespace export urlencode
	namespace export urldecode

	variable map
	variable alphanumeric a-zA-Z0-9
	for {set i 0} {$i <= 256} {incr i} { 
		set c [format %c $i]
		if {![string match \[$alphanumeric\] $c]} {
		set map($c) %[format %.2x $i]
		}
	}
	# These are handled specially
	array set map { " " + \n %0d%0a }
}

proc net_utils::urlencode {string} {
	variable map
	variable alphanumeric

	# The spec says: "non-alphanumeric characters are replaced by '%HH'"
	# 1 leave alphanumerics characters alone
	# 2 Convert every other character to an array lookup
	# 3 Escape constructs that are "special" to the tcl parser
	# 4 "subst" the result, doing all the array substitutions

	regsub -all \[^$alphanumeric\] $string {$map(&)} string
	# This quotes cases like $map([) or $map($) => $map(\[) ...
	regsub -all {[][{})\\]\)} $string {\\&} string
	return [subst -nocommand $string]
}

proc net_utils::urldecode {string} {
	# rewrite "+" back to space
	# protect \ from quoting another '\'
	set string [string map [list + { } "\\" "\\\\"] $string]

	# prepare to process all %-escapes
	regsub -all -- {%([A-Fa-f0-9][A-Fa-f0-9])} $string {\\u00\1} string

	# process \u unicode mapped chars
	return [subst -novar -nocommand $string]
}