# Copyright (c) 2013 Stein Gunnar Bakkeby all rights reserved.

package provide tapp_lib 1.0

#
# File utilities package that deals with common file operations.
#
# If dry-run is enabled then no harmful file operations will take place.
#
# @cfg FILE_DATE_PREFIX_FORMAT              The file date prefix format,
#                                           defaults to "%Y.%m.%d."
# @cfg FILE_TIME_BY_DATE_PREFIX      <0/1>  Whether to enable determining of
#                                           time/date based on a prefix in the
#                                           file name, defaults to disabled
# @cfg FILE_TIME_BY_EXIF             <0/1>  Whether to enable determining of
#                                           time/date based on EXIF information
#                                           or not, defaults to disabled
# @cfg NEF_EXIF_CMD                         Determines what command to use in
#                                           order to extract EXIF information
#                                           from .NEF files. Defaults to
#                                           "exiftool".
# @see                                      http://www.sno.phy.queensu.ca/~phil/exiftool/
# @cfg FILE_TIME_MIDNIGHT_ADJUSTMENT <0/1>  If the file was created after
#                                           midnight, but before 5am, then use
#                                           previous day instead. Only useful
#                                           if we are looking for the date
#                                           only. The primary use case here is
#                                           for sorting photos where one might
#                                           want pictures taken after midnight
#                                           to still be placed in the previous
#                                           day's folder, like for photos taken
#                                           on New Year's Eve for example.
#                                           Defaults to disabled.
# @cfg CHECKSUM_CMD                         The command to use to get the
#                                           checksum of a file. Defaults to:
#                                           md5sum "%file%". The %file%
#                                           placeholder is substituted with the
#                                           path to the file when the command
#                                           is being executed.
#
namespace eval file_utils {
	namespace export determine_dest
	namespace export determine_file_time
	namespace export move
	namespace export recurse_directory
	namespace export ensure_exists
	namespace export purge_empty_directories
	namespace export get_absolute_path
	namespace export save_array
	namespace export load_array

	variable HUMAN_READABLE_SIZE_MAP
	array set HUMAN_READABLE_SIZE_MAP {
		B 1
		K 1024
		M 1048576
		G 1073741824
		T 1099511627776
		P 1125899906842624
		E 1152921504606846976
		Z 1180591620717411303424
		Y 1208925819614629174706176
	}

	variable AUTO_EXECOK_DU [expr {[auto_execok du] != {}}]
}


#
# This proc determines the time/date of a given file according to a set of
# rules depending on configuration.
#
# This is primarily intended for applications that deals with images and
# sorting.
#
# If the given file does not exist then an error is thrown.
#
# The priority order is as follows:
#
#    - if the file contains a date prefix then that is used
#    - otherwise then EXIF data is used if available
#    - finally the file modification time is used if the above fails
#
# See the FILE_TIME_* config items for details.
#
# @param file            The file to determine the time for
# @param format          The output format to return the time in, if not
#                        provided the time will be returned in seconds since
#                        epoch
# @return                The determined time in seconds or the given format
#
proc file_utils::determine_file_time { file { format {} } } {

	if {![file exists $file]} {
		error "Trying to determine time for an nonexistent file: $file"
	}

	set time [file mtime $file]
	set prefix_found 0

	if {[cfg enabled FILE_TIME_BY_DATE_PREFIX 0]} {
		set prefix_found [get_file_date_prefix $file prefix_date]
		if {$prefix_found} {
			set time [clock scan $prefix_date -format [cfg get FILE_DATE_PREFIX_FORMAT {%Y.%m.%d.}]]
		}
	}

	# File date prefix takes precedence over image time
	if {!$prefix_found} {
		if {[cfg enabled FILE_TIME_BY_EXIF 0]} {
			lassign [get_exif_date $file] exif_found exif_date
			if {$exif_found} {
				set time $exif_date
			}
		}

		if {[cfg enabled FILE_TIME_MIDNIGHT_ADJUSTMENT 0]} {
			# If the file was created after midnight but before 5am
			# then use previous day instead. Only useful if we are
			# looking for the date only.
			if {[clock format $time -format "%H"] < 5} {
				incr time -86400
			}
		}
	}

	if {$format != {}} {
		set time [clock format $time -format $format]
	}

	return $time
}

#
# Attempts to retrieve a date prefix from a file.
#
# The date prefix can be specified with the FILE_DATE_PREFIX_FORMAT
# configuration item.
#
# @param file            The file to retrieve the date prefix from
# @param match           The variable name to store the prefix in if found
# @return                1 if the file contained a prefix, 0 otherwise
#
proc file_utils::get_file_date_prefix { file match } {
	upvar 1 $match prefix_date
	set regex [text_utils::convert_clock_format_to_regexp [cfg get FILE_DATE_PREFIX_FORMAT {%Y.%m.%d.}]]
	return [regexp "^($regex)" [file tail $file] - prefix_date]
}

#
# Attempt to get the EXIF date from a given file.
#
# This proc depends on the jpeg package from tcllib for retrieving the EXIF
# information from JPEG files
#
# @param file            The file to retrieve the EXIF information from
# @return                A list on the form <found> <time> where the former
#                        indicates whether the EXIF information was found and
#                        the latter is the time found (will be empty string if
#                        no EXIF information was found).
# 
proc file_utils::get_exif_date { file } {

	if {[catch { package require jpeg 0.3.5 } msg]} {
		log WARN {Package jpeg 0.3.5 is not present}
	}

	if {![file exists $file]} {
		error "Trying to read exif for an nonexistent file: $file"
	}

	set exif_found 0
	touch time
	set extension [string toupper [file extension $file]]

	# The EXIF information (if present)
	if {![catch { package present jpeg }] && [::jpeg::isJPEG $file]} {
		array set EXIF [::jpeg::getExif $file]
		foreach exifkey {DateTimeOriginal DateTimeDigitized DateTime} {
			if {[info exists EXIF($exifkey)]} {
				set time [clock scan $EXIF($exifkey) -format "%Y:%m:%d %H:%M:%S"]
				set exif_found 1
				break
			}
		}
	} elseif {$extension == {.NEF} && $::tcl_platform(platform) == {unix}} {
		array set EXIF [_get_nef_exif $file]
		foreach exifkey {{Date/Time Original} {Create Date}} {
			if {[info exists EXIF($exifkey)]} {
				set time [clock scan [string range $EXIF($exifkey) 0 18] -format "%Y:%m:%d %H:%M:%S"]
				set exif_found 1
				break
			}
		}
	}

	return [list $exif_found $time]
}

#
# Retrieves the EXIF information from a .NEF file by using the exiftool
# (configurable) application.
#
# @param file            The file to extract the EXIF information from
# @return                All EXIF data found as an array in list form
#
proc file_utils::_get_nef_exif { file } {
	
	set exif_cmd [cfg get NEF_EXIF_CMD exiftool]
	if {[catch {set output [exec $exif_cmd $file]} msg]} {
		log ERROR {Error occurred while executing $exif_cmd: $msg}
		return
	}
	set lines [split $output \n]
	array set EXIF {}
	foreach line $lines {
		set data  [split $line ":"]
		set key   [string trim [lindex $data 0]]
		set value [string trim [join [lrange $data 1 end] :]]
		set EXIF($key) $value
	}

	return [array get EXIF]
}

#
# This proc will go through a list of directories and remove any that are empty.
#
# @param directories     List of directories to purge
#
proc file_utils::purge_empty_directories { directories } {

	foreach dir $directories {
		if {[file isdirectory $dir]} {
			# If directory use glob to check whether it contains any files.
			if { [string length [glob -nocomplain -directory $dir *]] == 0 } {
				if {[cfg enabled DRY_RUN]} {
					log INFO {Pretending to remove directory $dir}
				} else {
					if {[catch {file delete $dir} msg]} {
						log ERROR {An error occurred attempting to remove directory $dir: $msg}
					} else {
						log INFO {Removed empty directory $dir}
					}
				}
			}
		}
	}
}

#
# Returns the file type of the given file.
#
# @param file            The file to retrieve the file type for
# @return                The file type of the given file, or "text" if the file
#                        does not exist
#
proc file_utils::file_type { file } {
	set file_type {text}
	# Due to poor implementation of the Tcl "file type" which will throw an error
	# if a file does not exist, combined with "file exists" which returns false
	# for a symlink that refers to a non-existing file, we have to do a catch all
	# as we need to get the file type of "link" although the file referred to may
	# not exist.
	catch {
		set file_type [file type $file]
	}
	return $file_type
}

#
# This proc handles common move operations.
#
# The proc handles the following:
#
#    - not attempting to move the file to the same destination / folder
#    - not moving the file if another file with same name already exists at
#      destination
#    - not making any changes if performing a dry-run
#    - unified logging
#
# @param file            The file to move
# @param dest            The destination file or directory to move the file to
# @return                1 if the file was moved, 0 otherwise
#
proc file_utils::move { file dest } {

	# Verify that we are not attempting to move the file to the same location
	set file_name [file tail $file]

	file stat $file FILE
	set DEST(ino) -1
	set DEST(dev) -1
	if {[cfg disabled DRY_RUN] && [file exists $dest]} {
		if {[file isdirectory $dest]} {
			if {[file exists [file join $dest $file_name]]} {
				file stat [file join $dest $file_name] DEST
			}
		} else {
			file stat $dest DEST
		}
	}

	# If this is the same device and we have the same inode ID then we are referring to the same file.
	# Note: Two different files can have the same inode ID on separate drives / media.
	if {$FILE(dev) == $DEST(dev) && $FILE(ino) == $DEST(ino)} {
		log WARN {Not moving the file $file to same destination}
	} elseif {$DEST(ino) != -1} {
		log WARN {Another file already exists at destination not moving file}
	} else {
		if {[cfg enabled DRY_RUN]} {
			log INFO {Pretending to move the file $file to $dest (dry-run)}
		} else {
			log INFO {Moving the file $file to $dest}
			if {[catch {file rename $file $dest} msg]} {
				log ERROR {An error occurred while attempting to move the file $file to $dest\nError was: $msg}
				return 0
			}
		}
		return 1
	}
	return 0
}

#
# Recurses a directory and returns two lists:
#
#    - a list of all subfolders
#    - a list of all files across all subfolders
#
# @param directory       The directory to recurse
# @param filters         Optional filters to only return particular files.
#                        Can be a list of glob patterns like {*.cfg *.tcl},
#                        defaults to selecting all files.
# @return                A list containing two lists, one containing all
#                        subfolders and one containing all files
#
proc file_utils::recurse_directory { directory {filters *} } {

	if {$directory == {}} {
		return [list {} {}]
	}

	set files [glob -nocomplain -types {f r} -path "$directory/" {*}$filters]
	set dirs  [glob -nocomplain -types {d r} -path "$directory/" *]
	set subdirs {}

	foreach dir $dirs {
		lassign [file_utils::recurse_directory $dir] subd subf
		set files   [concat $files   $subf]
		set subdirs [concat $subdirs $subd]
	}
	set dirs [concat $dirs $subdirs]
	return [list $dirs $files]
}

#
# Analyse a file using file lstat and add additional info to the array data.
#
# For the main elements in the returned array see the documentation for
# "file stat", typical values include the file ino ID, mtime, size, etc.
#
# Note that if the file does not exist then the file stat info will not be
# available.
# 
# For the added elements see the list below.
#
# Example usage:
#    array set DATA [file_utils::analyse Backup.tgz]
#    if {$DATA(digest) == {BROKEN_LINK}} {
#            ...
#    }
#
# @element file          The path of the file as passed in to the analyse proc
# @element path          The absolute path to the file
# @element name          The file name
# @element ext           The file extension
# @element exists        Whether the file exists or not
# @element type          The file type, will be "null" if the file does not
#                        exist
# @element digest        A short summary of what sort of file we are actually
#                        dealing with, will contain one of the following
#                        values: DIRECTORY, FILE, NON_EXISTING_FILE,
#                        BROKEN_LINK, SYMLINK, UNDETERMINED
# @param file            The file to analyse
# @see                   "file lstat" under
#                        http://www.tcl.tk/man/tcl8.5/TclCmd/file.htm#M21
# @return                The file info array in list form
#
proc file_utils::analyse { file } {

	set FILE(file)   $file
	set FILE(path)   [get_absolute_path $FILE(file)]
	set FILE(name)   [file tail $FILE(path)]
	set FILE(exists) [file exists $FILE(path)]
	set FILE(ext)    [string tolower [file extension $FILE(file)]]
	set FILE(type)   null

	if {$FILE(exists)} {
		file lstat $FILE(file) FILE
	}

	switch -exact -- "$FILE(exists)|$FILE(type)" {
	1|directory { set FILE(digest) DIRECTORY }
	1|file      { set FILE(digest) FILE }
	0|null      { set FILE(digest) NON_EXISTING_FILE }
	0|link      { set FILE(digest) BROKEN_LINK }
	1|link      { set FILE(digest) SYMLINK }
	default     { set FILE(digest) UNDETERMINED }
	}

	return [array get FILE]
}

#
# This will compare two files based on the output of file_utils::analyse and may be
# used to find out whether the files are the same or whether they are different in
# one way or another.
#
# Typical usage:
#
#   - find out if the files are the same (via symlink hardlink or copy)
#   - find out if one file is newer than the other
#   - find out if the two files are completely different but have the same name
#   - find duplicates
#       
# Possible return values:
#
#    foreach return_code [file_utils::compare <file1> <file2>] {
#    	switch -- $return_code {
#    	NEITHER_EXISTS -
#    	ONLY_LEFT_EXISTS -
#    	ONLY_RIGHT_EXISTS {
#    		# One or both of the files doesn't exist
#    	}
#    	BOTH_EXISTS {
#    		# Generally expected return code
#    	}
#    	LEFT_<FILE_TYPE>_RIGHT_<FILE_TYPE> -
#    	DIFFERENT_TYPE {
#    		# The files compared are of completely different file types
#    		# Generated codes on the form LEFT_<FILE_TYPE>_RIGHT_<FILE_TYPE>
#    		# may also be returned e.g. LEFT_DIRECTORY_RIGHT_FILE
#    	}
#    	SAME_TYPE {
#    		# Generally expected return code
#    	}
#    	SAME_FILE {
#    		# Both files are referring to the same path
#    	}
#    	BOTH_SYMLINKS_TO_SAME_FILE -
#    	RIGHT_SYMLINK_TO_LEFT -
#    	LEFT_SYMLINK_TO_RIGHT {
#    		# One or both of the files are symlinks
#    	}
#    	HARDLINK {
#    		# Both files are hardlinked to the same file
#    	}
#    	DUPLICATE {
#    		# The two files are duplicates
#    	}
#    	LEFT_NEWER_THAN_RIGHT -
#    	RIGHT_NEWER_THAN_LEFT -
#    	SAME_MTIME {
#    		# One of the files are newer than the other or have the same
#    		# modification time
#    	}
#    	LEFT_LARGER_THAN_RIGHT -
#    	RIGHT_LARGER_THAN_LEFT -
#    	SAME_SIZE {
#    		# One of the files are larger than the other or have the same
#    		# size
#    	}
#    	DIFFERENT_FILES {
#    		# Generally expected return code
#    	}
#    	MAD_SCIENTIST {
#    		# This should never happen
#    	}
#    	default {
#    	}
#    	}
#    }
#
# @param file1           The first file
# @param file2           The file to compare to the first file
# @return                A list of relevant return codes
#
proc file_utils::compare { file1 file2 } {

	set return_codes {}

	array set LEFT  [analyse $file1]
	array set RIGHT [analyse $file2]

	switch -- "$LEFT(exists)$RIGHT(exists)" {
	00 { lappend return_codes NEITHER_EXISTS }
	10 { lappend return_codes ONLY_LEFT_EXISTS }
	01 { lappend return_codes ONLY_RIGHT_EXISTS }
	11 { lappend return_codes BOTH_EXISTS }
	default {
		lappend return_codes MAD_SCIENTIST
	}
	}

	if {$LEFT(type) == $RIGHT(type)} {
		lappend return_codes SAME_TYPE
	} else {
		lappend return_codes DIFFERENT_TYPE
		lappend return_codes LEFT_[string toupper $LEFT(type)]_RIGHT_[string toupper $RIGHT(type)]
	}

	# If both file paths refers to the same file it is the same file.
	if {$LEFT(path) == $RIGHT(path)} {
		lappend return_codes SAME_FILE
	}

	# The rest of the return codes only apply to existing files
	# so return if this is not the case
	if {!$LEFT(exists) || !$RIGHT(exists)} {
		return $return_codes
	}

	# If both file arguments refer to the same file but path differs.
	if {
		   $LEFT(path) != $RIGHT(path)
		&& $LEFT(dev)  == $RIGHT(dev)
		&& $LEFT(ino)  == $RIGHT(ino)} {
		switch -exact "$LEFT(type)|$RIGHT(type)" {
		"link|link"      { lappend return_codes BOTH_SYMLINKS_TO_SAME_FILE }
		"directory|link" -
		"file|link"      { lappend return_codes RIGHT_SYMLINK_TO_LEFT }
		"link|directory" -
		"link|file"      { lappend return_codes LEFT_SYMLINK_TO_RIGHT }
		"directory|directory" -
		"file|file"      { lappend return_codes HARDLINK }
		default {
			log WARN {The improbable but faulty theory relies on slack data. File types are $file1_type and $file2_type.}
			lappend return_codes MAD_SCIENTIST
		}
		}
	}

	if {$LEFT(mtime) > $RIGHT(mtime)} {
		lappend return_codes LEFT_NEWER_THAN_RIGHT
	} elseif {$LEFT(mtime) < $RIGHT(mtime)} {
		lappend return_codes RIGHT_NEWER_THAN_LEFT
	} else {
		lappend return_codes SAME_MTIME
	}

	if {$LEFT(size) > $RIGHT(size)} {
		lappend return_codes LEFT_LARGER_THAN_RIGHT
	} elseif {$LEFT(size) < $RIGHT(size)} {
		lappend return_codes RIGHT_LARGER_THAN_LEFT
	} else {
		lappend return_codes SAME_SIZE
	}

	return $return_codes
}

#
# Ensure that a destination directory exists.
#
# If the directory does not already exist then it will be created. If a dry-run
# is performed then no directories will be created.
#
# @param dest            The directory to ensure exists
#
proc file_utils::ensure_exists { dest } {

	if {$dest == {} || [file exists $dest]} {
		return
	}

	if {[cfg enabled DRY_RUN]} {
		log INFO {Pretending to create directory $dest (dry-run)}
	} else {
		log INFO {Creating directory $dest}
		file mkdir $dest
	}
}

#
# Convenience proc as a reminder that it is possible to use file normalize for
# this purpose.
#
# @param file            The file to get the absolute path for
# @return                The absolute path (output from file normalize)
#
proc file_utils::get_absolute_path { file } {
	return [file normalize $file]
}

#
# Get the real path to the file, i.e. with any links resolved.
#
# This simulates by recursion the unix command line tool readlink -f <file>.
#
# @param file            The file to get the real path for
# @return                The real path of the file (like absolute path, but
#                        with symlinks resolved)
#
proc file_utils::get_real_path { file } {

	array set FILE [analyse $file]

	if {$FILE(digest) == {SYMLINK}} {
		set linked_file [file readlink $file]
		if {[file pathtype $linked_file] == {relative}} {
			set linked_file [file join [file dirname $file] $linked_file]
		}
		return [get_real_path $linked_file]
	}

	return $FILE(path)
}

#
# Checks whether a file or directory is empty.
#
# @param file            The file to check if empty
# @return                1 if the file does not exist, if it is a normal file
#                        and it's size is zero, if the file is a directory that
#                        contains no files. In all other circumstances 0 will
#                        be returned.
#
proc file_utils::is_empty { file } {

	if {[catch { set file_type [file type $file] } msg]} {
		log WARN {Failure to retrieve file type assumes file does not exist thus empty}
		return 1
	}

	if {[info command _is_empty_${file_type}] != {}} {
		set is_empty [_is_empty_${file_type} $file]
		set is [string map {0 {is not} 1 is} $is_empty]
		log DEBUG {The ${file_type} $file $is empty}
		return $is_empty
	}
	return 0
}

proc file_utils::_is_empty_file { file } {
	return [expr {[file size $file] == 0}]
}

proc file_utils::_is_empty_directory { file } {
	return [expr {[string length [glob -nocomplain -directory $file *]] == 0}]
}

#
# Convenience proc to read and return the content of a given file.
#
# Error will be thrown if the file does not exist or the user has no permission
# to read the file.
#
# @param file            The file to read
# @return                The file content as plain text
#
proc file_utils::read_file { file } {

	if {![file exists $file]} {
		log ERROR {Cannot read non-existing file ($file)}
		return
	}

	if {![file readable $file]} {
		log ERROR {No permission to read file ($file)}
		return
	}
	
	set fp [open $file r]
	set file_data [read $fp]
	close $fp
	return $file_data
}

#
# Convenience proc to append content to a given file.
#
# @param file            The file to append to
# @param content         The content/text to append
#
proc file_utils::append_file { file content } {
	return [write_file $file $content "a"]
}

#
# Tests a file matching pattern such as file*.txt and returns a list of files
# that matches.
#
# If no files matches then an empty string is returned.
#
# @param pattern         The file pattern to check
# @return                A list of files that matched the pattern, or an empty
#                        list if no files matched.
# 
proc file_utils::expand_file { pattern } {
	return [glob -nocomplain $pattern]
}

#
# Convenience proc to write data to a given file.
#
# Note that if the file already exists then the content will be overwritten.
#
# @param file            The file to append to
# @param content         The content/text to append
# @param mode            The mode to write to the file with, defaults to "w"
#                        which is to (over)write the file. An alternative is
#                        to pass in "a" in order to append to the file.
# @see                   Also see file_utils::append_file
#
proc file_utils::write_file { file content {mode "w"} } {

 	assert {$mode in {w a}}

	if {![file_writable $file]} {
		log ERROR {No permission to write to file ($file)}
		return
	}
	
	if {[cfg enabled DRY_RUN]} {
		log INFO {Pretending to write to file ($file) (dry-run)}
	} else {
		set fp [open $file $mode]
		puts $fp $content
		flush $fp
		close $fp
	}
}

#
# Delete a given file.
#
# Warning: Files will be permanently deleted, moving files to a designated
# "trash" directory is recommended unless you are absolutely sure of what you
# are doing.
#
# @param file            The file to permanently delete
# @return                1 if the file was deleted, 0 otherwise
#
proc file_utils::delete_file { file } {

	if {![file exists $file]} {
		log WARN { - Not deleting non-existing file $file}
		return 1
	}

	if {[cfg enabled DRY_RUN]} {
		log INFO { - Pretended to delete $file (dry run)}
		return 1
	}

	if {[catch {file delete -force -- $file} msg]} {
		log ERROR { - Failed to delete $file due to $msg}
		return 0
	}

	log INFO { - Deleted $file}
	return 1
}

#
# Procedure to convert a size in human readable format into bytes.
#
# Example usage:
#    file_utils::decode_human_readable_size 2.1M
#
# this would return "2202010".
#
# @param size            The size in human readable form, e.g. 952K, 1.5M, 7G
# @return                The size in bytes
#
proc file_utils::decode_human_readable_size { size } {

	variable HUMAN_READABLE_SIZE_MAP

	set size [string map {{ } {}} $size]
	if {![regexp {([0-9.]+)([A-Za-z]*?)(B|b)?$} $size {} num denotion bitorbyte]} {
		return [regsub -all {([^0-9]+)} $size {} size]
	}

	set denominator 1
	if {$bitorbyte == {} && [string index $denotion 0] == {b}} {
		if {![string match -nocase *byte* $denotion]} {
			set bitorbyte b
		}
	}

	set d [string toupper [string index $denotion 0]]
	if {[info exists HUMAN_READABLE_SIZE_MAP($d)]} {
		set denominator $HUMAN_READABLE_SIZE_MAP($d)
	}

	if {$bitorbyte == {b}} {
		set denominator [expr {$denominator / 8.00}]
	}

	return [format %.f [expr {$num * $denominator}]]
}

#
# Convert bytes into a human readable size.
#
# For example:
#    file_utils::human_readable_size 2202010
#
# would return "2.1M"
#
# @param bytes           Size in bytes to convert into human readable form
# @return                The size in human readable form,  e.g. 841K, 1.2M, 4G
# 
proc file_utils::human_readable_size { bytes } {

	variable HUMAN_READABLE_SIZE_MAP
	
	if {![string is integer $bytes] || $bytes == {}} {
		error "\"$bytes\" is not a number"
	}

	set denotion B; # Byte
	set denominator 1
	foreach den {B K M G T P E Z Y} {
		if {$bytes < $HUMAN_READABLE_SIZE_MAP($den)} {
			break
		}
		set denotion $den
		set denominator $HUMAN_READABLE_SIZE_MAP($den).00
	}

	set num [expr {$bytes / $denominator}]
	if {$num < 10} {
		set num [format %.1f $num]
	} else {
		set num [format %.0f $num]
	}

	return $num$denotion
}

#
# Convenience proc to check whether a file is writable.
#
# If the file does not exist the proc will check whether the directory of the
# given file is writable.
#
# @param file            The file to check if it can be written to
# @return                1 if the file exists and is writable or if the file
#                        does not exist and the parent directory is writable.
#                        Otherwise 0 is returned.
#
proc file_utils::file_writable { file } {

	if {![file exists $file]} {
		set file [file dirname $file]
	}

	return [file writable $file]
}

#
# Procedure to calculate the total size for a given set of files or
# directories.
#
# Warning: This proc can be very resource intensive, use with care.
#
# If the unix utility "du" for reporting disk usage is available then that will
# be used to determine directory sizes, otherwise directories will be processed
# recursively.
#
# Synopsis:
#    file_utils::file_size ?file file ...?
#
# @param file            The file or directory to get the total file size for
# @return                The total size of the given files
#
proc file_utils::file_size args {

	variable AUTO_EXECOK_DU

	set size 0
	set index 0
	for {set i 0} {$i < [llength $args]} {incr i} {
		set file [lindex $args $i]

		# ignore directories . and ..
		if {[file tail $file] in {. ..}} {
			continue
		}

		if {[file isfile $file]} {
			incr size [file size $file]
		} elseif {![file exists $file]} {
			incr size [string bytelength $file]
		} elseif {[file isdirectory $file]} {
			if {$AUTO_EXECOK_DU} {
				incr size [lindex [exec du -bs $file] 0]
			} else {
				# Warning: this may be slow
				set normal_files [glob -nocomplain -directory $file -types {r} *]
				set hidden_files [glob -nocomplain -directory $file -types {r hidden} *]
				lappend args {*}$normal_files {*}$hidden_files
			}
		}
	}
	return $size
}

#
# Load an array from file.
#
# @param arr             The name of the variable to load the array data into
# @param data_file       The file to load the array data from
# @return                The array data in list form
#
proc file_utils::load_array { arr data_file } {
	
	set array_data {}
	if {[file exists $data_file]} {
		set array_data [read_file $data_file]
	}

	if {$arr != {}} {
		upvar 1 $arr ARRAY
		array unset ARRAY
		array set ARRAY $array_data
	}

	return $array_data
}

#
# Save an array to file.
#
# @param arr             The name of the variable to retrieve the array data
#                        from
# @param data_file       The file to save the array data to
#
proc file_utils::save_array { arr data_file } {
	if {[llength $arr] == 1} {
		upvar 1 $arr ARRAY
		set content [array get ARRAY]
	} else {
		set content $arr
	}

	write_file $data_file $content
}

#
# Procedure to get the file checksum / hash.
#
# The checksum command is configurable, see the CHECKSUM_CMD configuration
# item. Throws an error if the checksum could not be made.
#
# @param file            The file to retrieve the checksum for
# @return                The calculated checksum, or empty string if anything
#                        has gone astray.
#
proc file_utils::get_checksum { file } {

	set replace_list [list %file% $file]
	set cmd [string map $replace_list [cfg get CHECKSUM_CMD {md5sum "%file%"}]]
	set ignore_dry_run 1
	
	set output [exec_utils::execute $cmd -ignore_dry_run]
	if {[regexp {([0-9a-fA-F]{31}[0-9a-fA-F]+?)} $output checksum]} {
		return $checksum
	}
	
	log ERROR {Checksum not found, output was: $output}
	error "file_utils::get_checksum: Checksum not found, output was: $output"
}

#
# Determines the destination directory based on a given prefix.
#
# @param prefix          A given prefix, f.ex. "2013-11-23"
# @param destination     The root directory, f.ex. /path/to/
# @return                The destination directory, i.e. /path/to/2013-11-23
#                        if the directory does not already exist, otherwise
#                        the existing directory is returned, e.g.
#                        /path/to/2013-11-23.Photos
#
proc file_utils::determine_dest { prefix destination } {
	set dest_prefix [file join $destination $prefix]
	if {[catch {set dest [lindex [glob -path $dest_prefix *] 0]} msg]} {
		set dest $dest_prefix
	}
	return $dest
}
