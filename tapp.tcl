# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require opt 1.0
package require tapp_lib 1.0

package provide tapp 1.0

#
# This is a wrapper package that is responsible for importing the various
# tapp_lib packages into the global namespace.
#
# The package also tries to determine the console width by executing the core
# linux utility "tty" if it is present. If successfully determined then the
# configuration item CONSOLE_WIDTH is updated to reflect this.
#
# This package also provides a rather powerful trace logging functionality.
# If the parameter --trace is passed in to the application then all procs
# created after package tapp has been loaded will be replaced by a wrapper
# function that logs out the parameters to the proc being called and what the
# returned output is. Be warned that this will produce massive amounts of log
# output and be very costly performance wise.
#
# @see                   cfg_utils
# @see                   tapp_log
# @see                   file_utils
# @see                   text_utils
# @see                   tapp_lang
# @see                   arg_utils
# @see                   opt
#
namespace eval tapp {}

proc _tapp_init { argv } {

	if {[info exists ::tapp::INIT] && $::tapp::INIT} {
		return
	}

	# This will import the tapp utils such as cfg and log into the global namespace.
	# The catch is to prevent errors when generating the pkgIndex.tcl
	if {[catch {
		namespace import opt::*
		namespace import arg_utils::*
		namespace import cfg_utils::*
		namespace import file_utils::*
		namespace forget tapp_lang::*
		namespace import tapp_lang::*
		namespace import tapp_log::*
		namespace import text_utils::*
		namespace import table_utils::*
		
		arg_utils::init
		
		tapp_log::log DEV {Loaded tapp}

		if {[auto_execok tty] != {} && [auto_execok stty] != {}} {
			if {![catch {exec tty -s}]} {
				cfg set CONSOLE_WIDTH [lindex [exec stty size] 1]
			}
		}
	} msg opt]} {
		set ret_code [lindex $opt [lsearch $opt -code]+1]
		if {$ret_code} {
			puts "ERROR: Failed to import tapp utils due to: $msg"
		}
		return
	}

	# Enable dynamic trace logging.
	# This method duplicates the amount of procs as we create a wrapper for each proc.
	# The benefit of this is that we can print out the output of the proc.
	# This is specifically loaded after package tapp_lib has been loaded as we want
	# to avoid putting trace logging on those.
	if {[info exists ::argv] && [lsearch $argv {--trace}] > -1} {
		rename proc _proc

		global _trace_log
		set _trace_log 0

		_proc proc { name params body } {
			if {[string match ::tcl::clock::* $name]
			|| [string match msgcat::* $name]
			|| [string match ::md5::* $name]
			|| [lsearch {ParseClockFormatFormat EnterLocale InitTZData parray} $name] > -1} {
				_proc ${name} $params $body
			} else {
				set _indent {   }
				set --> {|-->}
				set <-- {|<--}
				set log "if {!\$_trace_log} {set _trace_log 1;set _trace_indent \[string repeat {$_indent} \[info level\]\];tapp_log::log TRACE {\${_trace_indent}"
				set end "};set _trace_log 0;};"

				set new_body [subst -nocommands {
					global _trace_log
					set val {}
					if {\$args != {}} {
						set val \" (\$args)\"
					}
					${log}${-->} $name\$val${end}
					set ret [uplevel 1 eval \"${name}\u00A0 \{\$args\}\"]
					set val {}
					if {\$ret != {}} {
						set val \" (\$ret)\"
					}
					${log}${<--} $name\$val${end}
					return \$ret
				}]
				_proc ${name} args $new_body
				_proc ${name}\u00A0 $params $body
			}
		}
	}
	
	set ::tapp::INIT 1
}

if {[info exist argv]} {
	_tapp_init $argv
}