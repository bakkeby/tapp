# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp_lib 1.0

package provide notifications 1.0

#
# This package is intended as a wrapper for common message logic, such as for
# example desktop notifications.
#
# If dbus-tcl is installed then that will be the preferred method for sending
# notifications, but will need to be enabled via configuration. If it is not
# installed then the application "notify-send" will be used instead.
#
# For Windows Growl integration has been done, depends on growlnotify.exe
# (command line script).
# 
# Mac notifications has not yet been implemented, but contributions are most
# welcome.
#
# @cfg DBUS_NOTIFICATION    Enable dbus notification, defaults to disabled
# @cfg ENV_DISPLAY_VAL      On unix systems this config sets the DISPLAY
#                           environment variable if not already set (it does
#                           not override it). The main purpose of this is to
#                           enable desktop notifications for non-session
#                           processes such as cron jobs for example. Defaults
#                           to ":0" (display 0, i.e. main display).
# @cfg NOTIFY_LENGTH_LIMIT  Very long messages can cause various issues with
#                           the front end, such as display freezing / hanging
#                           and/or performance issues. Because of this a
#                           default limit of 2000 characters has been set on
#                           the message payload, but this can be overridden by
#                           changing this configuration item.
# @see                      http://dbus-tcl.sourceforge.net/
# @see                      http://ss64.com/bash/notify-send.html
# @see                      http://www.growlforwindows.com/gfw/help/growlnotify.aspx
#
namespace eval notifications {
	namespace export notify
}

#
# Send desktop notification message.
#
# @param icon            The icon representing the alert or the application
#                        sending the notification
# @param subject         Short description for the title of the notification
# @param body            The notification message content, the following formats
#                        should be supported:
#                        <b>...</b>                (bold),
#                        <i>...</i>                (italic),
#                        <u>...</u>                (underline),
#                        <a href="...">...</a>     (hyperlink),
#                        <img src="..." alt="..."/>  (image)
#                        
# @param application     The application sending the message
#
proc notifications::notify { icon subject body application } {

	_env_setup
	
	set max_length [cfg get NOTIFY_LENGTH_LIMIT 2000]
	if {[string length $body] > $max_length} {
		set body [string range $body 0 $max_length]...
	}

	if {[cfg enabled DBUS_NOTIFICATION 0] && ![catch { package require dbus-tcl } msg]} {
		_notify_dbus $icon $subject $body
	} elseif {[auto_execok {notify-send}] != {}} {
		_notify_send $icon $subject $body
	} elseif {[auto_execok {growlnotify.exe}] != {}} {
		_notify_growl $icon $subject $body $application
	} else {
		log DEBUG {Notifications not enabled}
	}
}

proc notifications::_env_setup {} {

	switch -- [string toupper $::tcl_platform(platform)] {
	UNIX {
		# Set the DISPLAY environment variable, needed by notifications from
		# non-session processes such as cron jobs.
		if {![info exists ::env(DISPLAY)] || $::env(DISPLAY) == {}} {
			set ::env(DISPLAY) [cfg get ENV_DISPLAY_VAL {:0}]
		}
	}
	default {}
	}

}

#
# Send desktop notification message using dbus.
#
# @param icon            The icon representing the alert or the application
#                        sending the notification
# @param subject         Short description for the title of the notification
# @param body            The notification message content
# @see                   http://www.galago-project.org/specs/notification/0.9/x408.html#command-notify
# @see                   http://dbus-tcl.sourceforge.net/dbus-tcl.html
# @see                   http://dbus.freedesktop.org/doc/dbus-tutorial.html
#
proc notifications::_notify_dbus { icon subject body } {

	set app_name {}
	set replaces_id 0
	set actions {}
	set hints {}
	set expire_timeout -1

	dbus connect session
	set id [dbus call session \
		-signature susssasa{sv}i \
		-dest org.freedesktop.Notifications \
		/org/freedesktop/Notifications \
		org.freedesktop.Notifications Notify \
		$app_name \
		$replaces_id \
		$icon \
		$subject \
		$body \
		$actions \
		$hints \
		$expire_timeout \
	]
	dbus close session

	return $id
}

#
# Send desktop notification message using the notify-send application.
#
# @param icon            The icon representing the alert or the application
#                        sending the notification
# @param subject         Short description for the title of the notification
# @param body            The notification message content
#
proc notifications::_notify_send { icon subject body } {

	if {[catch {exec notify-send --icon=$icon $subject $body} msg]} {
		log WARN {Unable to send notification; the program 'notify-send' is most likely not installed.\nThis can be installed by executing:\n    sudo apt-get install libnotify-bin}
		log WARN {The error that occurred: $msg}
		return 0
	}
	return 1
}

#
# Send desktop notification message using the growlnotify application.
#
# @param icon            The icon representing the alert or the application
#                        sending the notification
# @param subject         Short description for the title of the notification
# @param body            The notification message content
# @param application     The name of the application sending the message
#
proc notifications::_notify_growl { icon subject body application } {

	if {[catch {exec growlnotify.exe /t:"$subject" /r:"$application" /ai:"$icon" /n:"$application" /a:"$application" "$body"} msg]} {
		log WARN {Unable to send notification; the program 'growlnotify' is most likely not installed.\nVisit http://www.growlforwindows.com/gfw/help/growlnotify.aspx}
		log WARN {The error that occurred: $msg}
		return 0
	}
	return 1
}
