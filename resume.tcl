# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide resume 1.0

#
# This package is intended for applications processing a lot of files and adds
# the possibility of keeping track of what files are being processed and which
# are still outstanding.
# 
# This allows for the application to continue processing the remainign files if
# the process was abrupted half-way for whatever reason.
#
# This package relies on the array store utility for keeping track of remaining
# files to process.
#
# Warning: Depending on the application's integration with the resume package
# the ‑‑resume parameter has to be passed in to the application in order to
# continue processing where the application left off if it was abrupted.
# Note that this typically this has to be done already on the next run,
# otherwise the risk is that the resume list will be cleared.
# 
# Warning: The list of remaining files to process will be written to disk for
# each file being processed which may generate a high I/O.
#
# @cfg RESUME_TEMP_DIR         The temporary directory to store the list of
#                              remaining files to process
# @cfg RESUME_FILE_PROCESSING  Indicates whether to resume processing files
#                              after the process has been abrupted, this is
#                              generally enabled by passing ‑‑resume or
#                              ‑‑continue parameter to the application
# 
namespace eval resume {

	set pname [opt::proc resume --preset array]
}

#
# Initialises the resume package.
#
proc resume::init {} {
	
	variable PREVIOUS

	clear PREVIOUS

	# Get the location where we can store the temporary file(s)
	# to keep track of files left to process.
	set temp_dir [cfg get RESUME_TEMP_DIR $::env(HOME)]
	set temp_file .[this_script]_resume~
	cfg set ARRAY_STORE_LOCATION [file join $temp_dir $temp_file]
	resume load

	if {[cfg disabled RESUME_FILE_PROCESSING] || [cfg enabled RESET_ARRAY_STORE]} {
		resume reset
		resume save
	}

	if {![resume exists CURRENT]} {
		resume set CURRENT 0
	}

	if {![resume exists FILE_INDEX]} {
		resume set FILE_INDEX 0
	}
}

cfg lappend MOREARGS \
	{--resume}              {resume file operation if process was abrupted (provided the script has not been run since)}


#
# Parse resume package specific arguments.
#
# The package specifically looks for the ‑‑resume and ‑‑continue parameters.
#
# @param arguments       The application parameters to parse
# @return                The remaining arguments after parsing is complete
#                        (i.e. any arguments determined to not be relevant to
#                        the resume package)
#
proc resume::parse_args { arguments } {

	touch remaining_args

	foreach arg $arguments {

		switch -exact -- $arg {
		{--resume} -
		{--continue} {
			cfg set RESUME_FILE_PROCESSING 1
			log DEBUG {Enabled resuming of file processing}
		}
		default {
			lappend remaining_args $arg
		}
		}
	}

	return $remaining_args
}

#
# Get the next file to process.
#
# @return                The next file to process, or empty string if there are
#                        no more files to process
#
proc resume::next {} {

	variable PREVIOUS
	
	resume remove $PREVIOUS
	resume save

	set index [resume get CURRENT]
	set current_file [resume get $index]

	set PREVIOUS $index
	resume set CURRENT [incr index]
	
	return $current_file
}

#
# Check whether there are more files to process.
#
# @return                1 if there are more files to process, 0 otherwise
#
proc resume::has_more {} {
	return [resume exists [resume get CURRENT]]
}

#
# Add files to the list of files to process.
#
# @param files           The files to add to the resume list
#
proc resume::add_files { files } {

	if {[llength $files] == 0} {
		return
	}

	set file_index [resume get FILE_INDEX]

	foreach file $files {
		resume set $file_index $file
		incr file_index
	}

	resume set FILE_INDEX $file_index
}

#
# Add custom data as key value pairs to the resume package for later retrieval.
#
# This typically represents internal settings or array data that is required if
# the application is to resume processing from a given point.
#
# @param key            The name of the value being set
# @param value          The data to set the associated key to
#
proc resume::set_data { key value } {
	return [resume set $key $value]
}

#
# Retrieve custom data from the resume package.
#
# @param key            The name of the value to look up
# @return               The value associated with the given key, or empty
#                       string if the key does not exist
#
proc resume::get_data { key } {
	return [resume get $key]
}

#
# Commit or save any changes made to the resume data.
#
proc resume::commit {} {
	resume save
}

#
# Mark the job as finished. This will delete the resume list and associated
# data.
#
proc resume::finish {} {
	resume delete
}