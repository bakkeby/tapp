# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide tapp_lib 1.0

#
# This deals with more or less common string manipulation tasks.
#
# Most of the procedures in this namespace are intended for logging purposes.
#
namespace eval text_utils {
	namespace export string_capitalize
	namespace export convert_clock_format_to_regexp
	namespace export edify
	namespace export ingify
	namespace export index_replace

	variable format_group_regular_expressions {
		.     {\.}
		[     {\[}
		]     {\]}
		*     {\*}
		%a    {([A-Z][a-z]{2})}
		%A    {([A-Z][a-z]{5,7})}
		%b    {([A-Z][a-z]{2})}
		%B    {([A-Z][a-z]{2,7})}
		%c    {([A-Z][a-z]{2} [A-Z][a-z]{2}  ?[0-9]{1,2} [0-9]{2}:[0-9]{2}:[0-9]{2} [0-9]{4})}
		%C    {([0-9]{2})}
		%d    {(0[1-9]|[1-2][0-9]|3[01])}
		%D    {((?:0[1-9]|1[0-2])/(?:0[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4})}
		%e    {( [1-9]|[1-2][0-9]|3[01])}
		%Ec   {([A-Z][a-z]{2} [A-Z][a-z]{2}  ?(?: [1-9]|[1-2][0-9]|3[01]) [0-9]{2}:[0-9]{2}:[0-9]{2} [0-9]{4})}
		%EC   {([0-9]{2})}
		%EE   {(B[.]C[.](?:E[.])?|C[.]E[.]|A[.]D[.])}
		%Ex   {((?:0[1-9]|1[0-2])/(?:0[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4})}
		%EX   {((?:[0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])}
		%Ey   {([0-9]{2})}
		%EY   {([0-9]{4})}
		%g    {([0-9]{2})}
		%G    {([0-9]{4})}
		%h    {([A-Z][a-z]{2})}
		%H    {([0-1][0-9]|2[0-3])}
		%I    {(0[0-9]|1[0-2])}
		%j    {([0-2][0-9]{2}|3[0-5][0-9]|36[0-6])}
		%J    {([0-9]{6}[0-9]+)}
		%k    {([0-9]|1[0-9]|2[0-3])}
		%l    {([0-9]|1[0-2])}
		%m    {(0[1-9]|1[0-2])}
		%M    {([0-5][0-9])}
		%N    {( [0-9]||1[0-2])}
		%Od   {(0[1-9]|[1-2][0-9]|3[01])}
		%Oe   {((?: |0)[1-9]|[1-2][0-9]|3[01])}
		%OH   {([0-1][0-9]|2[0-3])}
		%OI   {(0[0-9]|1[0-2])}
		%Ok   {([0-9]|1[0-9]|2[0-3])}
		%Om   {(0[1-9]|1[0-2])}
		%OM   {([0-5][0-9])}
		%OS   {([0-5][0-9])}
		%Ou   {([1-7])}
		%Ow   {([0-6])}
		%Oy   {([0-9]{2})}
		%p    {(AM|PM)}
		%P    {(am|pm)}
		%Q    {(Stardate [0-9]+[.][0-9]+)}
		%r    {((?:[0-1][0-9]|2[0-3]):(?:[0-5][0-9]):(?:[0-5][0-9]) (?:am|pm|AM|PM)?)}
		%R    {((?:[0-1][0-9]|2[0-3]):(?:[0-5][0-9]))}
		%s    {([0-9]{9}[0-9]+)}
		%S    {([0-5][0-9])}
		%t    {(\t)}
		%T    {((?:[0-1][0-9]|2[0-3]):(?:[0-5][0-9]):(?:[0-5][0-9]))}
		%u    {([1-7])}
		%U    {(0[1-9]|[1-4][0-9]|5[0-3])}
		%V    {(0[1-9]|[1-4][0-9]|5[0-3])}
		%w    {([0-6])}
		%W    {([0-4][0-9]|5[0-3])}
		%x    {((?:0[1-9]|1[0-2])/(?:0[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4})}
		%X    {((?:[0-1][0-9]|2[0-3]):(?:[0-5][0-9]):(?:[0-5][0-9]))}
		%y    {([0-9]{2})}
		%Y    {([0-9]{4})}
		%z    {([+-](?:[0-1][0-9]|2[0-3])(?:[0-5][0-9]))}
		%Z    {([A-Z]+)}
		%%    {(%)}
		%+    {((?:[A-Z][a-z]{2}) (?:[A-Z][a-z]{2}) (?: [1-9]|[1-2][0-9]|3[01]) (?:(?:[0-1][0-9]|2[0-3]):(?:[0-5][0-9]):(?:[0-5][0-9])) (?:[A-Z]+) (?:[0-9]{4}))}
	}
	
	variable bash_format_map
	array set bash_format_map {
		bold          {1 21}
		bright        {1 21}
		dim           {2 22}
		italic        {3 23}
		underline     {4 24}
		underlined    {4 24}
		blink         {5 25}
		normal        {6 26}
		invert        {7 27}
		inverse       {7 27}
		reverse       {7 27}
		hidden        {8 28}
		invisible     {8 28}
		strikethrough {9 29}
		black         {30 39}
		red           {31 39}
		green         {32 39}
		yellow        {33 39}
		blue          {34 39}
		purple        {35 39}
		cyan          {36 39}
		white         {37 39}
		bg_black      {40 49}
		bg_red        {41 49}
		bg_green      {42 49}
		bg_yellow     {43 49}
		bg_blue       {44 49}
		bg_purple     {45 49}
		bg_cyan       {46 49}
		bg_white      {47 49}
	}
}

#
# Proc to add text formatting and colors for bash output.
#
# Example usage:
#   set str "The old man had lost his marbles"
#   puts [text_utils::bash_colorise -red $str]
#
# @param input           The string to add colours or format to
# @option -bold          formatting option
# @option -bright        formatting option
# @option -dim           formatting option
# @option -italic        formatting option
# @option -underline     formatting option
# @option -underlined    formatting option
# @option -blink         formatting option
# @option -normal        formatting option
# @option -invert        formatting option
# @option -reverse       formatting option
# @option -hidden        formatting option
# @option -invisible     formatting option
# @option -strikethrough formatting option
# @option -black         foreground color
# @option -red           foreground color
# @option -green         foreground color
# @option -yellow        foreground color
# @option -blue          foreground color
# @option -purple        foreground color
# @option -cyan          foreground color
# @option -white         foreground color
# @option -bg_black      background color
# @option -bg_red        background color
# @option -bg_green      background color
# @option -bg_yellow     background color
# @option -bg_blue       background color
# @option -bg_purple     background color
# @option -bg_cyan       background color
# @option -bg_white      background color
# @return                The string wrapped with bash escaped formatting codes
#
proc text_utils::bash_format { option input } {
	variable bash_format_map
	set option [string tolower [string trimleft $option -]]
	if {![info exists bash_format_map($option)]} {
		error "Unknown option $option passed to text_utils::bash_format"
	}
	lassign $bash_format_map($option) format endformat
	return \033\[${format}m$input\033\[${endformat}m
}

#
# Proc to capitalise every word in a string.
#
# If you need to capitalise only the first word in a sentence then have a look
# at "string totitle".
#
# For example:
#   set str "The cunning black crow tricked the lazy dog"
#   text_utils::string_capitalize $str
#
# would return:
#   The Cunning Black Crow Tricked The Lazy Dog
#
# @param input           The string to capitalise
# @return                The capitalised string
#
proc text_utils::string_capitalize { input } {
	set escaped [string map {\[ \\[ \] \\] \$ \\$ \\ \\\\} $input]
	regsub -all {([a-zA-ZÀ-ž']+)} $escaped {[string totitle &]} result
	return [subst $result]
}

#
# Proc to capitalise numbered names such as WW2, FF6, etc.
#
# Any word not starting with letters and ending with digits will remain
# unchanged.
#
# @param input           The string input to capitalise
# @return                The string input with numbered names captialised
#
proc text_utils::capitalize_numbered_names { input } {
	set words [split $input { }]
	set output {}
	foreach word $words {
		if {[regexp {^[A-Za-z]+[0-9]+$} $word]} {
			set word [string toupper $word]
		}
		lappend output $word
	}
	return [join $output { }]
}

#
# This proc attempts to generate a regular expression that should match
# date/time output produced with the given format.
#
# Example usage:
#   text_utils::convert_clock_format_to_regexp "%Y-%m-%d %H"
#
# would return:
#   ([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[01]) ([0-1][0-9]|2[0-3])
#
# Warning: Any locale-dependent format representations may not necessarily
# produce the right regular expression for all locales.
#
# @param format          The clock format to convert to a regular expression
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/clock.htm#M26
# @return                The regular expression matching the given format
#
proc text_utils::convert_clock_format_to_regexp { format } {
	
	variable format_group_regular_expressions

	return [string map $format_group_regular_expressions $format]
}

#
# A rather simplistic approach to converting a verb into the ing form by using
# lazy evaluation.
# 
# The proc uses some basic rules to determine whether the verb should get an
# additional consonant before the ing or if the the last character in the verb
# should be removed.
#
# This proc was primarily introduced for logging purposes to make the output
# more readable. For example say we have an action called "move" and we want to
# log "moving x to y" instead of the more sterile "action 'move' from x to y".
#
# Exaple conversions:
#     action  ==> actioning
#     move    ==> moving
#     play    ==> playing
#     swim    ==> swimming
#     plan    ==> planning
#     execute ==> executing
#     read    ==> reading
#
# This is obviously only intended for the English language.
#
# The approach here is generic and will not work for all possible verbs.
#
# @param verb            A given verb / string
# @return                The same string with "ing" appended
#
proc text_utils::ingify { verb } {
	set verb [string tolower $verb]
	set verb [string map {exec execute} $verb]
	
	set ing {ing}
	if {[regexp {e$} $verb]} {
		return [regsub {e$} $verb $ing]
	}
	if {[regexp {[^ae]([pnmt])$} $verb match last]} {
		return ${verb}${last}${ing}
	}
	return ${verb}${ing}
}

#
# Just a simple proc that returns a or an depending on whether
# the next work starts with a vowel or is thought to be an acronym.
#
# This is far from a fool-proof solution and will give the wrong
# article for certain words like "honour" for example.
#
# @param subject         The following word
# @return                a or an in lower case
#
proc text_utils::a_or_an { subject } {
	if {[regexp {^([aeiou]|FHLMNRSX[A-Z]|x[^aei])} $subject]} {
		return {an}
	}
	return {a}
}

#
# A rather simplistic approach to converting a verb into the ed form by using
# lazy evaluation.
# 
# The proc uses some basic rules to determine whether the verb should get an
# additional consonant before the "ed" or if the the last character in the verb
# should be removed.
#
# This proc was introduced for logging purposes to make the output more
# readable. For example say we have an action called "move" and we want to log
# "moved x to y" instead of the more sterile "action 'move' from x to y
# complete".
#
# Exaple conversions:
#     action  ==> actioned
#     move    ==> moved
#     play    ==> played
#     plan    ==> planned
#     execute ==> executed
#     stop    ==> stopped
#
# This is obviously only intended for the English language.
#
# The approach here is generic and exceptions like "swam" or "read" are not
# considered.
#
# @param verb            A given verb / string
# @return                The same string with "ed" appended
#
proc text_utils::edify { verb } {
	set verb [string tolower $verb]
	set verb [string map {exec execute} $verb]
	
	set ed {ed}
	if {[regexp {e$} $verb]} {
		set ed {d}
	}
	if {[regexp {([pn])$} $verb match last]} {
		set ed ${last}{ed}
	}
	return ${verb}${ed}
}

#
# Procedure to wrap text based on whole words.
#
# This can be used to optimise output to standard out by applying word wrap to
# the output before printing. The advantage of this is that the output may
# generally look better as the console wrapping will split words.
#
# Lines are separated by the newline character.
#
# @param text            The text to apply word wrapping to
# @param text_width      The maximum width of the text
# @param word_separator  The character or string that separates each word,
#                        defaults to the space character
# @return                The word wrapped string
#
proc text_utils::wrap_text {text text_width {word_separator { }}} {

	set output {}
	foreach line [split $text \n] {
		if {[string length $line] <= $text_width} {
			lappend output $line
			continue
		}
		foreach line_part [_line_split $line $text_width $word_separator] {
			lappend output $line_part
		}
	}

	return [join $output \n]
}

#
# Splits text into lines containing whole words.
#
# @param text            The text to split into whole words
# @param text_width      The maxium width of each line
# @see                   text_utils::wrap_text
# @return                List of lines containing whole words
#
proc text_utils::_line_split { text text_width {word_separator { }} } {

	if {[string length $text] <= $text_width} {
		return [list $text]
	}

	set i [string last $word_separator $text $text_width]

	# edge case, word separator not found within the text width
	# so we need to take the next whole word
	if {$i == -1} {
		set i [string first $word_separator $text $text_width]
		# edge case, the word separator is not found in the
		# whole text, so we return the whole text
		if {$i == -1} {
			return [list $text]
		}
	}
	set from_index [expr {$i + [string length $word_separator]}]
	lappend output [string range $text 0 [expr {$i - 1}]]
	lappend output {*}[_line_split [string range $text $from_index end] $text_width $word_separator]
	return $output
}

#
# Splits text into lines and adds padding on either the right or left side of
# each line, then joins and returns the text.
#
# @param text            The text to add padding to
# @param padding         What to pad the text with
# @param side            What side of each line to add the padding to, either
#                        left or right (defaults to left)
#
proc text_utils::pad { text padding { side left } } {

	if {$side == {left}} {
		set text $padding[string map [list \n \n$padding] $text]
	} else {
		set text [string map [list \n $padding\n] $text]$padding
	}
	return $text
}

#
# Replace every index in a given string with a given char.
#
# @param string          The base string
# @param indexes         The indexes in the given string to replace with the
#                        given character (as in string index)
# @param chars           The character (or characters) to replace the given
#                        indexes with, or alternatively a list of values to
#                        be distributed across the indexes respectively. If
#                        there are less values than indexes then the last
#                        value will be repeated for the remaining indexes.
# @return                The modified string
#
proc text_utils::index_replace { string indexes chars } {
	
	touch char
	
	set length [llength $indexes]
	for {set i 0} {$i < $length} {incr i} {
		set idx [lindex $indexes $i]
		if {$i < [llength $chars]} {
			set char [lindex $chars $i]
		}
		set csize [expr {[string length $char] - 1}]
		set string [string replace $string $idx $idx+$csize $char]
	}
	return $string
}

#
# Converts a key-value pairs as text into a dict for lookup purposes.
#
# @param text            The text to parse
# @param separator       The key-value separator, defaults to equals sign ( = )
proc text_utils::text_to_dict { text {separator {=}} {keytolower 0} } {
	set data [dict create]
	foreach line [split $text \n] {
		set eq_idx [string first $separator $line]
		if {$eq_idx == -1} {
			# No key-value pairs found on this line
			continue
		}
		set key [string range $line 0 $eq_idx-1]
		set val [string range $line $eq_idx+1 end]
		if {$keytolower} {
			set key [string tolower $key]
		}
		dict set data $key $val
	}
	return $data
}
