# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide tapp_lib 1.0

#
# Logging facility.
#
# The default log settings unless specified in config are INFO logging to
# standard out.
#
# Main features include:
#
#   - Pre-init buffering which allows for log messages to be cached and then
#     logged out when the log package initialises. This is useful for logging
#     what happens during the boot sequence of the application prior to the
#     package initialisation phase. For example it would allow the application
#     to log out what log file it is attempting to write to. It also allows the
#     developer to not worry about errors like "logging not initialised yet!".
#   - Use the same logging mechanism to log to file and standard out. The
#     benefit is that standard out logging is useful for interactive scripts,
#     but less so if running the script as a cron job for example.
#     Configuration or script initialisation parameters can disable standard
#     out logging and redirect the normal output to the log file.
#   - Log rotation. While the tapp package is intended for short lived scripts
#     there is support for automatic log rotation should this be needed.
#
# Note that most log configuration settings, such as forcing printing to
# standard out and/or enabling debug logging can normally be overridden by
# command line arguments to the application. Try passing in both ‑‑help and
# ‑‑more to the application for available parameters.
#
# The format of the log file output is currently not configurable, but may be
# addressed if there is any general interest in being able to customise this
# further.
#
# The general format of the log output looks something like this where
# multi-line log messages are logged as individual lines:
#
#         <timestamp> <level> ?<prefix>? ?<proc>? <sep> <msg_line1>
#         <timestamp> <level> ?<prefix>?                <msg_line2>
#         ...
#         <timestamp> <level> ?<prefix>?                <msg_lineN>
#
# @cfg LOG_FILE                   The log file name, can be set to stdout or
#                                 stderr if needed.
# @cfg LOG_DIR                    The directory where log files are stored, if
#                                 the directory does not already exist then it
#                                 will be automatically created
# @cfg LOG_LEVEL                  The log level the application logs at.
#                                 Available log levels are: TRACE, DEV, DEBUG,
#                                 INFO, ERROR, FATAL, STDOUT (log to standard
#                                 out instead of log file), STDERR and MUTE
#                                 which disables all logging (unless the
#                                 application actually logs at MUTE level,
#                                 which would be uncommon).
# @cfg LOG_TIME_FORMAT            Allows the date format used for the log line
#                                 time stamp to be specified. Defaults to
#                                 "%Y-%m-%d %H:%M:%S". See
#                                 http://www.tcl.tk/man/tcl8.5/TclCmd/clock.htm
#                                 for available formatting groups.
# @cfg LOG_PREFIX                 Allows for a specific prefix to be added to
#                                 the log line. This can be particularly useful
#                                 when multiple scripts are logging to the same
#                                 log file. An example of this would be a
#                                 script that runs another script and we want
#                                 the log output for both to be written to the
#                                 log file of the main application.
# @cfg LOG_PROC_PREFIX            Adds the name of the proc doing the logging
#                                 as a prefix on each log line.
# @cfg LOG_SEPARATOR              Defines the character(s) that separates the
#                                 log prefix string from the log message.
#                                 Defaults to double angle brackets (>>).
# @cfg LOG_DISABLE_STD_STREAMS    Disables logging to standard streams. With
#                                 this turned on any logging to STDERR or
#                                 STDOUT levels will be redirected to the log
#                                 file instead. This can be useful for scripts
#                                 that run as a cron job.
# @cfg LOG_AUTO_WRAP_STD_STREAMS  Ensures that the what is logged to standard
#                                 output streams are wrapped based on whole
#                                 words instead of the default wrapping
#                                 provided by the terminal. Enabled by default.
#                                 Try to disable if output formatting issues
#                                 occur.
# @cfg LOG_ROTATION               Log rotation for long running applications.
#                                 Available options: yearly, monthly, daily,
#                                 hourly, minutely, secondly and none.
#                                 The daily and hourly log rotation options are
#                                 the only ones that are recommendable. The
#                                 secondly log rotation is primarily only for
#                                 test purposes. Defaults to "none".
# @cfg VERBOSE                    Enables verbose output which is a special log
#                                 level that prints to standard out, but only
#                                 if verbose logging is enabled)
# @see                            text_utils::wrap_text for information on how
#                                 the auto wrapping works
#
namespace eval tapp_log {

	namespace export log
	namespace export log_vars
	namespace export log_level_enabled

	variable INIT 0
	variable LOGGER
	variable BUFFER {}

}

#
# Initialise the log package and open a log file for writing.
# 
# @param options         Key value pairs of log options. Generally this should
#                        not be necessary to pass in unless the application
#                        specifically needs to override log configuration set
#                        in config file.
#
proc tapp_log::init { { options {} } } {

	variable INIT
	variable LOGGER
	variable LOG_ROTATION_TIME
	variable BUFFER

	# Don't reinitialise if already done before
	if {$INIT} {
		return
	}

	# Override default configuration with with optional arguments
	foreach {cfg val} $options {
		set cfg [string trimleft [string toupper $cfg] -]
		if {![string match {LOG_*} $cfg]} {
			set cfg LOG_$cfg
		}
		cfg set $cfg $val
	}

	# Set default values unless overridden
	foreach {cfg default} [list\
		LOG_LEVEL   INFO\
		LOG_FILE    stdout\
		LOG_PREFIX  {}\
		LOG_DIR     {}\
	] {
		if {![cfg exists $cfg]} {
			cfg set $cfg $default
		}
	}

	# Set up log levels and level integer values
	set levels [list TRACE DEV DEBUG INFO WARN ERROR FATAL STDOUT MUTE]
	set index  [lsearch $levels [cfg get LOG_LEVEL]]
	foreach level [lrange $levels $index end] {
		cfg set $level [lsearch $levels $level]
	}

	# Rolling over the log file is done by a re-init.
	set log_dir  [clock format [clock seconds] -format [cfg get LOG_DIR]]
	set log_file [clock format [clock seconds] -format [cfg get LOG_FILE]]

	set LOG_ROTATION_TIME [get_next_log_rotation_time [string tolower [cfg get LOG_ROTATION none]]]

	if {[lsearch {stdout stderr} $log_file] > -1} {
		set LOGGER $log_file
	} else {
		# Attempt to create the log directory if it does not already exist.
		if {![file exists $log_dir] && $log_dir != {}} {
			file mkdir $log_dir
		}

		set LOGGER [open [file join $log_dir $log_file] a]
	}

	set INIT 1

	foreach {log_level log_msg} $BUFFER {
		log $log_level $log_msg
	}
}

#
# Log a message at a given log level.
#
# @param log_level       The log level to log the message at
# @param log_msg         The message that is to be logged. Variables will be
#                        substituted prior to being logged out if the log level
#                        is enabled.
#
proc tapp_log::log { log_level log_msg } {

	variable INIT
	variable LOGGER
	variable LOG_ROTATION_TIME
	variable BUFFER

	if {!$INIT} {
		if {[catch {
			lappend BUFFER $log_level [uplevel subst [list $log_msg]]
		} msg]} {
			lappend BUFFER $log_level [uplevel subst -nocommands [list $log_msg]]
		}
		return
	}

	if {![cfg exists $log_level]} {
		return
	}
	
	if {$log_level == {VERBOSE}} {
		if {![cfg enabled VERBOSE]} {
			return
		}
		set log_level STDOUT
	}

	if {[catch {
		if {[catch {
			set log_msg [uplevel subst [list $log_msg]]
		} msg]} {
			set log_msg [uplevel subst -nocommands [list $log_msg]]
		}
		set log_msg [string map {{\r} {}} $log_msg]
	} msg]} {
		set log_msg "Error occurred while attempting to log $log_msg\nError was: $msg"
		set log_level ERROR
	}

	set now [clock seconds]
	if {$now > $LOG_ROTATION_TIME} {
		tapp_log::close_log
		tapp_log::init
	}

	if {[catch {
		if {![cfg enabled LOG_DISABLE_STANDARD_STREAMS 0]
		    && [lsearch {STDOUT STDERR} $log_level] > -1} {
			set out [string tolower $log_level]
			
			if {[cfg enabled LOG_AUTO_WRAP_STD_STREAMS 1]} {
				puts $out [text_utils::wrap_text $log_msg [cfg get CONSOLE_WIDTH 80]]
			} else {
				puts $out $log_msg
			}
			flush $out
		} else {
			set time [clock format $now -format [cfg get LOG_TIME_FORMAT {%Y-%m-%d %H:%M:%S}]]
			lappend log_format $time
			lappend log_format $log_level

			set log_prefix [cfg get LOG_PREFIX]
			if {$log_prefix != {}} {
				lappend log_format $log_prefix
			}
		
			set sep [cfg get LOG_SEPARATOR {>>}]
			if {[cfg enabled LOG_PROC_PREFIX] && [info level] > 1} {
				set sep "> [lindex [info level -1] 0] $sep"
			}
			set indent [string repeat { } [string length $sep]]
			set format [join $log_format { }]
			foreach line [split $log_msg \n] {
				puts $LOGGER $format\ $sep\ $line
				set sep $indent
			}
			flush $LOGGER
		}
	} msg]} {
		if {![regexp {error writing "std(out|err)": broken pipe} $msg]} {
			puts stderr $msg
		}
	}
}

# 
# Debugging utility to log the values of variables at a given point in a procedure.
#
# @param log             List of what to log, either "vars", "locals" or both.
#                        The former logs all globally visible variables while
#                        the latter logs all local variables. Defaults to both.
# @param log_level       The log level to log the variables at, defaults to
#                        logging at DEBUG level.
# @param ignore          List of variable names to ignore, used to skip logging
#                        of variables that may contain sensitive information
#
proc tapp_log::log_vars { { log {vars locals} } { log_level DEBUG } { ignore {} } } {

	variable LOGGER

	if {![log_level_enabled $log_level]} {
		return
	}

	set time [clock format [clock seconds] -format [cfg get LOG_TIME_FORMAT {%Y-%m-%d %H:%M:%S}]]

	set prefix "$time $log_level LOG_VARS: "

	if {[info level] > 2} {
		puts $LOGGER "$prefix [lindex [info level -1] 0], called from [lindex [info level -2] 0]"
	}
	set variables {}
	foreach arg $log {
		if {$arg == {vars} || $arg == {locals}} {
			set variables [concat $variables [uplevel 1 [list info $arg]]]
			foreach var $ignore {
				set index [lsearch $variables $var]
				if {$index != -1} {
					set variables [lreplace $variables $index $index]
					puts $LOGGER "$prefix Not displaying variable: $var"
				}
			}
			puts $LOGGER "$prefix [llength $variables] arguments [join $variables {/}]"
		} else {
			puts $LOGGER "$prefix Unrecognised argument to log_vars: $arg"
		}
	}
	foreach variable $variables {
		if {[uplevel 1 [list array exists $variable]]} {
			foreach {name value} [uplevel 1 [list array get $variable]] {
				puts $LOGGER "$prefix $variable\($name\): $value"
			}
		} else {
			if {[uplevel 1 [list info exists $variable]]} {
				puts $LOGGER "$prefix $variable: [uplevel 1 [list set $variable]]"
			} else {
				puts $LOGGER "$prefix '$variable' is declared but doesn't exist."
			}
		}
	}
}


#
# Date/time utility to get the next whole hour or day relative to current time.
#
# For example if the time is now 14:32:19 then:
#    - the next hour will be 15:00:00
#    - the next minute will be 14:33:00
#    - the next second will be 14:32:20
#
# @param interval     The next interval to retrieve, can be one of year, month,
#                     day, hour, minute, second.
# @param time         The base time, will default to current time if not provided
# @return             The relative time in seconds
#
proc tapp_log::get_next_log_rotation_time { interval {time {}} } {
	
	if {$time == {}} {
		set time [clock seconds]
	}

	switch -exact -- $interval {
	none -
	n/a      { return [expr {[clock seconds] + 1000000000}] }
	second -
	secondly { set format {%Y-%m-%d %H:%M:%S} }
	minute -
	minutely { set format {%Y-%m-%d %H:%M:00} }
	hour -
	hourly   { set format {%Y-%m-%d %H:00:00} }
	day -
	daily    { set format {%Y-%m-%d 00:00:00} }
	month -
	monthly  { set format {%Y-%m-01 00:00:00} }
	year -
	yearly   { set format {%Y-01-01 00:00:00} }
	default {
		error "bad option \"$interval\": must be year, month, day, hour, minute, second or none"
	}
	}
	
	set next_time [clock format [clock add $time 1 $interval] -format $format]
	set next_secs [clock scan $next_time -format {%Y-%m-%d %H:%M:%S}]
	
	return $next_secs
}

#
# Check if a given log level is enabled or not.
#
# @param log_level       The log level to check if it is enabled
# @return                1 if the log level is enabled, 0 otherwise
#
proc tapp_log::log_level_enabled { log_level } {
	return [cfg exists $log_level]
}

#
# This will close the log file and re-initialise the log package.
#
proc tapp_log::reset {} {

	variable INIT
	variable BUFFER
	variable LOG_ROTATION_TIME
	variable LOGGER

	tapp_log::close_log
	catch {unset BUFFER}
	catch {unset LOGGER}
	catch {unset LOG_ROTATION_TIME}

	set BUFFER {}
	set INIT 0
}

#
# This will close the log file.
#
proc tapp_log::close_log {} {

	variable INIT
	variable LOGGER

	if {[info exists LOGGER] && [lsearch {stdout stderr} $LOGGER] == -1} {
		catch {close $LOGGER}
		set LOGGER {}
	}
	set INIT 0
}
