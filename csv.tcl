# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require csv

package provide csv_utils 1.0

#
# This package provides some additional CSV related utilities on top of the
# tcllib csv package.
#
namespace eval csv_utils {}

#
# Determine the number of columns in a CSV data set.
#
# Typically a CSV file contains an even number of columns, but in the case
# where they are not this proc can be used to find out what is the maximum
# number of columns in the given CSV data.
#
# @param csv_data        The data in CSV format
# @param separator       Character that separates columns, defaults to comma
# @param delimiter       Character that wraps content, defaults to double quote
# @return                The maximum number of columns in the CSV data
#
proc csv_utils::determine_number_of_columns { csv_data { separator , } { delimiter \" } } {
	return [table_utils::determine_number_of_columns [csv_to_table $csv_data {*}$args]]
}

#
# Remove empty columns from a CSV data set.
#
# If the data set contains some columns that are all empty then this proc can
# be used to remove those.
#
# @param csv_data        The data in CSV format
# @param ignore_header   Indicates that the CSV data contains a header and that
#                        this should be ignored when determining whether a
#                        column is empty or not
# @param separator       Character that separates columns, defaults to comma
# @param delimiter       Character that wraps content, defaults to double quote
# @return                The CSV data with empty columns removed
#
proc csv_utils::remove_empty_columns { csv_data { ignore_header 0 } args } {
	set table [csv_to_table $csv_data {*}$args]
	set idxes [table_utils::identify_empty_columns $table $ignore_header]
	set table [table_utils::remove_columns $table $idxes]
	return [table_to_csv $table {*}$args]
}

#
# Identify empty columns in a CSV data set.
#
# @param csv_data        The data in CSV format
# @param ignore_header   Indicates that the CSV data contains a header and that
#                        this should be ignored when determining whether a
#                        column is empty or not
# @param separator       Character that separates columns, defaults to comma
# @param delimiter       Character that wraps content, defaults to double quote
# @return                A list of indexes indicating empty columns
#
proc csv_utils::identify_empty_columns { csv_data { ignore_header 0 } args } {
	return [table_utils::identify_empty_columns [csv_to_table $csv_data {*}$args] $ignore_header]
}

#
# Remove a column from a CSV data set.
#
# @param csv_data        The data in CSV format
# @param indexes         A list of indexes indicating which columns to remove
# @param separator       Character that separates columns, defaults to comma
# @param delimiter       Character that wraps content, defaults to double quote
# @return                The CSV data set with the given columns removed
#
proc csv_utils::remove_columns { csv_data indexes args } {
	set table [csv_to_table $csv_data {*}$args]
	set table [table_utils::remove_columns $table $indexes]
	return [table_to_csv $table {*}$args]
}

#
# Create an ASCII table containing the CSV data.
#
# This will produce an ascii border around the CSV data for display purposes.
#
# Example usage:
#    csv_utils::convert_to_ascii_table "Col1,Col2,Col3\nVal1,Val2,Val3\nVal4,Val5,Val6"
#
# This will return:
#    +------+------+------+
#    | Col1 | Col2 | Col3 |
#    +------+------+------+
#    | Val1 | Val2 | Val3 |
#    +------+------+------+
#    | Val4 | Val5 | Val6 |
#    +------+------+------+
#
# @param csv_data                      The CSV data to convert
# @option --separator <character>      Indicates the CSV separator (the
#                                      character that separates columns),
#                                      defaults to comma (,)
# @option --delimiter <character>      Indicates the CSV delimiter (the
#                                      character that wraps content), defaults
#                                      to double quote
# @see                                 table_utils::convert_to_ascii_table for
#                                      additional formatting options
# @return                              The CSV data in text format with an
#                                      ASCII border
#
proc csv_utils::convert_to_ascii_table { csv_data args } {
	return [table_utils::convert_to_ascii_table [csv_to_table $csv_data {*}$args] {*}$args]
}

#
# Convert a CSV table into a list table (list of lists).
#
# @param csv_data                      The CSV data to convert
# @option --separator <character>      Indicates the CSV separator (the
#                                      character that separates columns),
#                                      defaults to comma (,)
# @option --delimiter <character>      Indicates the CSV delimiter (the
#                                      character that wraps content), defaults
#                                      to double quote
# @return                              The table in list form
#
proc csv_utils::csv_to_table { csv_data args } {

	let $args {
		separator        {,}
		delimiter        \"
	}
	
	touch table
	foreach line [split $csv_data \n] {
		lappend table [csv::split $line $separator $delimiter]
	}

	return $table
}

#
# Convert a tcl list table into a CSV table.
#
# @param table                         The tcl list table to convert
# @option --separator <character>      Indicates the CSV separator (the
#                                      character that separates columns),
#                                      defaults to comma (,)
# @option --delimiter <character>      Indicates the CSV delimiter (the
#                                      character that wraps content), defaults
#                                      to double quote
# @return                              The table in CSV form
#
proc csv_utils::table_to_csv { table args } {

	let $args {
		separator        {,}
		delimiter        \"
	}
	
	touch csv_data
	foreach line $table {
		lappend csv_data [csv::join $line $separator $delimiter]
	}

	return [join $csv_data \n]
}
