#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# APPTAG|tcl|notify|Triggers notification through the tapp package, primarily for testing purposes
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname [file dirname $script]]

package require tapp 1.0
package require notifications 1.0

namespace eval notify {}

# Defaults
cfg set SUBJECT {Notification}
cfg set BODY {Test Message}
cfg set APP {notify.tcl}
cfg set ICON {}
cfg disable DBUS_NOTIFICATION
cfg set LOG_FILE stdout
cfg set HELP \
{Usage: notify [OPTION]...

Sends a desktop notification using the tapp library.

This application is primarily intended for testing purposes.

Optional arguments:
%args%
(arguments are not dependent on which order they come in)

Warning! Signal failure imminent.
}

proc notify::main { argv } {

	if {[llength $argv] == 0} {
		lappend argv --help
	}
	
	param parse $argv remaining_args
	
	if {$remaining_args != {}} {
		cfg set BODY $remaining_args
	}

	if {$argv != {--help}} {
		notifications::notify [cfg get ICON] [cfg get SUBJECT] [cfg get BODY] [cfg get APP]
	}
}

proc notify::init {} {
	
	param register -s --subject\
		--config SUBJECT\
		--max_args 1\
		--comment "set the notification subject, defaults to \"[cfg get SUBJECT]\""
		
	param register -a --app\
		--config APP\
		--max_args 1\
		--comment "set the application (sender), relevant for growl only, defaults to \"[cfg get APP]\""

	param register -m --msg\
		--config BODY\
		--max_args 1\
		--comment "set the notification message, defaults to \"[cfg get BODY]\""

	param register -i --icon\
		--config ICON\
		--max_args 1\
		--comment {set the notification icon (path to file)}
		
	param register {} --dbus\
		--config DBUS_NOTIFICATION\
		--comment {enables DBUS notifications (linux only), requires dbus-tcl to be installed}
		
	param register -t {--test <num lines>}\
		--body {
			if {![llength $value]} {
				set value 10
			}
			set body {}
			for {set i 0} {$i < $value} {incr i} {
				lappend body [subst [string repeat {[format %c [expr {int(rand() * 26) + (rand() > .5 ? 97 : 65)}]]} [expr {int(rand()*100)}]]]
			}
			cfg set BODY [join $body \n]
		}\
		--max_args 1\
		--comment {enables DBUS notifications (linux only), requires dbus-tcl to be installed}
}
	
if {[info exists argv]} {
	notify::init
	notify::main $argv
}