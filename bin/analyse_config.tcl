#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# APPTAG|tcl|analyse_config|Parses config files and shows all configuration items set
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname [file dirname $script]]

package require tapp 1.0

namespace eval analyse_config {}

cfg set LOG_FILE stdout
cfg set HELP \
{Usage: analyse_config [OPTION]... [FILE]...

Parses given configuration files and prints out all configuration items set and their final values.

Optional arguments:
%args%
(arguments are not dependent on which order they come in)

Warning! Misuse may cause injury or death.
}

proc analyse_config::main { argv } {
	
	if {[llength $argv] == 0} {
		lappend argv --help
	}

	param parse $argv remaining_args
	param separate $remaining_args app_args files dirs

	process_files $files
}

proc analyse_config::init {} {

	param register {} {--cfg <item1,itemN>}\
		--body {
			foreach cfg_val [split $value {,}] {
				cfg lappend CFG_FILTER $cfg_val
				log DEBUG {Added $cfg_val as config filter}
			}
		}\
		--max_args 1\
		--comment {only list the given config items}
		
	param register -V --value-only\
		--config CFG_RETURN_VALUE_ONLY\
		--comment {omit key name when printing config value}
		
	param register -K --key-only\
		--config CFG_RETURN_KEY_ONLY\
		--comment {omit value when printing config}
		
	param register {} --bash\
		--config CFG_RETURN_BASH_FORMAT\
		--comment {list the config items as bash variables}
}

proc analyse_config::process_files { files } {

	set cfg_filter [cfg get CFG_FILTER]
	set analyse_cfg [cfg unload]

	cfg reset
	foreach file $files {
		cfg file $file
	}

	set file_cfgs [cfg print $cfg_filter]
	
	cfg reset
	cfg load $analyse_cfg

	if {[cfg enabled CFG_RETURN_VALUE_ONLY]} {
		foreach line [split $file_cfgs \n] {
			set value [string range $line [string first = $line]+2 end]
			log STDOUT {$value}
		}
	} elseif {[cfg enabled CFG_RETURN_KEY_ONLY]} {
		foreach line [split $file_cfgs \n] {
			set key [string range $line 0 [string first = $line]-2]
			log STDOUT {$key}
		}
	} elseif {[cfg enabled CFG_RETURN_BASH_FORMAT]} {
		foreach line [split $file_cfgs \n] {
			if {[string trim $line] == {}} {
				continue
			}
			set eq_idx [string first = $line]
			set value [string range $line $eq_idx+2 end]
			set value [string map {\" {\"}} $value] 
			set key [string range $line 0 $eq_idx-2]
			log STDOUT {$key="$value"}
		}	
	} else {
		log STDOUT {$file_cfgs}
	}
}

if {[info exists argv]} {
	analyse_config::init
	analyse_config::main $argv
}