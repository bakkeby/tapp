# Copyright (c) 2014 Stein Gunnar Bakkeby all rights reserved.

package provide opt 1.0

#
# Create a proc that provides a variety of functions based on second class options.
#
namespace eval opt {
	variable PROCS
}

#
# Create a single proc that can perform many actions depending on the options
# provided.
#
# This is similar to the struct:: packages part of tcllib with the main
# difference being that you can add additional options / procs to call.
#
# Default proc options:
#     <proc> options                Returns a list of the available options for
#                                   the proc
#     <proc> destroy                Deletes the generated proc / command. Note
#                                   that this will not unset any namespace
#                                   variables used. If this is used in a
#                                   context where procs are dynamically created
#                                   and destroyed / deleted then it is
#                                   recommended that the "destroy" option is
#                                   overridden by a proc that takes care of
#                                   any cleanup necessary.
#
# Synopsis:
#    opt::proc <proc> ?options? ?options?
#
# @param name              The name of the new proc. The current namespace will
#                          be used if not specified.
# @option -preset <name>   Specifies which options that should be available when
#                          the proc is created. Available presets are: list and
#                          array
# @see                     opt::option for how to add custom options to the
#                          created proc
# @see                     opt_array::preset
# @see                     opt_list::preset
# @see                     http://tcllib.sourceforge.net/doc/
# @return                  The name of the new proc including namespace
# 
proc opt::proc {name args} {
	
	variable PROCS
	
	set pname [_get_process_name $name]
	
	::proc $pname {option args} [subst {
		return \[opt::_parse $pname \$option \{*\}\$args\]
	}]
	
	_setup_opts $pname {*}$args

	return $pname
}

::proc opt::_setup_opts {pname args} {

	# Global options
	opt::option $pname options --proc [list opt::_options $pname]
	opt::option $pname destroy --proc [list opt::destroy $pname]
	
	set preset {}
	set idx [lsearch $args --preset]
	if {$idx > -1} {
		set preset [lindex $args $idx+1]
	}
	
	if {$preset != {}} {
		if {[info commands ::opt_${preset}::preset] != {}} {
			opt_${preset}::preset $pname {*}$args
		}
	}
}

#
# Adds options to the given proc.
#
# Synopsis:
#    opt::option <proc> <option> <cmd> ?<option> <cmd>?
#
# Example:
#    set pname [opt::proc good]
#    opt::option $pname morning [list puts {hello there}]
#    good morning
#
# The above will create a proc good and specify an option "morning". Passing
# the "morning" option as an argument when calling the command "good" will
# result in "hello there" to be printed to standard out. Alternatively the
# proc parameter names and body can be passed in to define anonymous procs
# (or functions) evaluated through the "apply" command.
#
# Take special notes regarding the use of a Tcl body:
#    - this will be arbitrary Tcl code that will run one level
#      below the calling code
#    - if namespace variables are used then they need to be
#      addressed in full, i.e. with the namespace prefix
# 
# @param pname             The proc name to add the option(s) for
# @param option            The name of the option to add, e.g. "get"
# @option --proc cmd       The proc to execute when the option is passed in,
#                          can be a list representing command followed by
#                          default arguments (like passing in the name of the
#                          namespace variable to use)
# @option --params <list>  Anonymous proc parameters, defaults to "args"
# @option --body <code>    Anonymous Tcl body to execute when option is called
# @see                     code for proc opt_list::preset for example usage
# @see                     opt::get_namespace_var in order to get hold of the
#                          proc's namespace variable
# @see                     apply
#
::proc opt::option {name option args} {
	variable PROCS
	
	set pname [_get_process_name $name]

	foreach param {proc params body} {
		set $param {}
		set idx [lsearch $args --$param]
		if {$idx > -1} {
			set $param [lindex $args $idx+1]
		}
	}
	
	if {$proc != {}} {
		set PROCS($pname,$option) "args \{\n\t\tuplevel 1 $proc \$args\n\t\}"
	} else {
		set PROCS($pname,$option) [list $params $body]
	}
	
}

#
# If the generated proc is only for temporary use then it can be destroyed.
#
# Note that this proc will not unset the proc's namespace variable and that
# this will need to be dealt with separately in order to prevent memory leaks.
#
# @param name            The name of the proc to remove
#
::proc opt::destroy {name} {

	variable PROCS

	set pname [_get_process_name $name]
	
	if {[info commands $pname] == $pname} {
		rename $pname {}
	} elseif {[info commands $name] == "::$name"} {
		rename $name {}
	}
	
	array unset PROCS $pname,*
}

#
# Returns a list of the proc options including the parameters and the body.
# Intended for debug situations.
#
# @param name            The name of the proc to list options for
# @return                The proc name along with the option parameter and body
# 
::proc opt::explain {name} {

	variable PROCS
	
	set pname [_get_process_name $name]
	
	set output {}
	foreach option [_options $pname] {
		lappend output "\t$pname $option $PROCS($pname,$option)"
	
	}
	
	return [join $output \n]
}

::proc opt::_options {name} {

	variable PROCS
	set options {}
	foreach opt [array names PROCS $name,*] {
		lappend options [string range $opt [string length $name]+1 end]
	}
	return $options
}

::proc opt::_parse {name option args} {

	variable PROCS

	if {![info exists PROCS($name,$option)]} {
		set options [opt::_options $name]
		if {[llength $options] > 1} {
			set options [lreplace $options end end [concat or [lindex $options end]]]
		}
		uplevel 1 "error \{unknown or ambiguous subcommand \"$option\": must be [join $options ,\ ]\}"
	}
        
	set res {}
	if {[catch {
		append res [uplevel 2 apply [list $PROCS($name,$option)] $args]
	} msg]} {
		regsub {apply lambdaExpr} $msg $name\ $option msg
		if {0 && [regexp {^[\s\n]*uplevel 1 ([^\s]+)} [lindex $PROCS($name,$option) 1] --> cmd]} {
			set body [lindex $PROCS($name,$option) 1]
			set regexp "should be \"$cmd"
			for {set i 2} {$i < [llength $body] - 2} {incr i} {
				append regexp { [^ ]+}
			}
			regsub $regexp $msg "should be \"$name $option" msg
		}
		puts "error is $msg"
		error $msg
	}
	
	return $res
}

#
# For a given namespace proc returns the name of a namespace variable with the
# same name (in uppercase).
#
# For example:
#    opt::get_namespace_var {::ns::etc::myproc}
#
# will return:
#    ::ns::etc::MYPROC
#
# @param pname           The proc name (including namespace)
# @return                The equivalent namespace variable (in upper case)
#
::proc opt::get_namespace_var { pname } {

	set plist [split $pname {:}]
	set plist [lreplace $plist end end [string toupper [lindex $plist end]]]
	set vname [join $plist {:}]

	return $vname
}

::proc opt::_get_process_name { name } {

	if {[string match ::* $name]} {
		set pname $name
	} else {
		set parent_namespace [uplevel 2 { namespace current }]
		if {$parent_namespace == {::}} {
			set pname ::$name
		} else {
			set pname ${parent_namespace}::$name
		}
	}
	
	return $pname
}
