# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide tapp_lib 1.0

#
# This package provides a series of language additions to Tcl.
#
# Most of these are for convenience while others are primarily for debugging
# purposes. All language additions (procs) are pure Tcl implementations.
#
namespace eval tapp_lang {
	namespace export assert
	namespace export this
	namespace export this_dir
	namespace export this_script
	namespace export do
	namespace export decr
	namespace export nvl
	namespace export default
	namespace export touch
	namespace export clear
	namespace export vputs
	namespace export lremove
	namespace export lsplit
	namespace export ldefault
	namespace export larrange
	namespace export get_arg
	namespace export prepend
}

# Whether assert statements are enabled or not, may be disabled for performance reasons.
set ::tapp_lang::ASSERT 1

#
# Assert functionality for a Tcl application.
#
# If the assert fails a Tcl error will be thrown.
#
# @param expression      The expression to assert, for example $myVar == 1.
#                        The expression is run through a normal Tcl expr
#                        statement. If the expression evaluates to false then
#                        a Tcl error is thrown.
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/expr.htm
#
proc tapp_lang::assert { expression } {

	if {$::tapp_lang::ASSERT && ![uplevel 1 [list expr $expression]]} {
		error "assertion failed: $expression ([uplevel 1 [list subst $expression]])"
	}
}

#
# This adds the rarely used do while functionality to Tcl.
#
# Synopsis:
#    do command while test
#
# Example usage:
#
#    do {
#            incr i
#            lappend result $i
#    } while {$i < 3}
#
# @param command         The command to execute, i.e. the body of the do while
#                        block
# @param while           Expected to be plain text "while" for readability
#                        purposes, value is not used
# @param test            The while test / condition as with a normal while loop
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/while.htm
#
proc tapp_lang::do args {
	if {[llength $args] != 3} {
		error {wrong # args: should be "do command while test"}
	}
	
	lassign $args script while test
	uplevel 1 set \u0 1
	uplevel 1 while \{\[set \u0\] || $test\} \{set \u0 0\;eval \{$script\}\}
	uplevel 1 unset \u0
}

#
# This is essentially the opposite of the Tcl command "incr".
#
# To decrement a value in Tcl the usual approach is to increment a value with a
# negative value, for example:
#
#    incr i -1
#    incr i -2
#
# This proc is a short hand notation for the above, example usage:
#
#    decr i
#    decr i 2
#
# Synopsis:
#    decr varName ?decrement?
#
# @param varName         The local variable to decrement
# @param decrement       How much to decrement the value by, defaults to 1
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/incr.htm
#
proc tapp_lang::decr args {

	if {![llength $args] in {1 2}} {
		error {wrong # args: should be "decr varName ?decrement?"}
	}

	set increment -1
	if {[lindex $args 1] != {}} {
		set integer [lindex $args 1]
		if {![string is integer $integer]} {
			error [subst {expected integer but got "$integer"}]
		}
		set increment [expr {$increment * $integer}]
	}

	uplevel 1 [list incr [lindex $args 0] $increment]
}

#
# This proc is the logical opposite of append.
#
# The variable given will be get the value(s) prepended to it.
#
# Example usage:
#    prepend str "_"
#
# is equivalent to:
#    set str "_${str}"
#
# Synopsis:
#    prepend varName ?value value ...?
#
# @param varName         The name of the variable to prepend the given string
#                        to
# @param value           The string to prepend the variable with
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/append.htm
# @return                The prepended string
# 
proc tapp_lang::prepend { varName args } {
	upvar 1 $varName s
	default s {}
	return [set s [join $args {}]${s}]
}

#
# A reference from object orientation methodology where "this" refers to the
# current application or script running.
#
# Note that symlinks are automatically resolved when looking up the real path
# of the running script.
#
# This can be used to identify the script running and access resources that are
# located relative to the script installation directory.
#
# @see                   file_utils::get_real_path
# @return                The real path of the script running
#
proc tapp_lang::this {} {
	return [file_utils::get_real_path [info script]]
}

#
# Returns the directory of the current script running.
#
# @see                   tapp_lang::this
# @return                The real path to the directory of the script running
#
proc tapp_lang::this_dir {} {
	return [file dir [this]]
}

#
# Returns the name of the script running.
#
# For example let's say we are running the following script:
#    /path/to/examine.tcl
#
# calling the "this_script" procedure would return:
#    examine
#
# @return                The name of the script running without file extension
#
proc tapp_lang::this_script {} {
	return [file tail [file rootname [this]]]
}

#
# Inspired by database systems the nvl (null value) will return the given
# default value if the variable does not exist, otherwise the value of the
# variable itself is returned.
#
# @param variable        The variable to check if it is defined or not
# @param default         The value to return if the variable does not exist
# @return                The value of the variable if it exists, returns the
#                        provided default variable if it doesn't
#
proc tapp_lang::nvl { variable { default {} } } {

	set var [lindex $variable 0]
	regsub {,$}   $var {} var
	regsub {^[$]} $var {} var
	upvar 1 $var variable_to_test

	if {[info exists variable_to_test]} {
		return $variable_to_test
	}
	
	foreach default_value [concat [lrange $variable 1 end] $default] {
		if {$default_value == {,}} {
			continue
		}
		return $default_value
	}

	return
}

#
# Short hand notation for checking whether a variable exists.
#
# If it does not exist then set the variable to given default value.
#
# @param variable        The variable to check whether it exists
# @param default         The value to set the variable to if it does not exist
# @return                1 if the variable was set, 0 otherwise
#
proc tapp_lang::default { variable { default {} } } {

	upvar 1 $variable variable_to_check
	if {![info exists variable_to_check]} {
		set variable_to_check $default
		return 1
	}
	return 0
}

#
# Short hand notation for initialising variables to empty strings.
#
# Inspired by the unix tool with the same name for updating timestamps and/or
# creating new empty files.
#
# Note that it does not set or change the variable if the variable already
# exists. Primarily used to ensure that variables exist.
#
# Example usage:
#    touch someVar anotherVar
#
# is equivalent of:
#    set someVar {}
#    set anotherVar {}
#
# Synopsis:
#    touch ?varName varName ...?
#
# @param varName         The variable to initialise if it is currently
#                        undefined
# @see                   Also see tapp_lang::default
#
proc tapp_lang::touch args {
	foreach arg $args {
		upvar 1 $arg var
		if {![info exists var]} {
			set var {}
		}
	}
}

#
# Short hand notation for setting variables to empty strings, but leaving them
# initialised.
#
# This is typically used to reset variables in a for loop back to being empty
# strings.
#
# Synopsis:
#    clear ?varName varName ...?
#
# @param varName         The variable name to set to be an empty string
#
proc tapp_lang::clear args {
	foreach arg $args {
		upvar $arg var
		set var {}
	}
}

#
# Short hand notation for printing out the value of a set of variables.
#
# This is primarily a development / debug tool that allows for a one-time
# inspection of variable values by printing to standard out.
#
# For example:
#    vputs someVar anotherVar
#
# is equivalent of:
#    puts "someVar = $someVar"
#    puts "anotherVar = $anotherVar"
#
# While this may sometimes be useful in order to get to the bottom of an issue
# the general recommendation is to set up relevant tests instead.
#
# Synopsis:
#    vputs ?varName varName ...?
#
# @param varName         The name of the variable to print to standard out
#
proc tapp_lang::vputs args {
	foreach arg $args {
		upvar $arg var
		if {[info exists var]} {
			puts "$arg = $var"
		} else {
			puts "$arg does not exist"
		}
	}
}

#
# Log a stack trace at a required point in the execution of the script.
#
proc tapp_lang::stack_trace {} {
	set r 0
	set level 1
	while { $r != 1 } {
		set r [catch {info level [expr [info level] - $level]} e]
		if {!$r} {
			log DEBUG "Called by ${e}."
		}
		incr level
	}
}

#
# Removes all list entries that matches any of the given elements.
#
# For example:
#    lremove [list a {} b {} c] {}]
#
# will remove all empty values from the list and return {a b c}
#
# Synopsis:
#    lremove list ?element element ...?
#
# @param list            The list to remove elements from
# @param element         The element to remove from the list
# @return                The list with the given elements removed
#
proc tapp_lang::lremove { list args } {

	foreach element $args {
		set list [lsearch -inline -all -not $list $element]
	}
	return $list
}

#
# Splits a list into several based on the amount of variables passed in.
#
# For example splitting key value pairs:
#    lsplit [list key1 val1 key2 val2 key3 val3] keys vals
#
# will result in:
#    set keys {key1 key2 key3}
#    set vals {val1 val2 val3}
#
# If there are not enough values to fulfil a whole set across all variables
# then the remaining values will be filled up with empty strings.
#
# Synopsis:
#    lsplit list ?varname1? ?varname2? ... ?varnameN?
#
# @param list            The list of elements to split into several lists
# @param args            Two or more variable names to store the new lists in
#
proc tapp_lang::lsplit { list args } {

	foreach $args $list {
		foreach arg $args {
			lappend l_$arg [set $arg]
		}
	}
	
	foreach arg $args {
		uplevel 1 "set $arg {[set l_$arg]}"
	}
}

#
# Checks if a given index in a list is an empty string,
# and if so it will default it to the first non-empty
# value provided.
#
# @param list            The name of the list to check
# @param index           The index in the list to use
# @param args            Possible default parameters
#
proc tapp_lang::ldefault { list index args } {
	upvar 1 $list _list

	if {[lindex $_list $index] == {}} {
		while {[llength $_list] <= $index} {
			lappend _list {}
		}
		lset _list $index [lsearch -inline -not $args {}]
	}
}

#
# Re-arranges list entries based on options and patterns.
#
# For example:
#    larrange -prioritise [list a b c] "c"
#
# will prioritise the "c" element by moving it to the start of the list.
#
# Synopsis:
#    larrange ?-option value ...? list pattern
#
# @option -prioritise    Prioritise elements matching the given pattern by
#                        moving them to the start of the list
# @option -deprioritise  Deprioritise elements matching the given pattern by
#                        moving them to the end of the list
# @option -glob          Use glob matching for:4 the pattern (default)
# @option -regexp        Use regexp matching for the pattern
# @option -exact         Pattern is a literal string that is compared for exact
#                        equality against each list element
# @option -list          The list to re-arrange
# @option -pattern       The pattern that indicates which elements to arrange
# @option -patterns      Declares a list of patterns / elements to arrange
# @param list            The list to re-arrange, presumed to be the next to
#                        last parameter unless -list option is specified
# @param pattern         The pattern, presumed to be the last parameter unless
#                        the -pattern option is specified
# @return                The re-arranged list
#
proc tapp_lang::larrange { args } {

	set match_method -glob
	touch task list patterns

	# Parse arguments
	for {set i 0} {$i < [llength $args]} {incr i} {
	
		set arg [lindex $args $i]
		
		switch -exact -- $arg {
		-glob -
		-exact -
		-regexp {
			set match_method $arg
		}
		-prioritise -
		-deprioritise {
			set task $arg
		}
		-list {
			set list [lindex $args [incr i]]
		}
		-pattern {
			set patterns [list [lindex $args [incr i]]]
		}
		-patterns {
			set patterns [lindex $args [incr i]]
		}
		default {
			if {![llength $list]} {
				set list $arg
			} elseif {![llength $pattern]} {
				set pattern $arg
			} else {
				log WARN {larrange: Unknown argument $arg}
			}
		}
		}
	}
	
	# Validate that we have a list and a pattern
	if {![llength $list] || ![llength $patterns]} {
		error {wrong # args: "should be larrange ?-option value ...? list pattern"}
	}

	foreach patt $patterns {
		set matches [lsearch $match_method -inline -all $list $patt]
		set rest    [lsearch $match_method -inline -all -not $list $patt]
		switch -exact -- $task {
		-prioritise   { set list [concat $matches $rest] }
		-deprioritise { set list [concat $rest $matches] }
		}
	}

	return $list
}

#
# Resets the following for test purposes.
#
#    - local variables
#    - visible variables
#    - configuration settings
#
proc tapp_lang::_reset_all {} {
	cfg reset
	uplevel 1 {
		tapp_lang::_reset_local_variables
		tapp_lang::_reset_visible_variables
	}
}

#
# Resets (or unsets) all visible variables in the calling proc.
#
proc tapp_lang::_reset_visible_variables {} {
	uplevel 1 {
		unset {*}[info vars]
	}
}

#
# Resets (or unsets) all local variables in the calling proc.
#
proc tapp_lang::_reset_local_variables {} {
	uplevel 1 {
		unset {*}[info locals]
	}
}

#
# Helper proc intended to be used in relation to trace when commands are
# executed.
#
# Usage:
#    trace add execution $value enter tapp::_watch_command_enter
#
proc tapp_lang::_watch_command_enter {cmdstring op} {

	set output {}
	lappend output "  --> $cmdstring"
	for {set i [expr {[info level] - 1}]} {$i > 0} {decr i} {
		set info [info level $i]
		set proc [lindex $info 0]
		
		set ns [uplevel [expr {[info level] - $i}] namespace current]
		if {$ns != {::} && [info command ${ns}::$proc] != {}} {
			set proc [string trimleft ${ns}::${proc} :]
		}
		
		set index 0
		set arg_print {}
		set argv [info args $proc]
		if {$argv != {}} {
			set values {}
			foreach arg $argv {
				lappend values \"[lindex $info [incr index]]\"
			}
			set arg_print " {$argv} values {[join $values { }]}"
		}
		lappend output "     - called by ${proc}${arg_print}"
	}
	
	log STDOUT [join $output \n]
}

#
# Helper proc intended to be used in relation to trace when commands are
# exiting.
#
# Usage:
#    trace add execution $value leave tapp::_watch_command_leave
#
proc tapp_lang::_watch_command_leave {cmdstring code result op} {

	set output {}
	if {$code != 0} {
		lappend output "  <-- return code is $code"
	}
	if {$result != {}} {
		lappend output "  <-- return $result"
	} else {
		lappend output "  <--"
	}
	
	log STDOUT [join $output \n]
}

#
# Helper proc intended to be used in relation to trace variable commands to
# log when a variable is set and read.
#
# Example usage:
#    trace variable <varname> w tapp_lang::_watch
#    trace variable <varname> r tapp_lang::_watch
#
proc tapp_lang::_watch {varname key op} {
        if {$key != ""} {
                set varname ${varname}($key)
	}
        upvar $varname var
	if {$var == {}} {
		lappend output "$varname is empty (operation $op)"
	} else {
		lappend output "$varname is $var (operation $op)"
	}

	switch -- $op {
	{write} {
		set prefix {  - set by }
	}
	{array} -
	{read} {
		set prefix {  - read by  }
	}
	{unset} {
		set prefix {  - unset by }
	}
	default {
		set prefix "  - $op by "
	}
	}

	for {set i [expr {[info level] - 1}]} {$i > 0} {decr i} {
		set info [info level $i]
		set proc [lindex $info 0]
		
		set ns [uplevel [expr {[info level] - $i}] namespace current]
		if {$ns != {::} && [info command ${ns}::$proc] != {}} {
			set proc [string trimleft ${ns}::${proc} :]
		}

		set index 0
		set arg_print {}
		set argv [info args $proc]
		if {$argv != {}} {
			set values {}
			foreach arg $argv {
				lappend values \"[lindex $info [incr index]]\"
			}
			set arg_print " {$argv} values {[join $values { }]}"
			set index 0
		}
		
		lappend output "${prefix} ${proc}${arg_print}"
		set prefix {  - called by }
	}
	log STDOUT [join $output \n]
}

#
# Promote the string subcommands to first level procs.
#
foreach subcommand {
	bytelength compare equal first index is last length map match range repeat
	replace reverse tolower totitle toupper trim trimleft trimright wordend wordstart
} {
	proc $subcommand args [subst { string $subcommand {*}\$args }]
}