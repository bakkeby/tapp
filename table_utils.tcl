# Copyright (c) 2015, Stein Gunnar Bakkeby, all rights reserved.

package provide tapp_lib 1.0

#
# This package deals with common tcl two-dimensional list tables (lists within lists).
#
namespace eval table_utils {
	
	variable ASCII_TABLE_PRESETS
	
	set ASCII_TABLE_PRESETS(markdown) {
		--hl 0\
		--hb 0\
		--hlheader 0\
		--alignment_row 1\
	}
	
	set ASCII_TABLE_PRESETS(mysql) {
		--colchars {| + + +}\
		--rowchars {- + + +}\
		--borderchars {- - | |}\
		--hl 0\
		--hlheader 1\
	}
	
	set ASCII_TABLE_PRESETS(jira) {
		--hl 0\
		--headercolchars {|| || || ||}\
		--borderchar {{} {}}\
		--hlheader 0\
		--hb 0\
	}
}



#
# Process to identify columns that are empty in a tcl list table.
#
# @param table             The tcl list table to search through
# @param ignore_first_row  Indicates whether to ignore the first row when
#                          running the check. This is typically used when
#                          the table contains a header.
# @return                  The list indexes of the columns that were found to
#                          be empty
#
proc table_utils::identify_empty_columns { table { ignore_first_row 0 } } {

	# Determining the maximum number of columns
	set num_cols 0
	foreach row $table {
		if {[llength $row] > $num_cols} {
			set num_cols [llength $row]
		}
	}
	if {$ignore_first_row} {
		set table [lreplace $table 0 0]
	}
	touch output
	for {set i 0} {$i < $num_cols} {incr i} {
		set content_found 0
		foreach row $table {
			if {[lindex $row $i] != {}} {
				set content_found 1
				break
			}
		}
		if {!$content_found} {
			lappend output $i
		}
	}

	return $output
}

#
# Determine the number of columns in a table.
#
# Typically a table contains an even number of columns, but in the case
# where they are not this proc can be used to find out what is the maximum
# number of columns in the given table.
#
# @param table           The data in list format
# @return                The maximum number of columns in the table
#
proc table_utils::determine_number_of_columns { table } {

	set max_cols 0
	foreach line $table {
		set cols [llength $line]
		if {$cols > $max_cols} {
			set max_cols $cols
		}
	}
	
	return $max_cols
}

#
# Process to remove columns from a given tcl list table.
#
# @param table           The tcl list table to remove columns from
# @param indexes         The indexes of the columns to remove
# @return                The table with the columns removed
#
proc table_utils::remove_columns { table indexes } {

	touch output
	set indexes [lsort -decreasing -integer $indexes]
	foreach row $table {
		foreach col_idx $indexes {
			set row [lreplace $row $col_idx $col_idx]
		}
		lappend output $row
	}
	
	return $output
}

#
# Procedure to get a list of max column sizes for a list table (list of lists).
#
# @param data            A list containing lists representing rows in a table
# @return                A list of maximum column sizes
#
proc table_utils::get_column_sizes { data } {

	set cols {}
	
	# determine max column lengths
	foreach row $data {
		set col_count [llength $row]
		for {set i [llength $cols]} {$i < $col_count} {incr i} {
			lappend cols {0}
		}

		for {set i 0} {$i < $col_count} {incr i} {
			set col_length [string length [lindex $row $i]]
			if {[lindex $cols $i] < $col_length} {
				lset cols $i $col_length
			}
		}
	}

	return $cols
}

#
# Add application specific ASCII table presets for convenience.
#
# @param name            The name of the preset to set
# @param args            The default parameters the preset should use
# @see                   table_utils::convert_to_ascii_table for available
#                        parameters
#
proc table_utils::add_ascii_table_preset { name args } {

	variable ASCII_TABLE_PRESETS
	set ASCII_TABLE_PRESETS($name) $args
}

#
# Create an ASCII table from a multidimensional list for display purposes.
#
# Example usage:
#    table_utils::convert_to_ascii_table [list\ 
#            {Col1 Col2 Col3}\
#            {Val1 Val2 Val3}\
#            {Val4 Val5 Val6}\
#    ]
#
# This will return:
#    |------|------|------|
#    | Col1 | Col2 | Col3 |
#    |------|------|------|
#    | Val1 | Val2 | Val3 |
#    |------|------|------|
#    | Val4 | Val5 | Val6 |
#    |------|------|------|
# 
# The below tries to explain what parameters control what aspect of the table
# generation.  When specifying the characters to use if less than four
# characters are specified in then the rest will default accordingly.
#
#      ‚----------------------------------‚--  --vb 1
#     /                                  /
#    /=====================\            /
#    X   Table Title       Z           /
#    /==========@==========@==========\        --hb 1
#    «   Col1   Y   Col2   Y   Col3   »        --header 1
#    X..........?..........?..........Z        --hlheader 1
#    <:--------:!:---------!---------:>        --alignment_row 1
#    (~~~~~~~~~~o~~~~~~~~~~o~~~~~~~~~~)        --hl 1
#    <   Val1   !   Val2   !   Val3   >
#    (~~~~~~~~~~o~~~~~~~~~~o~~~~~~~~~~)        --hl 1
#    <   Val4   !   Val5   !   Val6   >
#    \__________U__________U__________/        --hb 1
#                \          \      \\\
#                 \          \       \
#                  \          \       ‛------  --vpadding 3
#                   ‛----------‛-------------  --vl 1
#
# The parameters needed to generate the above:
#    --alignment_row 1
#    --col_align {c l r}
#    --colchars {! o @ U}
#    --rowchars {~ o ( )}
#    --borderchars {= _ < >}
#    --headerrowchars {. ? X Z}
#    --headercolchars {Y {} « »}
#    --edgechars {/ \\ \\ /}
#    --title {Table Title}
#    --title_colspan 2
#    --vpadding 3
#
# @param table                         The multidimensional list to generate
#                                      the ASCII table from
# @param args                          Optional formatting arguments
# @option --header <1/0>               Indicates whether a header is present in
#                                      the table or not, defaults to 1
# @option --title <string>             Add an optional title at the top of the
#                                      table
# @option --title_colspan <number>     The number of columns that the title
#                                      should span. Defaults to all columns.
# @option --border <1/0>               Indicates whether ASCII border (vertical
#                                      and horizontal) is enabled, defaults to
#                                      enabled
# @option --hb <1/0>                   Indicates whether horizontal border is
#                                      enabled or not, defaults to border value
# @option --vb <1/0>                   Indicates whether vertical border is
#                                      enabled or not, defaults to border value
# @option --hl <1/0>                   Indicates whether to include horizontal
#                                      line separator or not, defaults to
#                                      enabled
# @option --hlheader <1/0>             Indicates whether to include a special
#                                      horizontal line separator for the header
#                                      or not, defaults to enabled
# @option --alignment_row <1/0>        Indicates that a special horizontal
#                                      alignment row should be inserted.
#                                      Required for certain table formats such
#                                      as the markdown syntax used on github
#                                      and bitbucket.
# @option --col_align <list>           List indicating how each column is
#                                      aligned. Used by the special horizontal
#                                      line separator if applicable.
#                                      The values are limited to: Left,
#                                      Right and Center (l, r, c and empty
#                                      string)
# @option --vl <1/0>                   Indicates whether to include vertical
#                                      line separator or not, defaults to
#                                      enabled
# @option --borderchars <list>         Specifies the characters used for the
#                                      border. The list is on the form: "<top>
#                                      <bottom> <left> <right>"
# @option --edgechars <list>           Specifies the edge characters where the
#                                      borders meet. The list is on the form:
#                                      "<top left> <top right> <bottom left>
#                                      <bottom right>".
# @option --rowchars <list>            Specifies the row separator characters
#                                      used. The list is on the form: "<row
#                                      character> <column intersection> <left
#                                      border> <right border>". The border
#                                      characters overrides the main border.
# @option --colchars <list>            Specifies the column separator
#                                      characters used. The list is on the
#                                      form: "<column character> <row
#                                      intersection> <top border> <bottom
#                                      border>". The border characters
#                                      overrides the main border. The second
#                                      value is the same as rowchars second
#                                      value as they intersect. If both are
#                                      set then the colchars value takes
#                                      precedence.
# @option --headerrowchars <list>      As --rowchars, but just applies to the
#                                      header row
# @option --headercolchars <list>      As --colchars, but just applies to the
#                                      header row
# @option --alignchars <list>          Specifies the alignment characters used.
#                                      Defaults to {- :}
# @option --vpadding <1/0>             Indicates whether to include padding on
#                                      the right and left sides of the vertical
#                                      ASCII lines (space character). Value
#                                      represents the number of spaces used.
#                                      Defaults to being enabled.
# @option --hpadding <1/0>             Indicates whether to include padding on
#                                      the top and bottom sides of the
#                                      horizontal ASCII lines. Value represents
#                                      the number of spaces (rows) used.
#                                      Defaults to being disabled.
# @return                              The data in text format with an ASCII
#                                      border
#
proc table_utils::convert_to_ascii_table { data args } {

	variable ASCII_TABLE_PRESETS

	let $args { preset {} }
	if {$preset != {} && [info exists ASCII_TABLE_PRESETS($preset)]} {
		set args [concat $ASCII_TABLE_PRESETS($preset) $args]
	}
	
	let $args {
		header           1
		border           1
		hb               $border
		vb               $border
		title            {}
		title_colspan    all
		hl               1
		hlheader         1
		alignment_row    0
		col_align        {}
		vl               1
		vpadding         1
		hpadding         0
		rowchars         {-}
		colchars         {|}
		borderchars      {}
		edgechars        {}
		headerrowchars   {}
		headercolchars   {}
		alignchars       {- :}
	}
	
	set hb [subst $hb]
	set vb [subst $vb]

	if {![llength $rowchars]} {
		set hl 0
	}
	
	if {![llength $colchars]} {
		set vl 0
	}
	
	# Defaults
	ldefault colchars       1 [lindex $rowchars 1]    [lindex $colchars 0]
	ldefault colchars       2 [lindex $borderchars 0] [lindex $colchars 0]
	ldefault colchars       3 [lindex $borderchars 1] [lindex $colchars 2]
	ldefault borderchars    0 [lindex $rowchars 0]
	ldefault borderchars    1 [lindex $rowchars 0]
	ldefault borderchars    2 [lindex $colchars 0]
	ldefault borderchars    3 [lindex $colchars 0]
	ldefault rowchars       1 [lindex $colchars 1]    [lindex $rowchars 0]
	ldefault rowchars       2 [lindex $borderchars 2] [lindex $colchars 2] [lindex $rowchars 0]
	ldefault rowchars       3 [lindex $borderchars 3] [lindex $colchars 3] [lindex $rowchars 2]
	ldefault headerrowchars 0 [lindex $rowchars 0]
	ldefault headerrowchars 1 [lindex $rowchars 1]
	ldefault headerrowchars 2 [lindex $rowchars 2]
	ldefault headerrowchars 3 [lindex $rowchars 3]
	ldefault headercolchars 0 [lindex $colchars 0]
	ldefault headercolchars 1 [lindex $colchars 1]
	ldefault headercolchars 2 [lindex $borderchars 2]
	ldefault headercolchars 3 [lindex $borderchars 3]
	ldefault edgechars      3 [lindex $edgechars 1] [lindex $edgechars 0] [lindex $rowchars 3] [lindex $borderchars 3]
	ldefault edgechars      2 [lindex $edgechars 0] [lindex $rowchars 2] [lindex $borderchars 2]
	ldefault edgechars      1 [lindex $edgechars 0] [lindex $rowchars 3] [lindex $borderchars 3]
	ldefault edgechars      0 [lindex $rowchars 2] [lindex $borderchars 2]

	if {!$vb} {
		foreach list {borderchars headerrowchars headercolchars rowchars colchars} {
			lset $list 2 {}
			lset $list 3 {}
		}
		set edgechars [lrepeat 4 {}]
	}
	
	set col_sizes [table_utils::get_column_sizes $data]
	set max_cols [llength $col_sizes]
	
	# Default column alignment
	while {[llength $col_align] < $max_cols} {
		lappend col_align {l}
	}
	
	# Adds a table title
	if {$title != {}} {
		if {$title_colspan == {all}} {
			set title_colspan $max_cols
		}
		
		# Work out the required length for the title
		set required_length [expr {[string length $title] + $vpadding * 2 + $vb * 2 - 1}]
		
		# Check whether the title will fit inside the given colspan
		set colspan_size 0
		for {set i 0} {$i < $title_colspan && $i < $max_cols} {incr i} {
			set col_length [expr {[lindex $col_sizes $i] + $vl + $vpadding * ($vl + 1)}]
			incr colspan_size $col_length
		}

		# If the title doesn't fit, expand the last column in the
		# colspan so that it fits
		if {$colspan_size < $required_length} {
			decr i
			set new_size [expr {$col_length + [string length $title] - $colspan_size}]
			lset col_sizes $i $new_size
		} else {
			set required_length $colspan_size
		}

		decr required_length
		set title_length [expr {$required_length - $vpadding * 2}]
		
		set format {${left_border}${padding}${data_0}${padding}${right_border}}
		
		# Adds the border for the title, if applicable
		if {$hb} {
			set left_border  [lindex $edgechars 0]
			set right_border [lindex $edgechars 1]
			set padding [string repeat [lindex $borderchars 0] $vpadding]
			set padding {}
			set data_0 [string repeat [lindex $borderchars 0] $required_length]
			lappend output [subst $format]
		}
		
		set left_border [lindex $headerrowchars 2]
		set right_border [lindex $headerrowchars 3]
		set padding [string repeat { } $vpadding]
		set data_0 [format %-${title_length}s $title]
		lappend output [subst $format]
	}
	
	# Set up the generic format string that will later be substituted
	set format {${left_border}}
	append format {${padding}}
	for {set i 0} {$i < $max_cols} {} {
		append format \$\{data_$i\}
		incr i
		if {$i < $max_cols} {
			if {$vl} {
				append format {${padding}}
				append format {${separator}}
			}
			append format {${padding}}
		}
	}
	append format {${padding}}
	append format {${right_border}}

	# Adds top border
	if {$hb} {
		set left_border [lindex $edgechars 0]
		set padding [string repeat [lindex $borderchars 0] $vpadding]
		set separator [lindex $colchars 2]

		for {set i 0} {$i < $max_cols} {incr i} {
			set data_$i [string repeat [lindex $borderchars 0] [lindex $col_sizes $i]]
		}
		set right_border [lindex $edgechars 1]
		lappend output [subst $format]
		set skip_first_horizontal_line 1
	}
	
	# Adds header
	if {$header} {
		set header [lindex $data 0]
		set data [lrange $data 1 end]
		
		set left_border [lindex $headercolchars 2]
		set padding [string repeat { } $vpadding]
		set separator [lindex $headercolchars 0]
		for {set i 0} {$i < $max_cols} {incr i} {
			set data_$i [format %-[lindex $col_sizes $i]s [lindex $header $i]]
		}
		set right_border [lindex $headercolchars 3]
		lappend output [subst $format]
	
		# Adds header horizontal line separator
		if {$hlheader} {
			set left_border [lindex $headerrowchars 2]
			set padding [string repeat [lindex $headerrowchars 0] $vpadding]
			set separator [lindex $headerrowchars 1]

			for {set i 0} {$i < $max_cols} {incr i} {
				set data_$i [string repeat [lindex $headerrowchars 0] [lindex $col_sizes $i]]
			}
			set right_border [lindex $headerrowchars 3]
			lappend output [subst $format]
			set skip_first_horizontal_line 1
		}
	}
	
	# Adds special alignment row used by certain formats, such as the markdown syntax
	if {$alignment_row} {
		touch header_cols
		foreach col_length $col_sizes align $col_align {
			incr col_length $vpadding
			incr col_length $vpadding
			set dashes [string repeat [lindex $alignchars 0] $col_length]
			switch -nocase -glob -- $align {
			l* { set dashes [index_replace $dashes 0   [lindex $alignchars 1]] }
			r* { set dashes [index_replace $dashes end [lindex $alignchars 1]] }
			m* -
			c* { set dashes [index_replace $dashes {0 end} [lindex $alignchars 1]] }
			default {}
			}
			lappend header_cols $dashes
		}
		
		set left_border [lindex $borderchars 2]
		set padding {}
		set separator [lindex $colchars 0]

		for {set i 0} {$i < $max_cols} {incr i} {
			set data_$i [format %-[lindex $col_sizes $i]s [lindex $header_cols $i]]
		}
		set right_border [lindex $borderchars 3]
		lappend output [subst $format]
		set skip_first_horizontal_line 0
	}
	
	# Prepares the horizontal line if relevant
	if {$hl} {
		set left_border [lindex $rowchars 2]
		set padding [string repeat [lindex $rowchars 0] $vpadding]
		set separator [lindex $colchars 1]

		for {set i 0} {$i < $max_cols} {incr i} {
			set data_$i [string repeat [lindex $rowchars 0] [lindex $col_sizes $i]]
		}
		set right_border [lindex $rowchars 3]
		set horizontal_line [subst $format]
	}
	
	# Prepeares the horizontal padding line if relevant
	if {$hpadding} {
		set padding [string repeat { } $vpadding]
		for {set i 0} {$i < $max_cols} {incr i} {
			set data_$i [string repeat { } [lindex $col_sizes $i]]
		}
		set hpadding_line [subst $format]
	}
	
	set left_border [lindex $borderchars 2]
	set padding [string repeat { } $vpadding]
	set separator [lindex $colchars 0]
	set right_border [lindex $borderchars 3]
	
	# Adds data rows to the table
	foreach row $data {
	
		# Adds horizontal line separator
		if {$hl && !$skip_first_horizontal_line} {
			for {set h 0} {$h < $hpadding} {incr h} {
				lappend output $hpadding_line
			}
			lappend output $horizontal_line
		}
		for {set h 0} {$h < $hpadding} {incr h} {
			lappend output $hpadding_line
		}
		set skip_first_horizontal_line 0
		
		for {set i 0} {$i < $max_cols} {incr i} {
			switch -nocase -glob -- [lindex $col_align $i] {
			m* -
			c* {
				set col_size [lindex $col_sizes $i]
				set txt_size [string length [lindex $row $i]]
				set pad_size [expr {($col_size - $txt_size) / 2}]
				decr col_size $pad_size
				set align [string repeat { } $pad_size]%-${col_size}s
			}
			r*      { set align %[lindex $col_sizes $i]s  }
			l* -
			default { set align %-[lindex $col_sizes $i]s }
			}
			set data_$i [format $align [lindex $row $i]]
		}
		lappend output [subst $format]
	}
	
	for {set h 0} {$h < $hpadding} {incr h} {
		lappend output $hpadding_line
	}
	
	# Adds bottom border
	if {$hb} {
		set left_border [lindex $edgechars 2]
		set padding [string repeat [lindex $borderchars 1] $vpadding]
		set separator [lindex $colchars 3]

		for {set i 0} {$i < $max_cols} {incr i} {
			set data_$i [string repeat [lindex $borderchars 1] [lindex $col_sizes $i]]
		}
		set right_border [lindex $edgechars 3]
		lappend output [subst $format]
	}

	return [join $output \n]
}