# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package provide exec_utils 1.0

#
# This execution package is intended as a wrapper around the internal Tcl
# exec proc addressing some common tasks:
#
#   - Avoid execution pretending that the command executed fine if dry run is
#     enabled
#   - Avoid square brackes in the execution string from being interpreted as
#     Tcl commands to be evaluated
#   - Checking if the command being executed exists (verified by auto_execok)
#   - Unified logging
#
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/exec.htm
# @see                   http://www.tcl.tk/man/tcl8.5/TclCmd/library.htm
#
namespace eval exec_utils {
	namespace export execute
}

#
# Attempts to execute a given command.
#
# If an error occurs during the execution then this will be raised with the
# caller unless the -nocomplain flag is passed in.
#
# If the --cmd is not passed in then any remaining arguments will be treated
# as the command to execute.
#
# Synopsis:
#    exec_utils::execute ?-option ...? command
#
# @param command            The command to execute
# @option -ignore_dry_run   Option to ignore the dry-run flag of the
#                           application and execute the command anyway.
# @option -nocomplain       Don't complain on errors 
# @option -nolog            Option to not log the output of the execution
# @option --log_level       Option to adjust the general log level used by this
#                           proc, defaults to INFO
# @option --default         The default value this proc should return when an
#                           error occurs executing the command and --nocomplain
#                           is used, defaults to empty string
# @option --dry_run_output  A special default value that is returned when a
#                           dry-run is performed, e.g. simulating a successful
#                           run of the command
# @option -ignorestderr     Stops the exec command from treating the output of
#                           messages to the pipeline's standard error channel
#                           as an error case
# @option -stderrtostdin    Capture standard error as part of the standard input
# @option -keepnewline      Retains a trailing newline in the pipeline's output.
#                           Normally a trailing newline will be deleted.
# @return                   The output of the command (if any)
#
proc exec_utils::execute { args } {

	let $args {
		ignore_dry_run 0
		nocomplain     0
		nolog          0
		default        {}
		log_level      INFO
		dry_run_output {}
		ignorestderr   0
		stderrtostdin  0
		keepnewline    0
	} exec_string
	
	if {$exec_string == {}} {
		error {wrong # args: should be "execute ?-option ...? command"}
	}
	
	# Prevent file names containing square brackets from being executed
	set exec_string [string map { [ \\[ ] \\]} $exec_string]

	if {[catch {set cmd [lindex $exec_string 0]} msg]} {
		log WARN {Error occurred while attempting to identify command executable: $msg}
	} elseif {[auto_execok $cmd] == {}} {
		log WARN {This script requires $cmd to be installed.}
	}

	if {!$ignore_dry_run && [cfg enabled DRY_RUN]} {
		log $log_level { - Pretending to execute ${exec_string} (dry run)}
		return $dry_run_output
	}
	
	set exec_cmd {}
	if {$ignorestderr} {
		lappend exec_cmd -ignorestderr
	}
	
	if {$keepnewline} {
		lappend exec_cmd -keepnewline
	}
	
	lappend exec_cmd {*}$exec_string
	
	if {$stderrtostdin} {
		lappend exec_cmd {2>@1}
	}

	log $log_level { - Attempting to execute ${exec_string}}
	
	if {[catch {
		set output [exec {*}$exec_cmd]
	} msg]} {
		log ERROR {Error occured while executing:\n    $exec_string\nError was: $msg}
		if {!$nocomplain} {
			error $msg $::errorInfo $::errorCode
		}
		return $default
	}
	
	if {!$nolog} {
		log $log_level { - Command returned:\n$output}
	}

	return $output
}
